// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/12/Screen.jack

/**
 * A library of functions for displaying graphics on the screen.
 * The Hack physical screen consists of 256 rows (indexed 0..255, top to bottom)
 * of 512 pixels each (indexed 0..511, left to right). The top left pixel on 
 * the screen is indexed (0,0).
 */ 
class Screen {
    // 16384 - 24575 memory range for screen

    // 32 words per column, 256 columns
    
    static boolean color;
    static Array twosquares, rightside, leftside;
    
    /** Initializes the Screen. */
    function void init() {
        var int i, last;
        
        let twosquares = Array.new(16);
        let leftside = Array.new(16);  //111100000
        let rightside = Array.new(16); //000000111
        let rightside[15] = 1;
        let twosquares[0] = 1;
        let i = 1;
        let last = 1;
        while( i < 16 ) {
            let last = last + last;
            let rightside[15-i] = rightside[16-i] | last;
            let twosquares[i] = last;
            let leftside[16-i] = ~rightside[15-i];
            let i = i + 1;
        }
        let leftside[0] = ~rightside[1];
        
        let color = true; //black
        return;
    }

    /** Erases the entire screen. */
    function void clearScreen() {
        var int pos, val;
        let pos = 16384;
        while( pos < 24576 ) {
            do Memory.poke( pos, 0 ); // all white
            let pos=pos + 1;
        }
        return;
    }

    /** Sets the current color, to be used for all subsequent drawXXX commands.
     *  Black is represented by true, white by false. */
    function void setColor(boolean b) {
        let color = b;
        return;
    }

    /** Draws the (x,y) pixel, using the current color. */
    function void drawPixel(int x, int y) {
        var int bit, word, val;

        let val = x/16;
        let word  =  y * 32 + 16384 + val;
        
        if( word < 16384 | (word > 24575) ) {
            do Sys.error( 7 );
        }
        
        let bit = x - (val *16);
        
        if( color ) {
            // black
            let val = twosquares[bit] | Memory.peek( word );
        }
        else {
            // white
            let val = ~twosquares[bit] & Memory.peek( word );
        }
        do Memory.poke( word, val );
        return;
    }

    /** Draws a line from pixel (x1,y1) to pixel (x2,y2), using the current color. */
    function void drawLine(int x1, int y1, int x2, int y2) {
        var int dely, delx;
        let delx = Math.abs( x2 - x1 );
        let dely = Math.abs( y2 - y1 );
        if( delx > dely ) {
            if( x1 > x2 ) {
                do Screen._drawLine( x2, y2, x1, y1, delx, dely );
                return;
            }
            if( dely = 0 ) {
                
            }
        } else {
            if( y1 > y2 ) {
                do Screen._drawLine( x2, y2, x1, y1, delx, dely );
                return;
            }
        }
        do Screen._drawLine( x1, y1, x2, y2, delx, dely );
        return;
    }
    function void _drawLine(int x1, int y1, int x2, int y2, int delx, int dely) {
        
        var int m, w, b;
        if( x1 = x2 ) {
            // vertacular lines
            while( y1 < y2 ) {
                do Screen.drawPixel( x1, y1 );
                let y1 = y1+1;
            }
            return;
        }
        if( y1 = y2 ) {
            // horizontalist lines
            let m = x1 / 16;  // delta col word
            let w = y1 * 32 + 16384 + m ; // word col+row
            let b = x1 - (m*16); // bit
            if( color ) {
                do Memory.poke( w, Memory.peek( w ) | rightside[b] );
                //do Memory.poke( w, true );
            } else {
                do Memory.poke( w, Memory.peek( w ) & ~rightside[b] );
            }
            let w = w + 1;
            let x1 = x1 + b;
            while( x2 - x1 > 16 ) {
                do Memory.poke( w, color );
                let x1 = x1 + 16;
                let w = w + 1;
            }
            if( x2 - x1 > 0 ) {
                if( color ) {
                    do Memory.poke( w, Memory.peek( w ) | leftside[delx] );
                    //                    do Memory.poke( w, true );
                } else {
                    do Memory.poke( w, Memory.peek( w ) & ~leftside[delx] );
                }
            }
            return;
        }
        
        if( dely > delx ) {
            // steeping lines
            while( dely > 0 ) {
                let m = dely/delx + 1;
                let dely = dely - m;
                while( m > 0 ) {
                    do Screen.drawPixel( x1, y1 );
                    let m = m - 1;
                    let y1 = y1 + 1;
                }
                let delx = delx - 1;
                if( x1 > x2 ) {
                    let x1 = x1 - 1;
                } else {
                    let x1 = x1 + 1;
                }
            }
            return;
        }

        // shallows lines
        while( delx > 0 ) {
            let m = delx/dely;
            let delx = delx - m;
            if( m > 32 ) {
                do Screen._drawLine( x1, y1, x1 + m, y1, 0, 0 );
                let x1 = x1 + m;
            } else {
                while( m > 0 ) {
                    do Screen.drawPixel( x1, y1 );
                    let m = m - 1;
                    let x1 = x1 + 1;
                }
            }
            let dely = dely - 1;
            if( y1 > y2 ) {
                let y1 = y1 - 1;
            } else {
                let y1 = y1 + 1;
            }
        }
        return;
    } //_drawLine

    /** Draws a filled rectangle whose top left corner is (x1, y1)
     * and bottom right corner is (x2,y2), using the current color. */
    function void drawRectangle(int x1, int y1, int x2, int y2) {
        if( y1 > y2 ) {
            do Screen.drawRectangle( x2, y2, x1, y1 );
            return;
        }
        let y2 = y2 + 1;
        while( y1 < y2 ) {
            do Screen.drawLine( x1, y1, x2, y1 );
            let y1 = y1 + 1;
        }
        return;
    }

    /** Draws a filled circle of radius r<=181 around (x,y), using the current color. */
    function void drawCircle(int x, int y, int r) {
        var int rc, yd, t1, t2, t3, t4, r2;
        
        // get the top, bottom, left and right without
        // the expensive math
        do Screen.drawPixel( x, y + r );
        do Screen.drawPixel( x, y - r );
        do Screen.drawPixel( x - r, y );
        do Screen.drawPixel( x + r, y );
        let rc=r;
        let r2 = r*r;
        while( rc > 0 ) {
            // calculate the y
            let yd = Math.sqrt( r2-(rc*rc) );
            let t1 = x + rc;
            let t2 = x - rc;
            let t3 = y + rc;
            let t4 = y - rc;
            do Screen.drawPixel( t1, t3 );
            do Screen.drawPixel( t2, t3 );
            do Screen.drawPixel( t1, t4 );
            do Screen.drawPixel( t2, t4 );
            let rc = rc - 1;
        }
        return;
    }
}
