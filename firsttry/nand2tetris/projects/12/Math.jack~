// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/12/Math.jack

/**
 * A library of commonly used mathematical functions.
 * Note: Jack compilers implement multiplication and division using OS method calls.
 */
class Math {

    static Array twosquares;
    static int lastqy;
    /** Initializes the library. */
    function void init() {
        let twosquares = Array.new(15);
        let twosquares[0] = 1;
        let twosquares[1] = 2;
        let twosquares[2] = 4;
        let twosquares[3] = 8;
        let twosquares[4] = 16;
        let twosquares[5] = 32;
        let twosquares[6] = 64;
        let twosquares[7] = 128;
        let twosquares[8] = 256;
        let twosquares[9] = 512;
        let twosquares[10] = 1024;
        let twosquares[11] = 2048;
        let twosquares[12] = 4096;
        let twosquares[13] = 8192;
        let twosquares[14] = 16384;
        return;
    }

    /** Returns the absolute value of x. */
    function int abs(int x) {
        if( x < 0 ) {
            return -x;
        }
        return x;
    }

    /** Returns the product of x and y. 
     *  When a Jack compiler detects the multiplication operator '*' in the 
     *  program's code, it handles it by invoking this method. In other words,
     *  the Jack expressions x*y and multiply(x,y) return the same value.
     */
    function int multiply(int x, int y) {
        var int bit, shiftedX, sum, neg;
        if( x = 0 | (y=0) ) {
            return 0;
        }
        let neg = ( x < 0 | ( y < 0 ) ) & ( x > 0 | (y > 0 ) );
        let x = Math.abs( x );
        let y = Math.abs( y );
        let bit=0;
        let sum=0;
        let shiftedX = x;
        while( bit < 15 ) {
            if( y & twosquares[bit] ) {
                let sum = sum + shiftedX;
            }
            let shiftedX = shiftedX + shiftedX;
            let bit = bit + 1;
        }
        if( neg ) {
            return -sum;
        }
        return sum;
    }

    /** Returns the integer part of x/y.
     *  When a Jack compiler detects the multiplication operator '/' in the 
     *  program's code, it handles it by invoking this method. In other words,
     *  the Jack expressions x/y and divide(x,y) return the same value.
     */
    function int divide(int x, int y) {
        var int q, neg;

        if( y = 0 ) {
            do Sys.error( 1 );
        }
        let neg = ( x < 0 | ( y < 0 ) ) & ( x > 0 | (y > 0 ) );
        let x = Math.abs( x );
        let y = Math.abs( y );

        
        if( y > x ) { return 0; }
        let q = Math.divide( x, y + y );
        if( q = 0 ) {
            let lastqy = y;
            return 1;
        }
        if( x - lastqy  < y ) {
            if( neg ) {
                return -(q+q);
            }
            return (q+q);
        }
        if( neg ) {
            return -(q+q+1);
        }
        let lastqy = lastqy + y;
        return q+q+1;
    }

    /** Returns the integer part of the square root of x. */
    function int sqrt(int x) {
        var int y, j, q, qq;
        if( x < 0 ) {
            do Sys.error( 4 );
        }
        let y=0;
        let j=8;
        while( j > 0 ) {
            let j = j - 1;
            let q = y + twosquares[j];
            if( q < 182 ) { // 181 is the max for a 16 bit word size
                let qq = q*q;
                if( (qq < x) | (qq=x) ) {
                    let y = q;
                }
            }
        }
        return y;
    }

    /** Returns the greater number. */
    function int max(int a, int b) {
        if( a > b ) {
            return a;
        }
        return b;
    }

    /** Returns the smaller number. */
    function int min(int a, int b) {
        if( a < b ) {
            return a;
        }
        return b;
    }
}
