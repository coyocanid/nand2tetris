// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

@R0  //check if R0 or R1 is smaller. Use that as the iterator
D=M
@R1
D=M-D //if neg, R0 is smaller so jump to the start
@START
D;JLT

//swap
@R0
D=M
@R2
M=D
@R1
D=M
@R0
M=D
@R2
D=M
@R1
M=D

(START)
@0   // init R2
D=A
@R2
M=D

(LOOP)
@R0   // check if R0 is zero
D=M
@END
D;JEQ

@R1
D=M
@R2
M=D+M

@R0   // decrement R0
M=M-1

@LOOP
0;JMP    
    
(END)
@END
0;JMP


    
