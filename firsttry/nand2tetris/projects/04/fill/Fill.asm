// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// screen 16384 - 256 rows of 32 (+ 16384 (* 32 256))
// kbd at 24576

@0
D=A
@R0   // loop color
M=D

(KEYLOOP)

@24576
D=M
@FILL
D;JNE

// loop color is zero
@R0
D=M
@KEYLOOP
D;JEQ
@0
D=A
@R0
M=D
@PAINT
0;JMP

(FILL)
@R0
D=M
@KEYLOOP
D;JNE
@0
D=!A
@R0
M=D
@PAINT
0;JMP


@KEYLOOP
0;JMP

(PAINT)
 @16384
 D=A
 @R1
 M=D
 (PLOOP)
  @24576
  D=A
  @R1
  D=D-M
  @KEYLOOP
  D;JEQ
  @R0
  D=M
  @R1
  A=M
  M=D
  @R1
  M=M+1
 @PLOOP
 0;JMP
(END)
@END
0;JMP