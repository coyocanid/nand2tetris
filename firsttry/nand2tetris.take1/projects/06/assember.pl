#!/usr/bin/perl

use strict;

use Data::Dumper;
# assembler for the hack program.
# takes a file xxxx.asm and produces xxxx.hack
#
# comments are with //
# tokens are seperated with whitespace.
#

my $file = shift @ARGV;
unless( $file ) {
    print "usage: $0 <file>\n";
    exit;
}

my $row = 0;
my $memcount = 16;
my( %syms ) = (
    SP     => 0,
    LCL    => 1,
    ARG    => 2,
    THIS   => 3,
    THAT   => 4,
    SCREEN => 16384,
    KBD    => 24576,
    ( map { ("R$_" => $_) } (0..15)) );

open my $in, '<', $file;
while( my $line = <$in> ) {
    $line =~ s!(//.*)?[\n\r]+!!;
    next unless $line;
    my @tokens = split /\s+/s, $line;
    for my $token (grep {$_} @tokens) {
        if( $token =~ /^\s*\@([0-9]+)/ ) {
            $row++;
        }
        elsif( $token =~ /^\s*\@([_\.\$\:a-zA-Z][_\.\$\:a-zA-Z0-9]*)/ ) {
            $row++;
        }
        elsif( $token =~ /^\s*\(([_\.\$\:a-zA-Z][_\.\$\:a-zA-Z0-9]*)\)/ ) {
            my $loc = label( $1 );
#            print STDERR "LABEL $1 --> $loc\n";
        }
        elsif( $token =~ /^\s*(([A-Z]+)=)?([-01\!\+ADM\&\|]+)(;(J..))?/ ) {
            $row++;
        } else {
            print STDERR "?????[$line][$token]\n";
        }
    }
}
seek $in, 0, 0;
while( my $line = <$in> ) {
    $line =~ s!(//.*)?[\n\r]+!!;
    next unless $line;
    my @tokens = split /\s+/, $line;
    for my $token (grep {$_} @tokens) {
#        print STDERR "[$token]";
        if( $token =~ /^\s*\@([0-9]+)/ ) {
            my $loc = $1;
            die "improper token '$token'" unless $loc > 0 || $loc eq '0';
#            print STDERR "$token --> ".bin($loc)."\n";
            print bin($loc)."\n";
        }
        elsif( $token =~ /^\s*\@([_\.\$\:a-zA-Z][_\.\$\:a-zA-Z0-9]*)/ ) {            
            my $loc = var( $1 );
#            print STDERR "$1 --> ".bin($loc)."\n";
            print bin($loc)."\n";
        }
        elsif( $token =~ /^\s*\(([_\.\$\:a-zA-Z][_\.\$\:a-zA-Z0-9]*)\)/ ) {
            # should have been found on the first pass
        }
        elsif( $token =~ /^\s*(([A-Z]+)=)?([-01\!\+ADM\&\|]+)(;(J..))?/ ) {
            my( $dest, $comp, $jmp ) = ( $2, $3, $5 );
            my $jfield = '000';
            if( $jmp eq 'JMP' ) {
                $jfield = '111';
            } elsif( $jmp eq 'JLT' ) {
                $jfield = '100';
            } elsif( $jmp eq 'JGT' ) {
                $jfield = '001';
            } elsif( $jmp eq 'JGE' ) {
                $jfield = '011';
            } elsif( $jmp eq 'JLE' ) {
                $jfield = '110';
            } elsif( $jmp eq 'JNE' ) {
                $jfield = '101';
            } elsif( $jmp eq 'JEQ' ) {
                $jfield = '010';
            }
            my $dfield = '000';
            if( $dest eq 'M' ) {
                $dfield = '001';
            } elsif( $dest eq 'D' ) {
                $dfield = '010';
            } elsif( $dest eq 'MD' ) {
                $dfield = '011';
            } elsif( $dest eq 'A' ) {
                $dfield = '100';
            } elsif( $dest eq 'AM' ) {
                $dfield = '101';
            } elsif( $dest eq 'AD' ) {
                $dfield = '110';
            } elsif( $dest eq 'AMD' ) {
                $dfield = '111';
            }
            my $a = $comp =~ /M/ ? "1" : "0";
            $comp =~ s/M/A/;
            my $cfield = "${a}101010";
            if( $comp eq '1' ) {
                $cfield = "${a}111111";
            } elsif( $comp eq '-1' ) {
                $cfield = "${a}111010";
            } elsif( $comp eq 'D' ) {
                $cfield = "${a}001100";
            } elsif( $comp eq 'A' ) {
                $cfield = "${a}110000";
            } elsif( $comp eq '!D' ) {
                $cfield = "${a}001101";
            } elsif( $comp eq '!A' ) {
                $cfield = "${a}110001";
            } elsif( $comp eq '-D' ) {
                $cfield = "${a}001111";
            } elsif( $comp eq '-A' ) {
                $cfield = "${a}110011";
            } elsif( $comp eq 'D+1' ) {
                $cfield = "${a}011111";
            } elsif( $comp eq 'A+1' ) {
                $cfield = "${a}110111";
            } elsif( $comp eq 'D-1' ) {
                $cfield = "${a}001110";
            } elsif( $comp eq 'A-1' ) {
                $cfield = "${a}110010";
            } elsif( $comp eq 'D+A' ) {
                $cfield = "${a}000010";
            } elsif( $comp eq 'D-A' ) {
                $cfield = "${a}010011";
            } elsif( $comp eq 'A-D' ) {
                $cfield = "${a}000111";
            } elsif( $comp eq 'D&A' ) {
                $cfield = "${a}000000";
            } elsif( $comp eq 'D|A' ) {
                $cfield = "${a}010101";
            }
#            print STDERR "111$cfield$dfield$jfield\n";
            print "111$cfield$dfield$jfield\n";
        } else {
            print STDERR "?????$line\n";
        }
    }
}
close $in;
my( %labels );
sub label {
    my $label = shift;
    if( $labels{$label} ) {
        die "Error defining label. '$label' already in use.\n";
    }
    if( $syms{$label} ) {
        return;
    }
    $syms{$label} = $row;
    $labels{$label} = 1;
    return $row;
}
sub var {
    my $symb = shift;
    if( defined(my $loc = $syms{$symb}) ) {
        return $loc;
    }
    $syms{$symb} = $memcount++;
    return $syms{$symb};
}
sub bin {
    my $number = shift;
    my $bits = '';
    for (0..14) {
        my $bit = $number % 2;
        $number = int( $number/2 );
        $bits = "$bit$bits";
    }
    return "0$bits";
}
