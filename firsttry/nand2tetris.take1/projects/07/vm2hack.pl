#!/usr/bin/perl

use strict;

use Data::Dumper;

my( @files, $outfile );
my $file = shift @ARGV;
unless( $file ) {
    print "usage : $0 <fileordirectory>\n";
    exit;
}
unless( -e $file ) {
    print "Error, file '$file' doesn't exist\n";
    exit;
}
if( -d $file ) {
    opendir(my $dh, $file) or die "Unable to open '$file' : $! $@";
    ( @files ) = map { "$file/$_" } grep { /\.vm$/ } readdir( $dh );
    unless( @files ) {
        print STDERR "Error : no vm file found in '$file'.\n"; 
        exit;
    }
    $outfile = $file;
    $outfile =~ s!/$!!;
    $outfile =~ s!([^/]*)$!$1/$1.asm!;
} 
elsif( $file =~ /\.vm$/ ) {
    ( @files ) = ( $file );
    $outfile = $file;
    $outfile =~ s!.vm$!.asm!;
}
else {
    print STDERR "Error : '$file' is not a vm file.\n"; 
    exit;
}

open my $out, '>', $outfile;
#-------------

my $stackcount = 0;
my %function_stackcount;
my $labels;
my $function_label = '';
my $locals;

# segments : local, argument, this, that, constant, static

#
# SP  : 256 ( at first )
# LCL
# ARG
# THIS
# THAT
#

my %segments = (
    'local'    => 'LCL',
    'argument' => 'ARG',
    'this'     => 'THIS',
    'that'     => 'THAT',
    );
my $TEMPSTART = 5;
my @TEMP = (5..12);

sub out {
    my $txt = shift;
    $txt =~ s/([^\n\r])$/$1\n/;
    print STDERR $txt;
    print $out $txt;
}

sub dopop {
    if( $function_label && $function_stackcount{$function_label}-- < 1 ) {
        print STDERR "Error: trying to pop from empty function stack '$function_label'";
#        warn "Error: trying to pop from empty function stack '$function_label'";
    }
    my $to_D = shift;
    out '@SP';
    out 'AM=M-1';
    out 'D=M' if $to_D;
}
sub dopush {
    if( $stackcount++ > 1791 ) {
        # 2047 - 256 (- 2047 256)
        die "Error: trying to push onto full stack";
    }
    $function_stackcount{$function_label}++;
    out '@SP';   # A = 0
    out 'A=M';   # A = M[0]
    out 'M=D';   # M[M[0]] = D
    out '@SP';   # A = 0
    out 'M=M+1';   # M[0]=M[0]+1
}

#____________

# init the SP (stack pointer)
#    LCL TEMP and stuff
#out '@256';
#out 'D=A';
#out '@SP';
#out 'M=D';
process_files();
out '(END)';
out '@END';
out '0;JMP';

close $out;
exit;

sub process_files {
    for my $file (@files) {
        my $root = ".$file";
        $root =~ s/.vm$//;
        $root =~ s!/!:!g;

        open my $in, '<', $file;
        while( my $line = <$in> ) {
            $line =~ s/\s+$//s;
            $line =~ s/^\s+//s;
            $line =~ s!//.*!!;
            next unless $line;

            if( $line =~ /(pop|push)\s+(\S+)\s+(\S+)/ ) {
                my( $arg, $segment, $index ) = ( $1, $2, $3 );
                my $isPop = $arg eq 'pop';
                my $isPush = ! $isPop;
                dopop "D" if $isPop;
                if( $segment eq 'constant' && $isPush ) {
                    out "\@$index";
                    out "D=A";
                }
                elsif( $segment =~ /argument|local|this|that/ ) {
                    out "\@$segments{$segment}";
                    while( 0 < $index-- ) {
                        out "A=A+1";
                    }
                    if( $isPop ) {
                        # pop from the stack and put it in the segment
                        out "M=D";
                    } else {
                        # take the value from this segment and put it on the stack
                        out 'D=M';
                    }
                }
                elsif( $segment eq 'static' ) {
                    out "\@${segment}_$index";
                    out 'M=D' if $isPop;
                    out 'D=M' if $isPush;
                }
                elsif( $segment eq 'pointer' ) {
                    die "Max pointer index is 1" if $index > 1;
                    my $addy = 3 + $index;
                    out "\@$addy";
                    out 'M=D' if $isPop;
                    out 'D=M' if $isPush;
                }
                elsif( $segment eq 'temp' ) {
                    die "Max temp index is 7" if $index > 7;
                    my $addy = $TEMP[$index];
                    out "\@$addy";
                    out 'M=D' if $isPop;
                    out 'D=M' if $isPush;
                } else {
                    print STDERR "?????????$line\n";
                }
                dopush if $isPush;
            } 
            elsif( $line eq 'add' ) {
                dopop  'D';
                dopop;
                out 'D=M+D';
                dopush;
            } 
            elsif( $line eq 'sub' ) {
                dopop 'D';
                dopop;
                out 'D=M-D';
                dopush;
            }
            elsif( $line eq 'neg' ) {
                dopop 'D';
                out 'D=!D';
                out 'D=D+1';
                dopush;
            }
            elsif( $line eq 'eq' ) {
                dopop 'D';
                dopop;
                out 'D=M-D';
                out '@label_'.++$labels;
                out 'D;JEQ';
                out '@0';
                out 'D=A-1';
                out "(label_$labels)";
                out 'D=!D';
                dopush;
            }
            elsif( $line eq 'gt' ) {
                dopop 'D';
                dopop;
                out 'D=D-M';
                out '@32768'; #1000000000000000 
                out 'D=A&D';  #1000000000000000 if negative, 0 if pos
                out '@label_'.++$labels;
                out 'D;JEQ';
                out '@0';
                out 'D=A-1';
                out "(label_$labels)";
                dopush;
            }
            elsif( $line eq 'lt' ) {
                dopop 'D';
                dopop;
                out 'D=M-D';
                out '@32768'; #1000000000000000 
                out 'D=A&D';  #1000000000000000 if negative, 0 if pos
                out '@label_'.++$labels;
                out 'D;JEQ';
                out '@0';
                out 'D=A-1';
                out "(label_$labels)";
                dopush;
            }
            elsif( $line eq 'and' ) {
                dopop 'D';
                dopop;
                out 'D=M&D';
                dopush;
            }
            elsif( $line eq 'or' ) {
                dopop 'D';
                dopop;
                out 'D=M|D';
                dopush;
            }
            elsif( $line eq 'not' ) {
                out '// NOT';
                dopop 'D';
                out 'D=!D';
                dopush;
            }
            
            elsif( $line =~ /label\s+(\S+)/ ) {
                my $label= "$function_label:$1";
                out "($label)";
            }
            elsif( $line =~ /if-goto\s+(\S+)/ ) {
                my $label= "$function_label:$1";
                dopop 'D';
                out "\@$label";
                out 'D;JNE';
            }
            elsif( $line =~ /goto\s+(\S+)/ ) {
                my $label= "$function_label:$1";
                out "\@$label";
                out '0;JMP';
            }
            
            elsif( $line =~ /function\s+(\S+)\s+(\d+)/ ) {
                my $label = $1;
                $locals = $2;

                # to jump to this function
                out "($label)";

                # at this point in time, the args are on the stack as well as the return value
                # and previous LCL, ARG, THIS, THAT

                # init the local variables
                if( $locals > 0 ) {
                    for( 1..$locals ) {
                        out '@0';
                        out 'D=A';
                        dopush;
                    }
                }
                $function_label = $label;
            }
            elsif( $line =~ /call\s+(\S+)\s+(\S+)/ ) {
                my $to_function = "$root:$1";
                my $arg_count = $2;

                # create a return label to return to when the call is done.
                my $return_label = "\$return\$$function_label";
                out "\@$return_label";
                out 'D=A';
                dopush;

                # store old values of segment pointers
                for my $R (qw( LCL ARG THIS THAT ) ) {
                    out "\@$R";
                    out "D=M";
                    dopush;
                }
                    
                out "\@$to_function";
                out "0;JMP";
                out "($return_label)";
            }
            elsif( $line eq 'return' ) {

                if( $function_stackcount{$function_label} > 1 ) {
                    die "Error : function '$function_label' did not pop everything off its stack ($function_stackcount{$function_label}";
                }
                if( $function_stackcount{$function_label} == 0  ) {
                    die "Error : function '$function_label' did not push a return value on to the stack";
                }
                undef $function_label;

                # store return value in temp
                dopop 'D'; 
                out "\@$TEMP[0]";
                out 'M=D';

                # remove the locals
                if( $locals > 0 ) {
                    for( 1..$locals ) {
                        dopop;
                    }
                }
                # store the ARG in TEMP 2
                out '// ARGGGGG ';
                out '@ARG';
                out 'D=M';
                out "\@$TEMP[2]";
                out 'M=D';

                # restore the registers
                for my $R (qw( THAT THIS ARG LCL  ) ) {
                    dopop 'D';
                    out "\@$R";
                    out "M=D";
                }

                dopop "D"; #return address
                out "\@$TEMP[1]";
                out 'M=D';

                # put the PS to the stored ARG
                out "\@$TEMP[2]";
                out 'D=M';
                out '@PS';
                out 'M=D';
                                
                out "\@$TEMP[0]";
                out 'D=M';
                dopush; # return value back onto the stack

                # jump to the return address
                out "\@$TEMP[1]";
                out 'A=M';
                out "0;JMP";
            }
            else {
                print STDERR "???$line\n";
            }
        }
        close $in;
    }
} #process_files


