// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.

@R0
D=M
@R1
D=D-M; // if D is zero or less, then R0 < R1 so skip the swapem
@START 
D;JLT

// swapem, make sure R0 < R1
@R0  // A <- 0  ( Think of A @ as choosing which RAM )
D=M  // D <- R[0]
@R2  // A <- 2
M=D  // R[2] <- D
@R1  // A <- 1
D=M  // D <- R[1]
@R0  // A <- 0
M=D  // R[0] <- D
@R2  // A <- 2
D=M  // D <- R[2]
@R1  // A <- 1
M=D  // R[1] <- D

(START)
@R2
M=0

// add R1 to R2 R0 times
(LOOP) //while R0 > 0, 
 @R0    // check if R0 is 0. if so end
 D=M
 @END
 D;JEQ  // jump to end if R0 == 0

 @R1
 D=M
 @R2
 M=M+D

 @R0
 M=M-1

 @LOOP
 0;JMP
 
(END)
 @END
 0;JMP
