// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

(LOOP)

  // check keyboard at address 24576
  @24576
  D=M

  @CLEAR
  D;JEQ  //when nothing pressed

  @C
  M=-1
  @FILL
  0;JMP

(CLEAR)
 @C
 M=0
 
(FILL)
 // 16384 <-- start of screen. screen has 256 rows, each row has 32 words
 // 256 x 32 = 8192, so 24576 is the last address

 @16384
 D=A
 @R0 // KEEP TRACK OF WHICH ADDRESS TO USE
 M=D

 (FLOOP)
  @C
  D=M // paint color

  @R0 // A <-- 0
  A=M // A <-- M[0]
  M=D // M[M[0]] <- paint color. does paint

  // now incrememnt screen location
  @R0
  M=M+1
  
  @R0
  D=M
  
  @24576
  D=D-A

  @FLOOP
  D;JLT

 @LOOP //back to polling
 0;JMP

(END)
    @END
    0;JMP