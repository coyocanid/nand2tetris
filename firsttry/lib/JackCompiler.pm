package JackCompiler;

use strict;
use warnings;

use lib '/home/wolf/nand2tetris/lib';
use Analyzer;

sub compile {
    my( $self, @files ) = @_;
    my $vm_out;
    for my $file (@files) {
        my $vmfile = $file;
        $vmfile =~ s/\.jack$/.vm/;
        open $vm_out, ">", $vmfile or die "Unable to open '$vmfile' for output : $@ $!";
        $self->{vm_out} = $vm_out;
        $self->{class} = undef;
        $self->{subr}  = undef;
        $self->{nodes} = [];
        $self->{file} = $file;
        $self->{ANA}->analyze( $file, 'class', $self );
    }
}


sub new { 
    bless {
        ANA => new Analyzer(
            NON_TERMINALS => [qw( class classVarDec subroutineDec parameterList
                          subroutineBody varDec statements whileStatement
                          ifStatement returnStatement letStatement doStatement
                          expression term expressionList)],
            TERMINALS     => [qw(keyword symbol integerConstant stringConstant identifier)],
            TOKEN_ORDER       => [qw(stringConstant lineComment blockComment 
                                     endComment keyword symbol integerConstant identifier)],
            BLOCK_COMMENT     => 'blockComment',
            END_BLOCK_COMMENT => 'endComment',
            LINE_COMMENT      => 'lineComment',
            TOKENS => {
                keyword         => '(class|constructor|function|method|field|static|var|int|char|boolean|void|true|false|null|this|let|do|if|else|while|return)$',
                symbol          => '[-{}\(\)\[\].,;+*\/&|<>=~]',
                integerConstant => '\d+',
                stringConstant  => '"[^\"]*"',
                identifier      => '[_a-zA-Z][_a-zA-Z0-9]*',
                lineComment     => '\/\/',   # //
                blockComment    => '\/\*',   # /*
                endComment      => '.*?\*\/',  # */
            },
            STRUCTURE => {
                class          => "'class' className '{' classVarDec* subroutineDec* '}'",
                classVarDec    => "('static'|'field') type varName (',' varName)* ';'",
                type           => "'int'|'char'|'boolean'|className",
                subroutineDec  => "('constructor'|'function'|'method') ('void'|type) subroutineName '(' parameterList ')' subroutineBody",
                parameterList  => "((type varName) (',' type varName)*)?",
                subroutineBody => "'{' varDec* statements '}'",
                varDec         => "'var' type varName (',' varName)* ';'",
                className      => 'identifier',
                subroutineName => 'identifier',
                varName        => 'identifier',

                statements => 'statement*',
                statement => 'letStatement|ifStatement|whileStatement|doStatement|returnStatement',
                letStatement => "'let' varName ('[' expression ']' )? '=' expression ';'",
                ifStatement => "'if' '(' expression ')' '{' statements '}' ('else' '{' statements '}' )?",
                whileStatement => "'while' '(' expression ')' '{' statements '}'",
                doStatement     => "'do' subroutineCall ';'",
                returnStatement => "'return' expression? ';'",

                expression => 'term (op term)*',
                term => "integerConstant|stringConstant|keywordConstant|subroutineCall|varName '[' expression ']'|varName|'(' expression ')'|unaryOp term",
                subroutineCall => "(className|varName) '.' subroutineName '(' expressionList ')'|subroutineName '(' expressionList ')'",
                expressionList => "(expression (',' expression)* )?",
                op             => "'+'|'-'|'*'|'/'|'&'|'|'|'<'|'>'|'='",
                unaryOp => "'-'|'~'",
                keywordConstant => "'true'|'false'|'null'|'this'",
            },
            ),
    }, shift;
} #new

my %SYM_TO_OP = (
    '+' => 'add',
    '-' => 'sub',
    '/' => 'call Math.divide 2',
    '*' => 'call Math.multiply 2',
    '<' => 'lt',
    '>' => 'gt',
    '=' => 'eq',
    '&' => 'and',
    '|' => 'or',
    );
my %SYMB_TO_UNOP = (
    '-' => 'neg',
    '~' => 'not',
    );
sub err {
    my( $self, $msg ) = @_;
    print STDERR "$msg in $self->{file}:$self->{curLine}\n";
    exit;
}

sub receive_open {
    my( $self, $ttype, $line )  = @_;
    my $node = { type => $ttype, vals => [], val_types => [], lines => [], subnodes => [] };
    if( $self->{node} ) {
        $node->{parent} = $self->{node};
        push @{$self->{node}{subnodes}}, $node;
        push @{$self->{node}{vals}}, $node;
        push @{$self->{node}{val_types}}, $ttype;
        push @{$self->{node}{lines}}, $line;
        push @{$self->{node}{$ttype}}, $node;
        $self->{node} = $node;
    } else {
        $self->{node} = $node;
    }
    if( $ttype eq 'class' ) {
        $self->{class} = $node;
    }
            
    if( $ttype eq 'subroutineDec' ) {
        $self->{subr} = $node;
    }
}

sub receive_close {
    my( $self, $ttype, $line )  = @_;
    if( $ttype eq 'subroutineDec' ) {
        my( $subr_kind, $ret_val, $name ) = @{$self->{node}{vals}};
        my( undef, $var_cat ) = @{$self->{node}{val_types}};
        my $returnsObj = $var_cat eq 'identifier';
        $self->{class}{subrs}{$name} = [ $subr_kind, $ret_val, $returnsObj ];
    }
    elsif( $ttype eq 'classVarDec' || $ttype eq 'varDec' ) {
        my( $kind, $var_type, $var_name, @rest ) = @{$self->{node}{vals}};
        my( undef, $var_desig ) = @{$self->{node}{val_types}};
        my( undef, undef, @rest_lines ) = @{$self->{node}{lines}};
        my $isObj = $var_desig eq 'identifier';
        if( $kind eq 'var' ) {
            $kind = 'local';
        }
        my @names;
        while( @rest > 1 ) {
            ( undef, my $vname, @rest ) = @rest;
            push @names, $vname;
        }
        my $node;
        my $namespace = $self->classname;
        if( $ttype eq 'varDec' ) {
            $node = $self->{subr};
            $namespace .= ".".$self->subrname;
        } else {
            $node = $self->{class};
        }
        for my $name ($var_name,@names) {
            my $kk = $self->getkindcount( $kind );
            $self->{curLine} = shift @rest_lines;

            if( $node->{vars}{$kind}{$name} ) {
                $self->err( "Var '$var_name' already declared" );
            }
            $node->{kinds}{$kind}++;
            $node->{vars}{$kind}{$name} = [ $namespace, $name, $var_type, $isObj, $kind, $kk ];
        }
    }
    elsif( $ttype eq 'parameterList' ) {
        my( $var_type, $var_name, @rest ) = @{$self->{node}{vals}};
        if( $var_type ) {
            my( $var_desig, undef, @rest_types ) = @{$self->{node}{val_types}};
            my( undef, $var_line, @rest_lines ) = @{$self->{node}{lines}};            
            my $isObj = $var_desig eq 'identifier';
            
            my $namespace = $self->classname.'.'.$self->subrname;
            my $kind = 'argument';
            my $kk = $self->getkindcount( $kind );
            if( $self->{subr}{vars}{$kind}{$var_name} ) {
                $self->{curLine} = $var_line;
                $self->err( "Var '$var_name' already declared");
            }
            $self->{subr}{kinds}{$kind}++;
            $self->{subr}{vars}{$kind}{$var_name} = [ $namespace, $var_name, $var_type, $isObj, $kind, $kk ];
            while( @rest > 1 ) {
                ( undef, $var_type, $var_name, @rest ) = @rest;
                ( undef, $var_desig, undef, @rest_types ) = @rest_types;
                ( undef, undef, $self->{curLine} ) = shift @rest_lines;
                $isObj = $var_desig eq 'identifier';
                my $kk = $self->getkindcount( $kind );
                if( $self->{subr}{vars}{$kind}{$var_name} ) {
                    $self->err( "Var '$var_name' already declared" );
                }
                $self->{subr}{kinds}{$kind}++;
                $self->{subr}{vars}{$kind}{$var_name} = [ $namespace, $var_name, $var_type, $isObj, $kind, $kk ];
            }
        }
    } #parameterList
    elsif( $ttype eq 'class' ) {
        $self->_output( $self->{class} );
    } #class
        
    $self->{node} = $self->{node}{parent};
} #receive_close

sub receive_val {
    my( $self, $ttype, $val, $line )  = @_;
    push @{$self->{node}{vals}}, $val;
    push @{$self->{node}{val_types}}, $ttype;
    push @{$self->{node}{lines}}, $line;
    push @{$self->{node}{$ttype}}, $val;
    if( $self->{node}{type} eq 'subroutineDec' && @{$self->{node}{vals}} == 1) {
        if( $self->{node}{vals}[0] eq 'method' ) {
            $self->{node}{kinds}{argument} = 1;
        }
    }
}

our %LABELS;
sub _label {
    my( $self, $lab ) = @_;
    my $id = $LABELS{$self->{subr}}{$lab}++;
    "$lab$id";
}

sub _output_call {
    my( $self, $stuff, $stuff_type, $stuff_lines, $noassign ) = @_;
    my $vm_out = $self->{vm_out};
    if( $stuff->[1] eq '.' ) {
        my( $objOrCl, undef, $subr_name, undef, $express ) = @$stuff;
        my( @args ) = @{$express->{subnodes}};
        my $args = @args;
        my $var = $self->getvar( $objOrCl );
        if( $var ) {
            # method call
            my( $namespace, $name, $var_type, $isObj, $kind, $kind_idx ) = @$var;
            $args++;
            $kind = 'this' if $kind eq 'field';
            print $vm_out "push $kind $kind_idx\n"; 
            for my $arg (@args) {
                $self->_output( $arg );
            }
            print $vm_out "call $var_type.$subr_name $args\n";
        }
        else {
            for my $arg (@args) {
                $self->_output( $arg );
            }
            print $vm_out "call $objOrCl.$subr_name $args\n";
        }
    }
    else {
        my( $subr_name, undef, $arguments ) = @$stuff;
        my( $subr_name_line ) = @$stuff_lines;
        my $subr = $self->getsubr( $subr_name );
        unless( $subr ) {
            $self->{curLine} = $subr_name_line;
            $self->err( "Could not find subroutine '$subr_name'" );
        }
        my( $subr_type, $ret_val, $returnsObj ) = @$subr;
        # local function or method call
        my $cn = $self->classname;
        my $args = @{$arguments->{subnodes}};
        if( $subr_type eq 'method' ) {
            print $vm_out "push pointer 0\n";
            $args++;
        }
        $self->_output( $arguments );

        $noassign //= $ret_val eq 'void';
        
        print $vm_out "call $cn.$subr_name $args\n";
    }
    if( $noassign ) {
        print $vm_out "pop temp 0\n";
    }
} #_output_call

sub _output {
    my( $self, $node ) = @_;
    my $type = $node->{type};
    my $vm_out = $self->{vm_out};

    if( $type eq 'subroutineDec' ) {
        $self->{subr} = $node;
        my( $subrType, $retVal, $name ) = @{$node->{vals}};
        my( $paramList, $body ) = @{$node->{subnodes}};
        my $locals = $self->getkindcount('local');
        my $namespace = $self->classname.".$name";
        print $vm_out "function $namespace $locals\n";
        if( $subrType eq 'constructor' ) {
            my $fields = $self->{class}{kinds}{field} || 0;
            print $vm_out "push constant $fields\n";
            print $vm_out "call Memory.alloc 1\n";
            print $vm_out "pop pointer 0\n";
        }
        elsif( $subrType eq 'method' ) {
            print $vm_out "push argument 0\n";
            print $vm_out "pop pointer 0\n";
        }
        $self->_output( $body );
        my( @parts ) = @{$body->{subnodes}};
        my $statements = pop @parts;
        ( @parts ) = @{$statements->{subnodes}};
        my $stat = pop @parts;
        if( ! $stat || $stat->{type} ne 'returnStatement' ) {
            print $vm_out "push constant 0\n";
            print $vm_out "return\n";
        }
    }
    elsif( $type eq 'letStatement' ) {
        my( $expr1, $expr2 ) = @{$node->{subnodes}};
        my( undef, $var_name ) = @{$node->{vals}};
        my( undef, $var_name_line ) = @{$node->{lines}};
        my $var = $self->getvar( $var_name );
        unless( $var ) {
            $self->{curLine} = $var_name_line;
            $self->err( "Variable '$var_name' not found" );
        }
        my( $namespace, $name, $var_type, $isObj, $kind, $kind_idx ) = @$var;

        if( $expr2 ) {
            $self->_output( $expr1 );
            if( $kind eq 'field' ) {
                print $vm_out "push this $kind_idx\n";
            } else {
                print $vm_out "push $kind $kind_idx\n";
            }
            print $vm_out "add\n";
            $self->_output( $expr2 );
            my $tmp = $self->{class}{TEMP}++;
            print $vm_out "pop temp $tmp\n";
            print $vm_out "pop pointer 1\n";
            print $vm_out "push temp $tmp\n";
            $self->{class}{TEMP}--;
            print $vm_out "pop that 0\n";
        }
        else {
            $self->_output( $expr1 );
            if( $kind eq 'field' ) {
                print $vm_out "pop this $kind_idx\n";
            } else {
                print $vm_out "pop $kind $kind_idx\n";
            }
        }
    }
    elsif( $type eq 'ifStatement' ) {
        my( $expression, $statements, $elsebit ) = @{$node->{subnodes}};
        my $true_lab = $self->_label('IF_TRUE');
        my $false_lab = $self->_label('IF_FALSE');
        my $else_lab = $self->_label('IF_END');

        $self->_output( $expression );
        print $vm_out "if-goto $true_lab\n";
        print $vm_out "goto $false_lab\n";
        print $vm_out "label $true_lab\n";
        $self->_output( $statements );
        if( $elsebit ) {
            print $vm_out "goto $else_lab\n";
        }
        print $vm_out "label $false_lab\n";
        if( $elsebit ) {
            $self->_output( $elsebit );
            print $vm_out "label $else_lab\n";
        } 
    }
    elsif( $type eq 'whileStatement' ) {
        my( $expression, $statements ) = @{$node->{subnodes}};
        my $loop_lab = $self->_label('WHILE_EXP');
        my $out_lab = $self->_label('WHILE_END');
        print $vm_out "label $loop_lab\n";
        $self->_output( $expression );
        print $vm_out "not\n";
        print $vm_out "if-goto $out_lab\n";
        $self->_output( $statements );
        print $vm_out "goto $loop_lab\n";
        print $vm_out "label $out_lab\n";

    }
    elsif( $type eq 'doStatement' ) {
        my( undef, @stuff ) = @{$node->{vals}};
        my( undef, @stuff_type ) = @{$node->{val_types}};
        my( undef, @stuff_lines ) = @{$node->{lines}};
        $self->_output_call( \@stuff, \@stuff_type, \@stuff_lines, 'noassign' );
    }
    elsif( $type eq 'returnStatement' ) {
        my( $expression ) = @{$node->{subnodes}};
        if( $expression ) {
            $self->_output( $expression );
        } else {
            print $vm_out "push constant 0\n";
        }
        print $vm_out "return\n";
    }
    elsif( $type eq 'expression' ) {
        my( $term, @terms ) = @{$node->{subnodes}};
        my( @ops )   = @{$node->{symbol}||[]};
        $self->_output( $term );
        while( @terms ) {
            $term = shift @terms;
            $self->_output( $term );
            my $op = shift @ops;
            print $vm_out "$SYM_TO_OP{$op}\n";
        }
    }
    elsif( $type eq 'term' ) {
        my( @vals ) = @{$node->{vals}};
        my( @val_types ) = @{$node->{val_types}};
        my( @val_lines ) = @{$node->{lines}};
        if( @vals == 1 ) {
            if( $val_types[0] eq 'identifier' ) {
                # variable
                my $var = $self->getvar( $vals[0] );
                unless( $var ) {
                    $self->err( "unknown variable '$vals[0]'" );
                }
                my( $namespace, $name, $var_type, $isObj, $kind, $kind_idx ) = @$var;
                if( $kind eq 'field' ) {
                    print $vm_out "push this $kind_idx\n";
                }
                else {
                    print $vm_out "push $kind $kind_idx\n";
                }
            } 
            elsif( $val_types[0] eq 'integerConstant' ) {
                print $vm_out "push constant $vals[0]\n";
            }
            elsif( $val_types[0] eq 'stringConstant' ) {
                if( $vals[0] =~ /^"(.*)"$/ ) {
                    my $str = $1;
                    my $len = length( $str );
                    print $vm_out "push constant $len\n";
                    print $vm_out "call String.new 1\n";
                    for my $char ( map { ord($_) } split //, $str ) {
                        print $vm_out "push constant $char\n";
                        print $vm_out "call String.appendChar 2\n";
                    }
                }
            }
            elsif( $val_types[0] eq 'keyword' ) {
                if( $vals[0] eq 'true' ) {
                    print $vm_out "push constant 0\n";
                    print $vm_out "not\n";
                } elsif( $vals[0] eq 'false' || $vals[0] eq 'null' ) {
                    print $vm_out "push constant 0\n";
                } elsif( $vals[0] eq 'this' ) {
                    print $vm_out "push pointer 0\n";
                }
            }
        }
        elsif( $vals[0] eq '(' ) {
            $self->_output( $vals[1] ); #expression
        }
        elsif( $val_types[0] eq 'symbol' ) {
            # unary term
            $self->_output( $vals[1] ); #term
            print $vm_out "$SYMB_TO_UNOP{$vals[0]}\n";
        }
        elsif( $vals[1] eq '[' ) {
            # array, get the value
            my( $expr ) = @{$node->{subnodes}};
            my $var = $self->getvar( $vals[0] );
            my( $namespace, $name, $var_type, $isObj, $kind, $kind_idx ) = @$var;
            $self->_output( $expr );
            if( $kind eq 'field' ) {
                print $vm_out "push this $kind_idx\n";
            }
            else {
                print $vm_out "push $kind $kind_idx\n";
            }
            print $vm_out "add\n";
            print $vm_out "pop pointer 1\n";
            print $vm_out "push that 0\n";
        }
        else {
            # subr call
            $self->_output_call( \@vals, \@val_types, \@val_lines );
        }
    }
    else {
        for my $sn (@{$node->{subnodes}}) {
            $self->_output( $sn );
        }
    }
} #_output
sub classname {
    my $self = shift;
    $self->{class} ? $self->{class}{vals}[1] : undef;
}
sub subrname {
    my $self = shift;
    $self->{subr} ? $self->{subr}{vals}[2] : undef;
}
sub getkindcount {
    my( $self, $kind ) = @_;
    if( $kind eq 'local' || $kind eq 'argument' ) {
        return $self->{subr}{kinds}{$kind} || 0;
    }
    return $self->{class}{kinds}{$kind} || 0;
}

sub getsubr {
    my( $self, $subrname ) = @_;
    return $self->{class}{subrs}{$subrname};
}

sub getvar {
    my( $self, $varname ) = @_;
    if( $self->{subr} ) {
        for my $kind (qw( local argument )) {
            if( ( my $var = $self->{subr}{vars}{$kind}{$varname} ) ) {
                return $var;
            }
        }
    }
    for my $kind (qw( field static )) {
        if( ( my $var = $self->{class}{vars}{$kind}{$varname} ) ) {
            return $var;
        }
    }
}

1;
