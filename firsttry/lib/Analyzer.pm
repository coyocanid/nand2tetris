package Analyzer;

use strict;
use warnings;

use lib '/home/wolf/nand2tetris/lib';
use Tokenizer;

sub new {
    my( $pkg, %args ) = @_;

    my $TOK_REGEX = {};
    for my $ttype ( keys %{$args{TOKENS}} ) {
        $TOK_REGEX->{$ttype} = qr/^\s*($args{TOKENS}{$ttype})(.*)/;
    }
    
    bless {
        TOK           => new Tokenizer( %args ),
        TOK_REGEX     => $TOK_REGEX,
        STRUCTURE     => $args{STRUCTURE},
        TERMINALS     => { map { $_ => 1 } @{$args{TERMINALS}} },
        NON_TERMINALS => { map { $_ => 1 } @{$args{NON_TERMINALS}||[]} },
    }, shift;
} #new

sub analyze {
    my( $self, $file, $expect, $listener ) = @_;
    $self->{TOK}->tokenize( $file );
    $self->{LISTENER} = $listener;
    $self->{file} = $file;
    $self->{spec_tok} = [];
    $self->{spec_lvl} = [];
    $self->{emits} = [];
    $self->{err} = [];
    $self->{curLine} = 0;
    $self->_expect( $expect );
}

sub _emit_open {
    my( $self, $type ) = @_;
    return unless $self->{NON_TERMINALS}{$type};

    if( $self->_speculating ) {
        push @{$self->{emits}[0]}, ['open',$type];
    }
    else {
        $self->{LISTENER}->receive_open( $type, $self->{curLine} );
    }
} #_emit_open
sub _emit_close {
    my( $self, $type ) = @_;
    return unless $self->{NON_TERMINALS}{$type};
    if( $self->_speculating ) {
        push @{$self->{emits}[0]}, ['close',$type];
    }
    else {
        $self->{LISTENER}->receive_close( $type, $self->{curLine} );
    }
}
sub _emit_atom {
    my( $self, $ttype, $tok, $line ) = @_;
    $self->{curLine} = $line;
    return unless $self->{TERMINALS}{$ttype};
    if( $self->_speculating ) {
        push @{$self->{emits}[0]}, ['atom',$ttype,$tok,$line];
    }
    else {
        #        $self->{LISTENER}->receive_val( $ttype, $tok, $self->{curLine} );
        $self->{LISTENER}->receive_val( $ttype, $tok, $line );
    }
}

sub _err {
    my( $self, $err ) = @_;
    my $specs = $self->{spec_tok};
    my( $line, $ttype, $tok ) = split ' ', $specs->[$#$specs];
    my $eline = "$err and got $ttype '$tok' in $self->{file}:$line";
    if( $self->_speculating ) {
        unshift @{$self->{err}[0]}, $eline;
    } else {
        my $errs = shift @{$self->{err}};
        my $err  = (shift @$errs) || $eline;
        print STDERR "$err\n";
        exit;
    }
}

sub _speculate {
    my $self = shift;
    my $spec_lvl = $self->{spec_lvl};
    my $spec_idx = @$spec_lvl ? $spec_lvl->[0] : 0;
    unshift @$spec_lvl, $spec_idx;
    unshift @{$self->{emits}}, [];
    unshift @{$self->{err}}, [];
}

sub _speculating {
    @{shift->{spec_lvl}} > 0;
}

sub _end_speculation {
    my( $self, $accept ) = @_;

    my $spec_lvl = $self->{spec_lvl};
    my $spec_tok = $self->{spec_tok};
    my $emits    = $self->{emits};
    my $errs     = $self->{err};

    my $last_pos   = shift @$spec_lvl;
    my $last_emits = shift @$emits;

    if( $accept ) {
        shift @$errs;
        if( @$spec_lvl == 0 ) {
            for my $emission (@$last_emits) {
                if( $emission->[0] eq 'open' ) {
                    $self->_emit_open( $emission->[1] );
                }
                elsif( $emission->[0] eq 'close' ) {
                    $self->_emit_close( $emission->[1] );
                }
                elsif( $emission->[0] eq 'atom' ) {
                    $self->_emit_atom( $emission->[1], $emission->[2], $emission->[3] );
                }
            }
            # these tokens were used up to the last position
            splice @$spec_tok, 0, $last_pos + 1;
        }
        else {
            $spec_lvl->[0] = $last_pos;
            push @{$emits->[0]}, @$last_emits;
        }
    }
} #end_speculation


sub _nexttok {
    my $self = shift;
    my $tok;
    my $spec = $self->_speculating;
    my $spec_lvl = $self->{spec_lvl};
    my $spec_idx = $spec ? $spec_lvl->[0] : 0;
    my $spec_tok = $self->{spec_tok};
    if( $spec && $spec_idx < @$spec_tok ) {
        $tok = $spec_tok->[$spec_idx];
    } elsif( @$spec_tok && ! $spec ) {
        $tok = shift @$spec_tok;
    } else {
        $tok = $self->{TOK}->nexttok;
        chomp $tok;
        if( $spec ) {
            push @$spec_tok, $tok;
        }
    }
    if( $spec ) {
        $spec_lvl->[0]++;
    }
    my( $linenum, $ttype, $token ) = split( /\s+/, $tok, 3 );
    return $linenum, $ttype, $token;
} #_nexttok

sub _expect {
    my( $self, $expected ) = @_;

    my $shard = $self->_shatter( $expected );
    my $mode = $shard->{mode};
    if( $mode eq 'repeat' ) {
        my $what  = $shard->{repeat};
        my $rmode = $shard->{repeat_mode};
        $self->_emit_open( $shard->{recipe} );
        if( $rmode eq '*' ) {
            $self->_speculate;
            while( (my $res=$self->_expect( $what )) ) {
                $self->_end_speculation( 'e' );
                $self->_speculate;
            }
            $self->_end_speculation;
        }
        elsif( $rmode eq '?' ) {
            $self->_speculate;
            if( $self->_expect( $what ) ) {
                $self->_end_speculation( "e" );
            } else {
                $self->_end_speculation;
            }
        }
        else {
            die "Unknown repeat mode '$rmode'";
        }
        $self->_emit_close( $shard->{recipe} );
        return 1;
    }

    elsif( $mode eq 'sequence' ) {
        $self->_emit_open( $shard->{recipe} );
        for my $part (@{$shard->{sequence}}) {
            unless( $self->_expect( $part ) ) {
                $self->_err( "Expected $part" );
                return 0;
            }
        }
        $self->_emit_close( $shard->{recipe} );
        return 1;
    }

    elsif( $mode eq 'ors' ) {
        $self->_emit_open( $shard->{recipe} );
        for my $part (@{$shard->{ors}}) {
            $self->_speculate;
            if( $self->_expect( $part ) ) {
                $self->_end_speculation( 'e' );
                $self->_emit_close( $shard->{recipe} );
                return 1;
            }
            $self->_end_speculation;
        }
        $self->_err( "Expected $shard->{recipe}" );
        return 0;
    }

    elsif( $mode eq 'literal' ) {
        my( $line, $ttype, $tok ) = $self->_nexttok;
        if( $shard->{literal} eq $tok ) {
            $self->_emit_atom( $shard->{recipe}, $tok, $line );
            return 1;
        }
        $self->_err( "Expected $shard->{recipe} '$shard->{literal}'" );
        return 0;
    }

    elsif( $mode eq 'regex' ) {
        my( $line, $ttype, $tok ) = $self->_nexttok;
        if( $tok =~ $shard->{regex} ) {
            $self->_emit_atom( $shard->{recipe}, $tok, $line );
            return 1;
        }
        $self->_err( "Expected $shard->{recipe}, got '$tok'" );
        return 0;
    }

    else {
        die "Unknown mode '$mode'";
    }
} #_expect


sub _shatter {
    my( $self, $recipe ) = @_;

    my $shard;
    if( ($shard = $self->{SHATTERED}{$recipe}) ) {
        return $shard;
    }
    $shard = { recipe => $recipe };

    my( @parts );

    # break up the recipe into its parts

    my $pos = 0;
    my $lvl = 0;
    my $inStr = 0;

    my( @oridx );

    while( $pos < length($recipe) ) {
        my $c = substr $recipe, $pos, 1;
        if( $c eq "'" ) {
            $inStr = ! $inStr;
        }
        elsif( ! $inStr ) {
            if( $c eq '|' && $lvl == 0 ) {
                push @oridx, $pos;
            }
            elsif( $c eq '(' ) {
                $lvl++;
            }
            elsif( $c eq ')' ) {
                $lvl--;
            }
        }
        $pos++;
    } # collect ors

    $pos = 0;
    if( @oridx ) {
        my( @ors );
        for my $idx (@oridx) {
            my $len = $idx - $pos;
            push @ors, grep { $_ } substr( $recipe, $pos, $len );
            $pos = $idx + 1;
        }
        push @ors, grep { $_ } substr( $recipe, $pos );

        #
        # mode - ors
        # ors - or recipes
        # recipe - asdfas|asfd|asfdsaf
        #

        $shard->{mode} = 'ors';
        $shard->{ors} = \@ors;
        $self->{SHATTERED}{$recipe} = $shard;

        return $shard;
    }

    # done with ors. now go after parens and other parts
    $lvl = 0;
    $pos = 0;
    my $wasParen = 0;
    my $startFrom = 0;
    while( $pos < length($recipe) ) {
        my $c = substr $recipe, $pos, 1;
        if( $c eq "'" ) {
            $inStr = ! $inStr;
        }
        elsif( ! $inStr ) {
            if( $c eq '(' ) {
                $lvl++;
            }
            elsif( $c eq ')' ) {
                $lvl--;
                if( $lvl == 0 ) {
                    $wasParen = 1;
                }
            }
            elsif( $wasParen ) {
                if( ! ( $c eq '*' || $c eq '?' ) ) {
                    my $len = $pos - $startFrom;
                    push @parts, substr( $recipe, $startFrom, $len );
                    $wasParen = 0;
                    $startFrom += $len + 1;
                }
            }
            elsif( $lvl == 0 && $c eq ' ' ) {
                my $len = $pos - $startFrom;
                push @parts, substr( $recipe, $startFrom, $len );
                $startFrom += $len + 1;
            }
        }
        $pos++;
    } # collect parts
    push @parts, grep { $_ } substr( $recipe, $startFrom );

    if( @parts > 1 ) {
        #
        # mode - sequence
        # sequence - recipes
        # recipe - asdfas|asfd|asfdsaf
        #
        $shard->{mode} = 'sequence';
        $shard->{sequence} = \@parts;
        $self->{SHATTERED}{$recipe} = $shard;

        return $shard;
    }

    my( $part ) = (@parts);

    if( $part =~ /^\((.*?)\)$/ ) {
        # the parens melt away, what is inside is shattered &  returned
        my $formula = $1;
        my $components = $self->_shatter( $formula );
        for my $key (keys %$components) {
            next if $key eq 'recipe';
            # the shard aquires the characteristsics
            # of its componetns
            $shard->{$key} = $components->{$key};
        }
        $self->{SHATTERED}{$recipe} = $shard;
        return $shard;
    }
    elsif( $part =~ /^(.*?)([?*])$/ ) {
        my( $core, $repeat ) = ( $1, $2 );
        #
        # mode - repeat
        # repeat - repeated recipe
        # repeat_mode = '*' or '?'
        # recipe - asdfas*
        #
        $shard->{mode} = 'repeat';
        $shard->{repeat} = $core;
        $shard->{repeat_mode} = $repeat;
        $self->{SHATTERED}{$recipe} = $shard;
        return $shard;
    }

    if( (my $formula = $self->{STRUCTURE}{$part}) ) {
        my $components = $self->_shatter( $formula );
        if( $self->{NON_TERMINALS}{$recipe} ) {
            #
            # fundamental : associate the recipe
            #  with its own shard
            #
            # mode - mode of the shattered formula
            # other things are the same as the shattered formula
            # recipe - whatever the name is
            #
            for my $key (keys %$components) {
                # the shard aquires the characteristsics
                # of its componetns
                next if $key eq 'recipe';
                $shard->{$key} = $components->{$key};
            }
            $self->{SHATTERED}{$recipe} = $shard;
            return $shard;
        }
        # not fundamental, associate the recipe
        # with its shattered components
        $self->{SHATTERED}{$recipe} = $components;
        return $components;
    }

    if( (my $regex = $self->{TOK_REGEX}{$part}) ) {
        $shard->{mode} = 'regex';
        $shard->{regex} = $regex;
        $self->{SHATTERED}{$recipe} = $shard;
        return $shard;
    }
    elsif( $part =~ /^'(.*)'$/ ) {
        my $word = $1;
        for my $re (qw( keyword symbol )) {
            if( $word =~ $self->{TOK_REGEX}{$re} ) {
                $shard->{recipe} = $re;
                $shard->{mode} = 'literal';
                $shard->{literal} = $word;
                $self->{SHATTERED}{$recipe} = $shard;
                return $shard;
            }
        }
        die "Unkown keyword or symbol '$word'" ;
    }
    die "Tokenizer unable to parse '$recipe'";

} #_shatter



1;
