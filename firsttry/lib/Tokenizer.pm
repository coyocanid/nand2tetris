package Tokenizer;

use strict;
use warnings;

use lib '/home/wolf/nand2tetris/lib';

sub new {
    my( $pkg, %args ) = @_;

    bless {
        TOKEN_ORDER       => $args{TOKEN_ORDER},
        TOK_REGEX         => { map { $_ => qr/^\s*($args{TOKENS}{$_})(.*)/ } keys %{$args{TOKENS}} },
        LINE_COMMENT      => $args{LINE_COMMENT},
        BLOCK_COMMENT     => $args{BLOCK_COMMENT},
        END_BLOCK_COMMENT => $args{END_BLOCK_COMMENT},
    }, shift;
}

sub tokenize {
    my( $self, $in_file ) = @_;
    
    if( $self->{FH} ) {
        close $self->{FH};
    }
    
    open my $fh, '<', $in_file or die "unable to open '$in_file' : $! $@";
    $self->{FH} = $fh;
    $self->{inComment} = 0;
    $self->{curLine} = 0;
    $self->{line} = '';
    $self->{curLine} = 0;
}

sub _line {
    my $self = shift;
    while( $self->{line} eq '' ) {
        my $fh = $self->{FH};
        my $line = <$fh>;
        return unless defined( $line );
        $self->{curLine}++;
        $line =~ s/^\s*//;
        $line =~ s/[\n\r\s]*$//s;
        $self->{line} = $line;
    }
    return $self->{line};
} #_line

sub nexttok {
    my $self = shift;
  LINE: while( $self->_line ) {

    CURLINE:
      my $found = 0;
      for my $ttype (@{$self->{TOKEN_ORDER}}) {
          if( $self->{line} =~ $self->{TOK_REGEX}{$ttype} ) {
              $found = 1;
              my $tok = $1;
              $self->{line} = $2;
              if( $ttype eq $self->{LINE_COMMENT} ) {
                  $self->{line} = '';
                  next LINE;
              }
              if( $ttype eq $self->{BLOCK_COMMENT} ) {
                  $self->{inComment} = 1;
                  goto CURLINE;
              }
              if( $ttype eq $self->{END_BLOCK_COMMENT} ) {
                  if( ! $self->{inComment} ) {
                      die "comment end where none expected";
                      exit;
                  }
                  $self->{inComment} = 0;
                  goto CURLINE;
              }
              if( ! $self->{inComment} ) {
                  return "$self->{curLine} $ttype $tok";
              }
          }
      } #each type regex
      if( $self->{inComment} ) {
          $self->{line} = '';
      }
      elsif( ! $found && $self->{line} ) {
          die "Unable to tokenize line '$self->{line}'";
          exit;
      }
  } #each line
} #nexttok


1;
