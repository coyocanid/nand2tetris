#!/usr/bin/perl

use strict;
use Data::Dumper;
use Cwd 'abs_path';

# compiles the following VM language spec into
# op codes
#   call <subrname> <arg-count>
#   if-goto <linenum>
#   goto <linenum>
#   label <label>
#   push <segment> <segment index>
#   pop  <segment> <segment index>
#   function <name> <local var count>
#   return
#   add | sub | neg | eq | lt | gt | and | or | not
#
# valid segments
#   constant - integer constant source. doesn't take up memory
#   temp - 8 general use registers
#   argument - function arguments
#   local - function local variables
#   this    - used to manip heap
#   that    - used to manip heap
#   pointer - 2 entry holds base address of this&that
#   static - static vars shared by all function in class
#

my( @files, $outfile, $labels, $cur_function );

my( $file, $do_init, $init_sp ) = @ARGV;
unless( $file ) {
    print "Usage: $0 <fileordirectory>\n";
    exit;
}
unless( -e $file ) {
    print "Error : File '$file' not found";
    exit;
}

if( -d $file ) {
    opendir( my $dh, $file);
    @files = map { "$file/$_" } grep { /\.vm$/ } readdir $dh;
    $outfile = abs_path($file);
    $outfile =~ s!/$!!;
    if( $outfile =~ m!([^/]+)(/+)?$! ) {
        $outfile = "$file/$1.asm";
    }
}
else {
    push @files, $file;
    $outfile = $file;
    $outfile =~ s/\.vm$/.asm/;
}

open my $out, '>', $outfile or die "Error: Unable ot open '$outfile' for writing : $! $@";

my %REGS = (
    'local'    => 'LCL',
    'argument' => 'ARG',
    'this'     => 'THIS',
    'that'     => 'THAT',
    );

dostart() if $init_sp;
if( $do_init ) {
    out( "\@Sys.init" );
    out( '0;JMP' );
}
dofiles();
doend() unless $do_init;

exit;

#--------------------

sub out {
    my $txt = shift;
    print STDERR "$txt\n";
    print $out "$txt\n";
}

sub dopush {
    out '@SP';
    out 'A=M';
    out 'M=D';
    out '@SP';
    out 'M=M+1';
}

sub dopop {
    my $toD = shift;
    out '@SP';
    out 'AM=M-1';
    out 'D=M' if $toD;
}

sub doend {
    out '(END)';
    out '@END';
    out '0;JMP';
}

sub label {
    'L_'.++$labels;
}

sub docall {
    my( $function, $args ) = @_;

    # ARGS HAVE BEEN PUSHED ON ALREADY
    
    # SAVE RET VALUE after args
    my $ret_addy = label;
    out "\@$ret_addy";
    out 'D=A';
    dopush;

    # SAVE values of caller registers
    for my $REG (qw( LCL ARG THIS THAT ) ) {
        out "\@$REG";
        out 'D=M';
        dopush;
    }

    # set new ARG pointer. It is back 5, then back $args times.
    my $goback = 5 + $args;
    out "\@$goback";
    out 'D=A';
    out '@SP';
    out 'D=M-D';
    out '@ARG';
    out 'M=D';

    # set current local to current SP location
    # locals will be pushed here by the called function
    out '@SP';
    out 'D=M';
    out '@LCL';
    out 'M=D';
    
    # jump to the called function
    out "\@$function";
    out '0;JMP';

    # set the return address location
    out "($ret_addy)";
}

sub dostart {
    out '@256';
    out 'D=A';
    out '@SP';
    out 'M=D';
}

sub dofiles {
    for my $file (@files) {
        open my $in, '<', $file or die "ERROR : unable to open file '$file' : $! $@";
        my $stem = $file;
        $stem =~ s!.*([^/]+)\.vm$!$1!;
        while( my $line = <$in> ) {
            $line =~ s!//.*!!;
            $line =~ s/^\s*//;
            $line =~ s/[\s\r\n]*$//s;
            next unless $line;

            if( $line =~ /^(push|pop)\s+(\S+)\s+(\S+)/ ) {
                my( $action, $segment, $index ) = ( $1, $2, $3 );
                my $isPush = $action eq 'push';
                my $isPop = $action eq 'pop';
                dopop 'D' if $isPop;
                if( $segment eq 'constant' ) {
                    if( $isPush ) {
                        out "\@$index";
                        out 'D=A';
                    } else {
                        # not sure if valid, but it could just
                        # do a pop to nowhere
                    }
                } else {
                    if( $segment eq 'static' ) {
                        out "\@$stem:ST_$index";
                    } 
                    elsif( $segment eq 'temp' ) {
                        my $loc = 5 + $index;
                        out "\@$loc";
                    }
                    elsif( $segment eq 'pointer' ) {
                        my $loc = 3 + $index;
                        out "\@$loc";
                    }
                    elsif( (my $REG = $REGS{$segment}) ) {
                        out "\@$REG";
                        if( $index > 0 ) {
                            out 'A=M+1';
                            if( $index > 1 ) {
                                for( 2..$index ) {
                                    out 'A=A+1';
                                }
                            }
                        } else {
                            out 'A=M';
                        }
                    } 
                    else {
                        print STDERR "??????$line<<($segment)($REGS{$segment})\n";
                    }
                    if( $isPush ) {
                        out 'D=M';
                    } else {
                        out 'M=D';
                    }
                }
                dopush if $isPush;
            }

            elsif( $line =~ /^label\s+(\S+)/ ) {
                my $lab = "$cur_function:$1";
                out "($lab)";
            }   
            elsif( $line =~ /^goto\s+(\S+)/ ) {
                my $lab = "$cur_function:$1";
                out "\@$lab";
                out '0;JMP';
            }
            elsif( $line =~ /^if-goto\s+(\S+)/ ) {
                my $lab = "$cur_function:$1";
                dopop 'D';
                out "\@$lab";
                out 'D;JNE';                
            }
            elsif( $line =~ /^call\s+(\S+)\s+(\S+)/ ) {
                docall( $1, $2 );
                # after call
                #  --- call stack SP ---
                #  args  <--- NEW ARG
                #  return addy 
                #  LCL  (caller LCL)
                #  ARG  (caller ARG)
                #  THIS (caller THIS)
                #  THAT (caller THAT)
                #         <---- NEW LCL
            }
            elsif( $line =~ /^function\s+(\S+)\s+(\S+)/ ) {
                ( $cur_function, my $locals ) = ( $1, $2 );
                out "($cur_function)";
                out 'D=0';
                for ( 1..$locals ) {
                    dopush;
                }
            }
            elsif( $line eq 'return' ) {
                # capture return value, return address
                # reset SP
                dopop "D";  #return value
                out "\@T_RV:$cur_function";
                out 'M=D';

                # capture where the SP will be set to
                # save as REPOSP
                out '@ARG';
                out 'D=M';
                out "\@T_SP:$cur_function"; #temp variable
                out 'M=D';
                
                out '@LCL';  # just after the saved THAT
                out 'D=M';   # D is the THAT ADRESS + 1
                out '@SP';
                out 'M=D';   # position the stack pointer at THAT + 1

                # restore old registers
                for my $REG ( qw( THAT THIS ARG LCL ) ) {
                    dopop "D";
                    out "\@$REG"; 
                    out 'M=D';
                }
                # D with return addy value
                dopop "D";
                out "\@T_RA:$cur_function";
                out 'M=D';

                # REPOSITION SP
                out "\@T_SP:$cur_function"; #temp variable
                out 'D=M';
                out '@SP';
                out 'M=D';

                # PUSH RETURN VALUE HERE
                out "\@T_RV:$cur_function";
                out 'D=M';
                dopush;

                # CLEAR ALL TEMP SEGMENTS
#                out '@5';
#                for( 1..8 ) {
#                    out 'M=0';
#                    out 'A=A+1';
#                }

                # GOTO RETURN ADDY
                out "\@T_RA:$cur_function";
                out 'A=M';
                out '0;JMP';
            }
            elsif( $line eq 'add' ) {
                dopop "D";
                dopop;
                out 'D=M+D';
                dopush;
            }
            elsif( $line eq 'sub' ) {
                dopop "D";
                dopop;
                out 'D=M-D';
                dopush;
            }
            elsif( $line eq 'neg' ) {
                dopop "D";
                out 'D=!D';
                out 'D=D+1';
                dopush;
            }
            elsif( $line eq 'eq' ) {
                dopop "D";
                dopop;
                out 'D=M-D';
                my $label = label;
                out "\@$label";
                out 'D;JEQ';
                out '@0';
                out 'D=A-1';
                out "($label)";
                out 'D=!D';
                dopush;
            }
            elsif( $line eq 'gt' ) {
                dopop "D";  #B  is A>B?
                dopop;      #A
                out 'D=D-M'; #is neg if A>B
                my $label = label;
                out '@32767'; #0111111111111111
                out 'D=D|A';  #1111111111111111 if gt, 0????????????????? otherwise
                out "\@$label";
                out 'D;JLT';
                out 'D=0';
                out "($label)";
                dopush;
            }
            elsif( $line eq 'lt' ) {
                dopop "D";  #B  is A>B?
                dopop;      #A
                out 'D=M-D'; #is neg if A>B
                my $label = label;
                out '@32767'; #0111111111111111
                out 'D=D|A';  #1111111111111111 if gt, 0????????????????? otherwise
                out "\@$label";
                out 'D;JLT';
                out 'D=0';
                out "($label)";
                dopush;
            }
            elsif( $line eq 'and' ) {
                dopop "D";
                dopop;
                out 'D=D&M';
                dopush;
            }
            elsif( $line eq 'or' ) {
                dopop "D";
                dopop;
                out 'D=D|M';
                dopush;
            }
            elsif( $line eq 'not' ) {
                dopop "D";
                out 'D=!D';
                dopush;
            }

            else {
                print STDERR "????$line\n";
            }
        }
    }
}
__END__

example of pointer, this and that
>>push constant 3030
SP 3030
P0 
P1
THIS
THAT
------
>>pop pointer 0
SP
P0 3030
P1
THIS
THAT
------
>>push constant 3040
SP 3040
P0 3030
P1 
THIS
THAT
------
>>pop pointer 1
SP
P0 3030
P1 3040
THIS
THAT
------
>>push constant 32
SP 32
P0 3030
P1 3040
THIS
THAT
------
>>pop this 2
SP
P0 3030
P1 3040
THIS M[3030+2] = 32
THAT M[3040]
------
>>push constant 46
SP 46
P0 3030
P1 3040
THIS
THAT 
------
>>pop that 6
SP 
P0 3030
P1 3040
THIS M[3030+2] = 32
THAT M[3040+6] = 46
------
>>push pointer 0
SP 3030
P0 3030
P1 3040
THIS M[3030+2] = 32
THAT M[3040+6] = 46
------
>>push pointer 1
SP 3030 3040
P0 3030
P1 3040
THIS M[3030+2] = 32
THAT M[3040+6] = 46
------
>>add
SP 6070
P0 3030
P1 3040
THIS M[3030+2] = 32
THAT M[3040+6] = 46
------
>>push this 2
SP 6070 32
P0 3030
P1 3040
THIS M[3030+2] = 32
THAT M[3040+6] = 46
------
>>sub
SP 6038
P0 3030
P1 3040
THIS M[3030+2] = 32
THAT M[3040+6] = 46
------
>>push that 6
SP 6038 46
P0 3030
P1 3040
THIS M[3030+2] = 32
THAT M[3040+6] = 46
------
>>add
SP 6084
P0 3030
P1 3040
THIS M[3030+2] = 32
THAT M[3040+6] = 46
