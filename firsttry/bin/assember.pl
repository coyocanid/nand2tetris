#!/usr/bin/perl

use strict;

use Data::Dumper;
use Cwd;
# assembler for the hack program.
# takes a file xxxx.asm and produces xxxx.hack
#
# comments are with //
# tokens are seperated with whitespace.
#

my $file = shift @ARGV;
my( @files );
unless( $file ) {
    print "usage: $0 <file>\n";
    exit;
}
if( -d $file ) {
    # directory
    opendir( my $dh, $file);
    @files = map { "$file/$_" } grep { /\.asm$/ } readdir $dh;
    
} 
elsif( $file =~ /\.asm$/ ) {
    push @files, $file;
}

my $row = 0;
my $memcount = 16;
my( %syms ) = (
    SP     => 0,
    LCL    => 1,
    ARG    => 2,
    THIS   => 3,
    THAT   => 4,
    SCREEN => 16384,
    KBD    => 24576,
    ( map { ("R$_" => $_) } (0..15)) );

my( %jfields ) =  (
    JMP => '111',
    JLT => '100',
    JGT => '001',
    JEQ => '010',
    JNE => '101',
    JLE => '110',
    JGE => '011',
    );

my( %dfields ) =  (
    A          => '001',
    D          => '010',
    M          => '001',
    AD         => '110',
    AM         => '101',
    MD         => '011',
    AMD        => '111',
    );
my( %cfields ) = (
    '1'   => '111111',
    '-1'  => '111010',
    'D'   => '001100',
    'A'   => '110000',
    '!D'  => '001101',
    '!A'  => '110001',
    '-D'  => '001111',
    '-A'  => '110011',
    'D+1' => '011111',
    'A+1' => '110111',
    'D-1' => '001110',
    'A-1' => '110010',
    'D+A' => '000010',
    'D-A' => '010011',
    'A-D' => '000111',
    'D&A' => '000000',
    'D|A' => '010101',
    );

sub assemble {
    my $file = shift;
    open my $in, '<', $file;
    while( my $line = <$in> ) {
        $line =~ s!(//.*)?[\n\r]+!!;
        next unless $line;
        my @tokens = split /\s+/s, $line;
        for my $token (grep {$_} @tokens) {
            if( $token =~ /^\s*\@([0-9]+)/ ) {
                $row++;
            }
            elsif( $token =~ /^\s*\@([_\.\$\:a-zA-Z][_\.\$\:a-zA-Z0-9]*)/ ) {
                $row++;
            }
            elsif( $token =~ /^\s*\(([_\.\$\:a-zA-Z][_\.\$\:a-zA-Z0-9]*)\)/ ) {
                my $loc = label( $1, $file );
                #            print STDERR "LABEL $1 --> $loc\n";
            }
            elsif( $token =~ /^\s*(([A-Z]+)=)?([-01\!\+ADM\&\|]+)(;(J..))?/ ) {
                $row++;
            } else {
                die "Invalid on line $line : '$token'\n";
            }
        }
    }
    seek $in, 0, 0;
    while( my $line = <$in> ) {
        $line =~ s!(//.*)?[\n\r]+!!;
        next unless $line;
        my @tokens = split /\s+/, $line;
        for my $token (grep {$_} @tokens) {
            if( $token =~ /^\s*\@([0-9]+)/ ) {
                my $loc = $1;
                die "improper token '$token'" unless $loc > 0 || $loc eq '0';
                print bin($loc)."\n";
            }
            elsif( $token =~ /^\s*\@([_\.\$\:a-zA-Z][_\.\$\:a-zA-Z0-9]*)/ ) {            
                my $loc = var( $1, $file );
                print bin($loc)."\n";
            }
            elsif( $token =~ /^\s*\(([_\.\$\:a-zA-Z][_\.\$\:a-zA-Z0-9]*)\)/ ) {
                # should have been found on the first pass
            }
            elsif( $token =~ /^\s*(([A-Z]+)=)?([-01\!\+ADM\&\|]+)(;(J..))?/ ) {
                my( $dest, $comp, $jmp ) = ( $2, $3, $5 );
                my $jfield = $jfields{$jmp} || '000';
                my $dfield = $dfields{$dest} || '000';
                
                my $a = $comp =~ /M/ ? "1" : "0";
                $comp =~ s/M/A/;
                my $cfield = $a . ($cfields{$comp} || '101010' );
                print "111$cfield$dfield$jfield\n";
            } else {
                die "Bad token line $line : '$token'\n";
            }
        }
    }
    close $in;
} #assemble
my( %labels );
sub label {
    my( $label, $file ) = @_;
    $label = "$file\$$label";
    if( $labels{$label} ) {
        die "Error defining label. '$label' already in use.\n";
    }
    if( $syms{$label} ) {
        return;
    }
    $syms{$label} = $row;
    $labels{$label} = 1;
    return $row;
}
sub var {
    my( $symb ) = @_;
    if( defined(my $loc = $syms{$symb}) ) {
        return $loc;
    }
    $syms{$symb} = $memcount++;
    return $syms{$symb};
}
sub bin {
    my $number = shift;
    my $bits = '';
    for (0..14) {
        my $bit = $number % 2;
        $number = int( $number/2 );
        $bits = "$bit$bits";
    }
    return "0$bits";
}
