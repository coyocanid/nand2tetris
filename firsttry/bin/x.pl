use strict;
use warnings;
use Data::Dumper;
sub mul {
    my( $x, $y ) = @_;
    use Carp 'longmess'; print STDERR Data::Dumper->Dump([longmess]) unless defined($y);
    return 0 if $x == 0 || $y == 0;
    my $neg = ( $x > 0 && $y < 0 ) || ( $y > 0 && $x < 0 );
    $x = abs( $x );
    $y = abs( $y );
    my $sum = 0;
    my $bit = 0;
    my $sh = $x;
    while( $bit < 15 ) {
        if( $y & 2**$bit ) {
            $sum += $sh;
        }
        $sh = $sh+$sh;
        $bit++;
    }
    $sum;
}
sub divy {
    my( $x, $y ) = @_;
    my $neg = ( $x < 0 || $y < 0 ) && !($x<0&&$y<0);
    $x = abs( $x );
    $y = abs( $y );
    die "div by zero error" if $y== 0;
    return 0 if $y > $x;
    my $q = mul(2 , divy( $x, $y+$y ) );
    if( ($x - mul($q,$y)) < $y ) {
        if( $neg ) {
            return (-1 * $q);
        }
        return $q;
    }
    if( $neg ) {
        return ( -1 * ($q+1));
    }
    return $q+1;
}
sub d {
    my( $x, $y ) = @_;
    print "$x / $y = ".divy( $x, $y )."\n";
}
sub squirt {
    my $x = shift;
    die if $x<0;
    my $j = 8;
    my $y = 0;
    while( $j > 0 ) {
        $j--;
        my $q = $y + 2**$j;
        print "QQ ($j) $q --> $y + 2**$j = ".mul($q,$q)."\n";
        if( mul($q,$q) < ( $x+1) ) {
            $y = $q;
        }
    }
    return $y;
}
sub sq {
    my $x = shift;
    print "sqrt $x = ".squirt($x)."\n";
}
sub ml {
    my( $x, $y ) = @_;
    print "$x * $y = ".mul( $x, $y )."\n";
}
# ml( 181, 181 );
# ml( 182, 182 );
# ml( 183, 183 );
# ml( 184, 184 );

# d( 1, 12 );
# d( 12, 1 );
# d( -3000, 800 );
# d( 234, 3 );
# d( -30, -4 );

# sq( 9 );
# sq( 1 );
# sq( 0 );
# sq( 93 );
sq( 32767 );
