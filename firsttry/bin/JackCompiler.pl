#!/usr/bin/perl

#fri feb 23rd
#6pm start
#55$/person (choice of entre)
#
#patio espaniol 2850 alameny blvd

use strict;

use Data::Dumper;

use Carp 'confess';
$SIG{__DIE__} = \&Carp::confess;

use lib '/home/wolf/nand2tetris/lib';
use JackCompiler;

my( $file ) = @ARGV;
$file ||= 'Square/Square.jack';
my( @files );

unless( $file ) {
    print "usage : $0 <file.jack or directory>\n";
    exit;
}

if( -d $file ) {
    opendir( my $dh, $file);
    @files = map { "$file/$_" } grep { /\.jack$/ } readdir $dh;
} else {
    push @files, $file;
}

my $compa = new JackCompiler;
$compa->compile( @files );

exit;

1;
