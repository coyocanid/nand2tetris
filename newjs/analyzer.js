let {matches, match} = require( "./tools.js" );
//export
class Analyzer {
    constructor(language_defs) {
        this.lang = language_defs;
        let types = this.lang.token_types;
        this.lang.token_types = {};
        types.map( t => this.lang.token_types[t] = t );
        this.nodes = {};
    }
    get_analyzer( name ) {
        if( this.nodes[name] ) {
            return this.nodes[name];
        }
        let formula = this.lang.composites[ name ];
        if( ! formula ) {
            throw new Error( "Unknown composite '"+name+"'");
        }
        let working_node = new ANode('AND');
        working_node.lang = this.lang;
        working_node.name = name;
        working_node.ana = this;
        this.nodes[name] = working_node;
        while( match( formula, /\S/ ) ) {
            if( match( formula, /^\s*'([^']+)'\s*([\?\*])?(.*)/ ) ) {
                formula = matches[3];
                working_node.add( 'LITERAL', matches[1], matches[2] );
            }
            else if( match( formula, /^\s*\((.*)/ ) ) {
                formula = matches[1];
                working_node = working_node.add( 'AND' );
            }
            else if( match( formula, /^\s*\|(.*)/ ) ) {
                formula = matches[1];
                if( working_node.type !== 'OR' ) {
                    working_node.type = 'OR';
                    if( working_node.subnodes.length > 1 ) {
                        let andNode = new ANode('AND');
                        working_node.lang = this.lang;
                        working_node.ana = this;
                        andNode.subnodes = working_node.subnodes;
                        working_node.subnodes = [ andNode ];
                        working_node = working_node.add( 'AND' );
                    }
                }
            }
            else if( match( formula, /^\s*\)\s*([\?\*])?(.*)/ ) ) {
                formula = matches[2];
                working_node = working_node.close( matches[1]);
            }
            else if( match( formula, /^\s*([a-zA-Z_0-9]+)\s*([\?\*])?(.*)/ ) ) {
                formula = matches[3];
                if( this.lang.token_types[ matches[1] ] ) {
                    working_node.add( 'TOKEN', matches[1], matches[2] );
                }
                else if( this.lang.token_types[ this.lang.composites[matches[1]] ] ) {
                    working_node.add( 'TOKEN', this.lang.composites[matches[1]], matches[2] );
                }
                else if( this.lang.composites[matches[1]] ) {
                    working_node.add( 'NODE', matches[1], matches[2] );
                }
                else {
                    throw new Error( "Unknown formula name '" + matches[1] + "'" );
                }
            }
            else {
                throw new Error( "Unknown part of formula '" + formula + "'" );
            }
        }
        return working_node;
    } //get_analyzer
} //class Analyzer

module.exports = { Analyzer };

class MatcherNode {
    constructor( type, val ) {
        this.type = type;
        if( type === 'TOKEN' || type === 'LITERAL' ) {
            this.tok = val;
        } else {
            this.name = val;
        }
        this.subnodes = [];
    }
    giveBackTokens(tokens) {
        if( this.type === 'TOKEN' || this.type === 'LITERAL' ) {
            tokens.unshift( this.tok );
            return 1;
        }
        let count = 0;
        for( let i=(this.subnodes.length-1); i>=0; i-- ) {
            count = count + this.subnodes[i].giveBackTokens( tokens );
        }
        return count;
    }
    add( nodes ) {
        // nodes can be a list of or a single node
        if( Array.isArray( nodes ) ) {
            this.subnodes.push( ...nodes );
        }
        else {
            this.subnodes.push( nodes );
        }
    }
    to_string(indent) {
        indent = indent || '';
        let str = '';
        if( this.name ) {
            str = indent + '<' + this.name + '>\n';
            indent = indent + ' ';
        }
        if( this.type === 'LITERAL' || this.type === 'TOKEN' ) {
            str = indent + '<'+this.tok.type+'>' + this.tok.value + '</'+this.tok.type+'>\n'
        }
        if( this.type === 'AND' ) {
            str = str + this.subnodes.map( sn => sn.to_string( indent ) ).join('');
        }
        if( this.name ) {
            indent = indent.substring(1);
            str = str + indent + '</' + this.name + '>\n';
        }
        return str;
    } //to_string
    tokens(valueOnly) {
        if( this.type === 'TOKEN' || this.type === 'LITERAL' ) {
            return valueOnly ? [this.tok.value] : [this.tok];
        }
        let toks = [];
        for( let i=0; i<this.subnodes.length; i++ ) {
            let sntoks = this.subnodes[i].tokens(valueOnly);
            toks.push( ...sntoks );
        }
        return toks;
    } //tokens
    tag_tokens() {
        if( this.type === 'TOKEN' || this.type === 'LITERAL' ) {
            return;
        }
        if( this.name ) {
            let toks = this.tokens();
            toks.map( t => {
                t.tag.unshift( this.name );
            } );
        }
        // most specific tags go first
        this.subnodes.map( sn => {
            sn.tag_tokens();
        } );
    }
} //MatcherNode

let acount = 1;
class ANode {
    constructor(type,val) {
        this.type = type;
        this.value = val;
        this.subnodes = [];
        this.repeat = undefined;
        this.ana = undefined;
        this.id = acount++;
    }
    get_analyzer(name) {
        return this.ana.get_analyzer(name);
    }
    add(type,val,repeat) {
        let add_node;
        if( type === 'NODE' ) {
            add_node = new ANode( 'NODE', this.get_analyzer( val ) );
            add_node.name = val;
        }
        else if( type === 'LITERAL' || type === 'TOKEN' ) {
            add_node = new ANode( type, val );
            add_node.lang = this.lang;
            add_node.ana = this;
        }
        else if( type === 'AND' ) {
            add_node = new ANode( type );
            add_node.lang = this.lang;
            add_node.ana = this;
            add_node.parent = this;
        }
        add_node.repeat = repeat;
        this.subnodes.push( add_node );
        return add_node;
    } //add
    close(repeat) {
        this.repeat = repeat;
        return this.parent;
    }
    to_string() {
        let val = '';
        if( this.type === 'LITERAL' ) {
            val = '"' + this.value + '"';
        }
        else if( this.type === 'TOKEN' ) {
            val = this.value;
        }
        else if( this.type === 'NODE' ) {
            val = this.name;
        }
        else if( this.type === 'AND' ) {
            if( this.name || this.subnodes.length === 1) {
                val = this.subnodes.map( sn => sn.to_string() ).join(' ');
            } else {
                val = '(' + this.subnodes.map( sn => sn.to_string() ).join(' ') + ')';
            }
        }
        else if( this.type === 'OR' ) {
            if( this.name) {
                val = this.subnodes.map( sn => sn.to_string() ).join('|');
            } else {
                val = '(' + this.subnodes.map( sn => sn.to_string() ).join('|') + ')';
            }
        }

        if( this.repeat ) {
            val = val + this.repeat;
        }
        return val;
    } //to_string

    expect(tokens) {
        // remove any comments
        tokens = tokens.filter( t => t.type !== 'comment' );
        let res = this._expect( tokens );
        if( res[0] ) {
            if( tokens.length > 0 ) {
                console.log( res[1].tokens().map(t=>t.value).join(" ") );
                throw new Error( "Unable to compile. " + tokens.length + " tokens unrecognized" );
            }
            return res[1];
        }
        throw new Error( "Error in compiling. " + tokens.length + " tokens unrecognized" );        
    }
    
    _expect(tokens,inRepeat) {
        if( this.type !== 'LITERAL' && this.type !== 'TOKEN' ) 
            console.log( "EXPECT " +(this.name?"'"+this.name+"'":'') + " (" + this.to_string()+ ") --> [[" + tokens.filter((t,idx)=>idx<21).map(t=>t.value).join(' ') + ']]' );
        let firstTok = tokens.length > 0 ? tokens[0] : undefined;

        if( this.repeat && ! inRepeat ) {
            let matches = [];
            if( this.repeat === '?' ) { //zero or one
                let exp = this._expect( tokens, true );
                if( exp[0] ) {
                    let nodes = exp[1];
                    if( Array.isArray( nodes ) ) {
                        matches.push( ...nodes );
                    } else {
                        matches.push( nodes );
                    }
                }
            }
            else { //zero or more
                while( true ) {
                    let exp = this._expect( tokens, true );
                    if( exp[0] ) {
                        let nodes = exp[1];
                        if( Array.isArray( nodes ) ) {
                            matches.push( ...nodes );
                        } else {
                            matches.push( nodes );
                        }
                    } else {
                        break;
                    }
                }
            }
            console.log( "FOUND " + matches.length + " REPEATS OF " + this.to_string() );
            return [true,matches];
        } //if a repeat

        if( ! firstTok || firstTok.doesNotMatch[this.id] || firstTok.workingOn[this.id] ) {
//            return [false];
        }

        firstTok.workingOn[this.id] = 1;
        
        if( this.type === 'LITERAL' ) {
            if( firstTok.value === this.value ) {
                tokens.shift();
                console.log( 'YES "' + firstTok.value + '"' );
                return [true, new MatcherNode( 'LITERAL', firstTok ) ];
            }
            firstTok.doesNotMatch[this.id] = 1;
            return [false];
        }
        else if( this.type === 'TOKEN' ) {
            if( firstTok.type === this.value ) {
                tokens.shift();
                console.log( 'YES "' + firstTok.value + '"' );
                return [true, new MatcherNode( 'TOKEN', firstTok ) ];
            }
            firstTok.doesNotMatch[this.id] = 1;            
            return [false];
        }
        else if( this.type === 'NODE' ) {
            return this.value._expect( tokens );
        }
        else if( this.type === 'AND' ) {
            firstTok.workingOn[this.id] = 1;
            let andMatcher = new MatcherNode( 'AND', this.name );
            for( let i=0; i<this.subnodes.length; i++ ) {
                let sn = this.subnodes[i];
                let exp = sn._expect( tokens );
                if( exp[0] ) {
                    andMatcher.add( exp[1] );
                } else {
                    let cnt = andMatcher.giveBackTokens( tokens );
                    if( cnt > 0 ) {
                        console.log( "FAILED", sn.to_string(), "GIVING BACK", andMatcher.tokens(true).join(" ") );
                    } else {
                        console.log( "FAILED " + sn.to_string() );
                    }
                    firstTok.doesNotMatch[this.id] = 1;
                    return [false];
                }
            }
            if( this.name ) {
                console.log( "YES '" + this.name + "' ==>  " + andMatcher.tokens().map( t => t.value ).join(' ') );
            } else {
                console.log( "YES " + this.to_string() );
            }
            delete firstTok.workingOn[this.id];            
            return [true,andMatcher];
        }
        else if( this.type === 'OR' ) {
            firstTok.workingOn[this.id] = 1;
            for( let i=0; i<this.subnodes.length; i++ ) {
                let sn = this.subnodes[i];
                let exp = sn._expect( tokens );
                if( exp[0] ) {
                    let matcher;
                    if( this.name ) {
                        matcher = new MatcherNode( 'AND', this.name );
                        matcher.add( exp[1] );
                    } else {
                        matcher = exp[1];
                    }
                    delete firstTok.workingOn[this.id];
                    return [true, matcher ];
                }
            }
            firstTok.doesNotMatch[this.id] = 1;
            return [false];
        }
    } //_expect
} //class ANode


//module.exports = { Analyzer };
