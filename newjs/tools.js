
let matches = [];
const match = ( str, regex ) => {
    matches.length = 0;
    let m = str.match( regex );
    if( m ) {
        m.forEach( ma => matches.push(ma) );
        return true;
    }
    return false;
};
module.exports = { matches, match };
