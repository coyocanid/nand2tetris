let {Tokenizer} = require( "./tokenizer.mjs" );
let {TokenAnalyzer} = require( "./token_analyzer.mjs" );

let t = new Tokenizer( 'js' );
let a = new TokenAnalyzer( 'js' );

let toks = t.tokenize( `
    let /* comment */ foo = "123" /*more comment*/;
 class HelloWorld /*OMMENTy*/ extends /*COMMENT*/ Component.withWatchables("text") {
       dbElements(){
           return e("div","HELLO WORLD");
       }
 }
 render( HelloWorld, "root" ); 
` );

console.log( toks );

toks = t.tokenize( `
     foo[]().bar
` );

console.log( a.analyze_tokens( toks, 'value' ) );
