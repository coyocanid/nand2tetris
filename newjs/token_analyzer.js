import {matches, match} from "./tools.js";

const language_definitions = {
    jack : {
        token_types : [ 'keyword',
                        'symbol',
                        'integerConstant',
                        'identifier',
                        'stringConstant' ],
        named : [ 'class',
                  'classVarDec',
                  'doStatement',
                  'expression',
                  'expressionList',
                  'ifStatement',
                  'letStatement',
                  'parameterList',
                  'returnStatement',
                  'statements',
                  'subroutineBody',
                  'subroutineDec',
                  'term',
                  'varDec',
                  'whileStatement' ],
        composites : {
            statements      : 'statement*',
            statement       : 'letStatement | ifStatement | whileStatement | doStatement | returnStatement',
            letStatement    : "'let' varName ( '[' expression ']' )? '=' expression ';'",
            ifStatement     : "'if' '(' expression ')' '{' statements '}' ( 'else' '{' statements '}' )?",
            whileStatement  : "'while' '(' expression ')' '{' statements '}'",
            doStatement     : "'do' subroutineCall ';'",
            returnStatement : "'return' expression? ';'",
            unaryOp         : "'-' | '~'",
            keywordConstant : "'true' | 'false' | 'null' | 'this' ",
            expressionList  : "(expression ( ',' expression )* )?",
            op              : "'+' | '-' | '*' | '/' | '&' | '|' | '<' | '>' | '='",
            expression      : "term (op term)*",
            term            : "integerConstant | stringConstant | keywordConstant | subroutineCall | varName '[' expression ']'  | varName | '(' expression ')' | unaryOp term",
            subroutineCall : "( className | varName ) '.' subroutineName '(' expressionList ')' | subroutineName '(' expressionList ')'",
            class          : "'class' className '{' classVarDec* subroutineDec* '}'",
            classVarDec    : "('static'|'field') type varName(',' varName)*';'",
            type           : "'int' | 'char' | 'boolean' | className",
            subroutineDec  : "('constructor'|'function'|'method') ('void' | type) subroutineName '(' parameterList ')' subroutineBody",
            parameterList  : "((type varName)(',' type varName)*)?",
            subroutineBody : "'{' varDec* statements '}'",
            varDec         : "'var' type varName (',' varName)* ';'",
            className      : 'identifier',
            subroutineName : 'identifier',
            varName        : 'identifier',
        },
    },
    js : {
        token_types : [ 'keyword',
                        'symbol',
                        'numberConstant',
                        'identifier',
                        'stringConstant' ],
        named : [ 'classDec',
                  'functionDec',
                  'varDec',
                  'returnStat',
                  'ifStat',
                  'whileStat',
                  'forStat',
                  'assignStat',
                  'method',
                  'funCall',
                  'term',
                ],
        composites : {
            statements : "statement*",

            statement  : "( classDec | functionDec | varDec | returnStat | ifStat | whileStat | forStat | assignStat | expression )",

            classDec : " 'class' identifier ( 'extends' reference )? '{' method* '}' ",

            functionDec : "(identifier '=>' (statement | '{' statements '}' ))" + ' | ' +
                "( '(' paramList ')' '=>' ( statement | '{' statements '}' ))" + ' | ' +
                "( 'function' identifier? '(' paramList ')' '{' statements '}' )",

            varDec : "( 'constant' | 'let' | 'var' ) identifier ('=' expression)? ( ',' identifier ('=' expression)? )* ';'",

            expression      : "term (op term)*",

            returnStat      : "'return' expression? ';'",

            ifStat      : "'if' '(' expressionList ')' ( statement | '{' statements '}' ) " + 
                "( 'else' 'if' ( statement | '{' statements '}' ) )* " +
                "( 'else' ( statement | '{' statements '}' ))?",

            whileStat      : "'while' '(' expressionList ')' ( statement | '{' statements '}' ) ",

            forStat      : "'for' '(' expression? ';' expression? ';' expression? ')' ( statement | '{' statements '}' ) ",

            assignStat : "identifier '=' ( expression | functionDec )",

            reference : " variable '[' expression ']' | funCall | variable '.' reference | variable ",

            funCall : "variable '(' expressionList ')'",

            method : "identifier '(' paramList ')' '{' statements '}'",

            paramList  : "(variable ( ',' variable )* )?",

            variable   : "( '...' )? identifier",

            term : "numberConstant | stringConstant | keywordConstant | '(' expression ')' | list | hash | reference | preUnaryOp term | reference postUnaryOp",

            op : "'+' | '-' | '*' | '/' | '&' | '|' | '<' | '>' | '=' | '==' | '===' | '!==' | '+=' | '*=' | '/=' | '>=' | '<=' | '!='  | '&&' | '||' ",

            keywordConstant : "'true' | 'false' | 'null' | 'this' ",

            preUnaryOp : "'-' | '!' | '++' | '--'",

            postUnaryOp : "'--'| '++'",

            list : "'[' ( expression ( ',' expression )* )? ']'",

            hash : "'{' ( ( identifier | stringConstant ) ':' expression ( ',' ( identifier | stringConstant ) ':' expression )* )? '}'",

            expressionList  : "(expression ( ',' expression )* )?",

        },
    },
}; //language definitions

export class TokenAnalyzer {
    constructor( language ) {
        this.defs = language_definitions[language];
        this.COMPILED = {};
        this.NAMED = {};
        this.defs.named.forEach( n => { this.NAMED[n] = 1; } );
        this.is_token = {};
        this.defs.token_types.forEach( tt => { this.is_token[tt] = tt; } );
        this.speculation_level = 0;
    }
    _compile_formula( formula_name ) {
        let { COMPILED, NAMED, is_token } = this;
        let COMPOSITES = this.defs.composites;

        let group = COMPILED[formula_name];
        if ( group ) {
	        return group;
        }
        
        let groups = [];
        
        let formula = COMPOSITES[formula_name];
        if( ! formula ) {
	        throw new Error( "Unknown formula '" + formula_name + "'" );
        }

        group = { name : formula_name,
	              val  : [],
	              type : 'AND',
	              formula : formula,
	              named   : NAMED[formula_name] };

        COMPILED[formula_name] = group;

        while ( formula.match(/\S/) ) {

	        if ( match( formula, /^\s*'([^']+)'([\*\?])?\s*(.*)/ ) ) {
                let compo   = matches[1];
                let repeat  = matches[2];
                formula = matches[3];
                let node = { val : compo, type : 'LITERAL' };
                if ( repeat ) {
		            group.val.push( { val  : node,
				                      type : 'REPEAT',
				                      repeat : repeat,
				                      formula : repeat } );
                } else {
		            group.val.push( node );
                }
            }
	        else if ( match( formula, /^\s*\(\s*(.*)/ ) ) {
                groups.push( group );
                group = { val : [], type : 'AND', name : 'grouping' };
                formula = matches[1];
            }
	        else if ( match( formula, /^\s*\)([\*\?])?\s*(.*)/ ) ) {
                let repeat  = matches[1];
                formula = matches[2];
                let closedgroup = group;
	            if ( closedgroup.parentOR ) {
		            let orgroup = closedgroup.parentOR;
		            if ( group.val.length === 1 ) {
                        orgroup.val.push( group.val[0] );
                    }
		            else {
                        orgroup.val.push( group );
		            }
		            closedgroup = orgroup;
		            groups.pop();
                }

                group = groups.pop();
                if ( repeat ) {
		            group.val.push( { val     : closedgroup,
				                      type    : 'REPEAT',
				                      repeat  : repeat,
				                      formula : repeat } );
                } else {
		            group.val.push( closedgroup );
                }
            }
	        else if ( match( formula, /^\s*\|(.*)/ ) ) {
                formula = matches[1];
	            
                if ( group.parentOR ) {
		            let orgroup = group.parentOR;
		            // check for single 'AND' items
		            if ( group.val.length === 1 ) {
		                orgroup.val.push( group.val[0] );
                    }
		            else {
		                orgroup.val.push( group );
                    }
		            group = orgroup;
                } else {
		            group.type = 'OR';
		            if ( group.val.length > 1 ) {
                        //if more than one thing is already there,
                        // combind it into one AND node as the ors first val
		                group.val = [ { val  : group.val.concat([]),
				                        type : 'AND',
				                        name : group.name,
				                        named : group.named } ];
                    }
		            groups.push( group );
                }
                group = { val  : [],
		                  type : 'AND',
		                  name : 'grouping',
		                  parentOR : group };
            }
	        else if ( match( formula, /^\s*([a-zA-Z]+)([\*\?])?\s*(.*)/ ) ) {
                let part   = matches[1];
                let repeat = matches[2];
                formula    = matches[3];
                if ( is_token[part] ) {
		            if ( repeat ) {
		                group.val.push( { val : { name : part,
					                              val  : part,
					                              type : 'TOKEN' },
				                          type : 'REPEAT',
				                          repeat : repeat,
				                          formula : repeat } );
                    } else {
		                group.val.push( { name : part, val : part, type : 'TOKEN' } );
                    }
                }
                else if ( is_token[COMPOSITES[part]] ) {
		            if ( repeat ) {
		                group.val.push ( { val : { name : part,
					                               val : COMPOSITES[part],
					                               type : 'TOKEN' },
				                           type : 'REPEAT',
				                           repeat : repeat,
				                           formula : repeat } );
                    } else {
                        group.val.push( { name : part, val : COMPOSITES[part], type : 'TOKEN' } );
                    }
                }
                else if ( COMPOSITES[part] ) {
		            let cgroup = this._compile_formula( part );
		            if ( repeat ) {
		                group.val.push( { val     : cgroup,
				                          type    : 'REPEAT',
				                          repeat  : repeat,
				                          formula : repeat }  );
                    } else {
		                group.val.push( cgroup );
                    }
                } else {
		            throw new Error( "Error compiling formula '" + formula_name + "' : Unknown part '" + part + "'" );
                }
            }
        } //while still a formula

        // might have to close a group here now
        let orgroup = groups.pop();
        if ( orgroup ) {
	        if ( group.val.length === 1 ) {
	            orgroup.val.push( group.val[0] );
	        } else {
	            group.length;
                orgroup.val.push( group );
            }
	        group = orgroup;
        }

        return group;
    } //_compile_formula

    _expect_group( group ) {
        // returns [ success?, node, tokens ] where node
        //   is one of the following :
        //  group node { named, type, name, val }
        //   where named is the node forumulat (if any)
        //         type is one of 'REPEAT', 'AND', 'OR'
        //         name is the composite name that created this node, or 'grouping'
        //         val is a list of nodes inside this one
        //         
        //  atom node  { token, type, name, val, line }
        //   where token is the token object
        //         type is 'LITERAL' or 'TOKEN'
        //         val is the token value

        if( group.formula && group.name ) {
	        let peeky = this.tokstore[0] || this.all_tokens[0];
            //console.log( this.indent + "--=="+group.name + "  {" + group.formula + "}   ==-- ************* " + peeky[2] + " *************" );
            this.indent = ' ' + this.indent;
        }

        if ( group.type === 'REPEAT' ) {

	        if ( group.repeat === '?' ) {
                this.speculation_level++;
                let items = [];
	            
	            let res  = this._expect_group( group.val );
	            let got  = res[0];
	            let ret  = res[1];
	            let toks = res[2];

                this.speculation_level--;

                if ( ! got ) {
		            this.tokstore = toks.concat( this.tokstore );
                } else {
		            items.push( ret );
                }

                if( group.formula && group.name ) {
                    this.indent = this.indent.substring( 1 );
                    //console.log( this.indent + "SUCCESS " + group.name + " { " + group.formula + "}", toks.map( e => e[2] ).join(' ') );
                }

                if ( this.speculation_level === 0 ) {
		            return [ 1, { i  : 'A', type : group.type, named : group.named, name : group.name, val : items }, [] ];
                }
                return [ 1, { i : 'B', named : group.named, type : group.type, name : group.name, val : items }, toks ];
            }
	        else if ( group.repeat === '*' ) {
                let rtoks = [];
                let repeats = [];
                while (1) {
		            this.speculation_level++;
		            let res  = this._expect_group( group.val );
		            let got  = res[0];
		            let rep  = res[1];
		            let toks = res[2];
		            this.speculation_level--;
		            if ( got ) {
		                if( rep !== undefined ) { 
			                repeats.push( rep );
		                } else {
			                try {throw new Error("BEEEG"); } catch(e){} }
		                rtoks = rtoks.concat( toks );
		            } else {
		                this.tokstore = toks.concat( this.tokstore );
		                break;
		            }
                }
                if( group.formula && group.name ) {
                    this.indent = this.indent.substring( 1 );
                    //console.log( this.indent + "SUCCESS " + group.name + " {" + group.formula + " }", rtoks.map( e => e[2] ).join(' ') );
                }

                if ( this.speculation_level === 0 ) {
		            return [1, { i : 'C', named : group.named, type : group.type, name : group.name, val : repeats }, []];
                }
                return [ 1, { i : 'D', named : group.named, type : group.type, name : group.name, val : repeats }, rtoks ];
	        }
        }
        else if ( group.type === 'AND' ) {
	        let rtoks = [];
	        let succ = [];
	        for( let i=0; i<group.val.length; i++ ) {
	            let grp = group.val[i];

	            let res  = this._expect_group( grp );
	            let got  = res[0];
	            let val  = res[1];
	            let toks = res[2];

	            rtoks = rtoks.concat( toks );
                if ( got ) {
		            if( val ) {
		                succ.push( val );
		            }
                } else {
		            if ( this.speculation_level > 0 ) {
		                this.tokstore = rtoks.concat( this.tokstore );
                    }
		            if( this.lasterr === undefined && grp.formula ) {
		                this.lasterr = "Unable to find "+grp.name+" at line " + this.linenum;
		            }
                    if( group.formula && group.name ) {
                        this.indent = this.indent.substring( 1 );
                        //console.log( this.indent + "FAIL " + group.name + "  {" + group.formula + " } err : " + this.lasterr );
                    }

		            return [0, undefined, []];
                }
            }

	        this.lasterr = undefined;

            if( group.formula && group.name ) {
                this.indent = this.indent.substring( 1 );
                //console.log( this.indent + "SUCCESS " + group.name + " {" + group.formula + " } ", rtoks.map( e => e[2] ).join(' ') );
            }

	        return [ 1, { i : 'E', named : group.named, type : group.type, name : group.name, val : succ }, rtoks ];
        }
        else if ( group.type === 'OR' ) {
	        this.speculation_level++;
	        for( let i=0; i<group.val.length; i++ ) {
	            let groupcand = group.val[i];

	            let res  = this._expect_group( groupcand );
	            let success  = res[0];
	            let val      = res[1];
	            let toks     = res[2];
                // the toks go back
                if ( success ) {
		            this.speculation_level--;

                    if( group.formula && group.name ) {
                        this.indent = this.indent.substring( 1 );
                        //console.log( this.indent + "SUCCESS " + group.name + " { " + group.formula + " }", toks.map( e => e[2] ).join(' ') );
                    }

		            if ( this.speculation_level == 0 ) {
		                // can throw out the used tokens if not speculating
		                if ( groupcand.named ) {
			                return [ 1, { i : 'F', named : group.named, type : group.type, name : group.name, val : val, }, [] ];
		                }
		                return [1, val, [] ];
		            }
		            if ( group.named ) {
		                return [ 1, { i : 'F', named : group.named, type : group.type, name : group.name, val : val, }, toks ];
		            }
		            this.lasterr = undefined;
		            return [ 1, val, toks ];
                }
                // okey, if a failed loop, those tokens should be recycled
                // back to the hopper
	            this.tokstore = toks.concat( this.tokstore );
	        }
	        this.speculation_level--;

	        if( this.lasterr === undefined && group.formula ) {
	            this.lasterr = "Unable to find "+group.name+" at line "+ this.linenum;
	        }

            if( group.formula && group.name ) {
                this.indent = this.indent.substring( 1 );
                //console.log( this.indent + "FAIL " + group.name + " { " + group.formula + " } err :" + this.lasterr );
            }

	        return [ 0, undefined, [] ];
        }
        else if ( group.type === 'LITERAL' ) {

	        let tok = this.tokstore.shift() || this.all_tokens.shift();
	        if ( tok ) {
	            if( this.linenum === undefined ) {
		            this.linenum = tok[1];
	            }
                if ( tok[2] === group.val && ( tok[0] === 'symbol' || tok[0] === 'keyword' ) ) {
		            this.maxlinenum = tok[1] > (this.maxlinenum||0) ? tok[1] : this.maxlinenum;

		            return [ 1, { i : 'H', token : tok, type : group.type, name : group.name, val : tok[2], line : tok[1] }, [tok] ];
                }
	            if( this.lasterr === undefined ) {
		            this.lasterr = "Literal '"+group.val+"' not found in line '"+tok[1]+"'. Got '" + tok[2] + "' instead";
	            }
	        }
	        return [0, undefined, [tok]];
        }
        else if ( group.type === 'TOKEN' ) {
	        let tok = this.tokstore.shift() || this.all_tokens.shift();
	        if ( tok ) {
	            if( this.linenum === undefined ) {
		            this.linenum = tok[1];
	            }
                if ( tok[0] === group.val ) {
		            this.maxlinenum = tok[1] > (this.maxlinenum||0) ? tok[1] : this.maxlinenum;

		            return [ 1, { i : 'I', token : tok, type : group.type, name : group.name, val : tok[2], line : tok[1] }, [tok] ];
                }
	            if( this.lasterr === undefined ) {
		            this.lasterr = group.val + " not found in line '" + tok[1] + "', got '" + tok[0] + "'";
	            }
	        }
	        return [0, undefined, [tok] ];
        }
        else {
	        throw new Error( "Unknown formula type "+group.type );
        }
    } //_expect_group

    analyze_tokens( tokens, formula ) {
        this.speculation_level = 0;
        this.all_tokens = [...tokens];
        this.tokstore   = [];
        this.linenum    = 0;
        this.maxlinenum = 0;
        this.lasterr    = 0;
        this.indent = '';
        return this._expect_group( this._compile_formula( formula ) );
    } //analyze_tokens

} //TokenAnalyzer class

/*

 FOR THE forumlas, there is always an implied grouping
  If there are | bars, it means an or group. If none, then
  an and group. If a mix, something is wrong.
 Forumla --> group.
  group fields
    repeat '*' or '?'
    type 'AND', 'OR', 'LITERAL', 'TOKEN', 'REPEAT'
    val : list of groups or literal or token value
*/
