let {Tokenizer} = require( "./tokenizer.js" );
let {Analyzer} = require( "./analyzer.js" );


/*
let readline = require('readline');
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});
rl.on('line', line => {
    console.log( line );
} );
*/

//export
class Javascript {
    constructor() {
        this.tokenizer = new Tokenizer( {
            lineComment : /^\s*(\/\/.*)/,
            commentStart : /^\s*(\/\*)(.*)/,
            commentEnd   : /^(.*?\*\/)(.*)/,
            textStart    : /^\s*([\'\"\`])(.*)/,
            symbol_types : ['keyword',
                            'symbol',
                            'numberConstant',
                            'identifier'],
            types : {
                keyword : /^\s*(abstract|arguments|await|boolean|break|byte|case|catch|char|class|const|continue|debugger|default|delete|do|double|else|enum|eval|export|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|let|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|volatile|while|with|yield)\b(.*)/,
                symbol : /^\s*(\.\.\.|[=\!]==|\!=|=\>|\&\&|\+\+|\-\-|\|\||[<>]=|[-{}()\*\[\]\.;+\/\&\|<>=~,:!?^])(.*)/,
                numberConstant : /^\s*(\d+(?:\.\d*)?|\.\d+)(.*)/,
                identifier      : /^\s*([_a-zA-Z][_a-zA-Z0-9]*)(.*)/,
            },
        });
        this.analyzer = new Analyzer( {
            token_types : [ 'keyword',
                            'symbol',
                            'numberConstant',
                            'identifier',
                            'stringConstant' ],
            named : [ 'program',
                      'block',
                      'class',
                      'function',
                      'declare',
                      'return',
                      'if',
                      'while',
                      'try',
                      'throw',
                      'for',
                      'term',
                    ], //named
            composites : {
                program : " statements ",
                
                statements : "statement*",

                statement  : "class | declare | return | if | throw | try | while | for | expression | ';'",

                block : "'{' statements '}'",

                varList : " '(' (identifier (',' identifier)* ','?)? ')' ",

                mblock : " block | statement ",

                class : " 'class' identifier ( 'extends' expression )? '{' method* '}' ",

                method : "'static'? identifier varList block",
                
                declare : "( 'constant' | 'let' | 'var' ) identifier ('=' expression)? ( ',' identifier ('=' expression)? )*",

                return : " 'return' expression? ",

                if : " 'if' '(' expression ')' mblock ( 'else' 'if' '(' expression ')' mblock )? ( 'else' mblock )? ",

                throw : " 'throw' expression ",

                try : " 'try' block ('catch' varList block)* ('finally' | ('catch' varList)) block ",

                while : " 'while' '(' expression ')' mblock ",
                
                for : " 'for' '(' declare? ';' expression? ';' expression? ')' mblock ",

                ternary : "term '?' term ':' term",
                
                expression : " ternary | (term (op term)*) ",

                op : " '===' | '!==' | '>=' | '<=' | '!='  | '&&' | '||' | '==' | '+' | '-' | '*' | '/' | '&' | '|' | '<' | '>' | '=' ",

                new : " 'new' expression paramList",

                expressionList : " ( expression ( ',' expression )* ',' ? )? ",
                
                paramList : " '(' expressionList ')' ",

                term : "new | function | hash | list | numberConstant | stringConstant | keywordConstant | ( ( '(' expression ')' ) junk ) | (preUnaryOp term) | (identifier postUnaryOp) | (identifier junk)",

                preUnaryOp : "'-' | '!' | '++' | '--'",
                
                postUnaryOp : "'--'| '++'",

                junk : " ( braceIdx | dotIdx | paramList )* ",

                braceIdx : " '[' expression ']' ",
                
                dotIdx : " '.' identifier ",

                hash : " '{' (hpair (',' hpair)* ','?)? '}' ",
                
                hpair : " expression (':' expression)?",

                list : "'[' expressionList ']'",

                function : " ((identifier | varList | varList) '=>' mblock ) | (function identifier? block )",
                
                keywordConstant : "'true' | 'false' | 'null' | 'this' ",

            } , //composites
        } );
    } //constructor

    tokenize( program ) {
        return this.tokenizer.tokenize( program );
    }
    get_tokenizer() {
        return this.tokenizer;
    }
    get_analyzer() {
        return this.analyzer.get_analyzer('program');
    }
    build_parse_tree( program ) {
        let tokens = Array.isArray(program) ? program : this.tokenizer.tokenize( program );
        tokens = tokens.filter( t => t.type !== 'comment' );
        console.log( tokens.map(t=>t.value).join(" ") );        
        //        console.log( tokens.map(t=>t.type+ ":" + t.value).join("\n") );
        return this.analyzer.get_analyzer('program').expect( tokens );
    }
    
    
} // Javascript class

module.exports = { Javascript };
