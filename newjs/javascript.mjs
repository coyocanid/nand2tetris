let {Tokenizer} = require( "./tokenizer.mjs" );
let {Analyzer} = require( "./analyzer.js" );


/*
let readline = require('readline');
let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});
rl.on('line', line => {
    console.log( line );
} );
*/

class Javascript {
    constructor() {
        this.tokenizer = new Tokenizer( {
            lineComment : /^\s*(\/\/.*)/,
            commentStart : /^\s*(\/\*)(.*)/,
            commentEnd   : /^(.*?\*\/)(.*)/,
            textStart    : /^\s*([\'\"\`])(.*)/,
            symbol_types : ['keyword',
                            'symbol',
                            'numberConstant',
                            'identifier'],
            types : {
                keyword : /^\s*(abstract|arguments|await|boolean|break|byte|case|catch|char|class|const|continue|debugger|default|delete|do|double|else|enum|eval|export|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|let|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|volatile|while|with|yield)\b(.*)/,
                symbol : /^\s*(\.\.\.|[=\!]==|\!=|\&\&|\|\||[<>]=|[-{}()\*\[\]\.;+\/\&\|<>=~,:!?^])(.*)/,
                numberConstant : /^\s*(\d+(?:\.\d*)?|\.\d+)(.*)/,
                identifier      : /^\s*([_a-zA-Z][_a-zA-Z0-9]*)(.*)/,
            },
        });
        this.analyzer = new Analyzer( {
            token_types : [ 'keyword',
                            'symbol',
                            'numberConstant',
                            'identifier',
                            'stringConstant' ],
            named : [ 'program',
                      'block',
                      'classDec',
                      'functionDec',
                      'varDec',
                      'returnStat',
                      'ifStat',
                      'whileStat',
                      'forStat',
                      'assignStat',
                      'method',
                      'functionCall',
                      'term',
                    ], //named
            composites : {
                program : " statements ",
                
                statements : "statement*",

                statement  : "( classDec | functionDec | varDec | returnStat | ifStat | throwStat | whileStat | forStat | assignStat | functionCall | expression  ) ';'?",

                classDec : " 'class' identifier ( 'extends' expression )? '{' method* '}' ",

                expression : "term (op term)*",
                
                term : "('new' identifier '(' paramList ')')| numberConstant | stringConstant | keywordConstant | ( '(' expression ')' ) | functionCall |( ( unaryOp | inFixOp ) expression ) |( identifier inFixOp ? ) | list | hash ",
                
                op : " '===' | '!==' | '>=' | '<=' | '!='  | '&&' | '||' | '==' | '+' | '-' | '*' | '/' | '&' | '|' | '<' | '>' | '=' | '.' ",
                
                method : "'static'? identifier '(' paramList ')' '{' statements '}'",
                
                keywordConstant : "'true' | 'false' | 'null' | 'this' ",

                unaryOp : "'-' | '!'",
                
                inFixOp : "'--'| '++'",

                paramList  : "(param ( ',' param )* )?",
                
                param   : "(( '...' ? identifier) | list | hash | expression )",
                
                x   : "( identifier | symbol  ) ';'?",

                functionDec : "(identifier '=>' (statement | ( '{' statements '}' ) ))" + ' | ' +
                    "( '(' paramList ')' '=>' ( statement | ( '{' statements '}' ) ))" + ' | ' +
                    "( 'function' identifier? '(' paramList ')' '{' statements '}' )",

                varDec : "( 'constant' | 'let' | 'var' ) (identifier|hash|list) ('=' expression)? ( ',' identifier ('=' expression)? )*",

                returnStat      : "'return' expression?",

                ifStat      : "'if' '(' expression ')' ( statement | ( '{' statements '}' ) ) " + 
                    "( 'else' 'if' '(' expression ')' ( statement | ( '{' statements '}' ) ) )* " +
                    "( 'else' ( statement | ( '{' statements '}' ) ))?",

                throwStat      : "'try' '{' statements '}' ( 'catch' '(' paramList ')' '{' statements '}' )? ",
                
                whileStat      : "'while' '(' expression ')' ( statement | ( '{' statements '}' ) ) ",

                forStat      : "'for' '(' expression? ';' expression? ';' expression? ')' ( statement | ( '{' statements '}' ) ) ",

                assignStat : "identifier '=' ( expression | functionDec | functionCall )",
                
                expressionList  : "(expression ( ',' expression )* )?",

                functionCall : "identifier '(' paramList ')'",

                list : "'[' ( expression ( ',' expression )* )? ',' ? ']'",

                hash : "'{' ( ( identifier | stringConstant ) (':' expression )? ( ',' ( identifier | stringConstant ) (':' expression)? )* )? ',' ? '}'",

            } , //composites
        } );
    } //constructor

    tokenize( program ) {
        return this.tokenizer.tokenize( program );
    }
    get_tokenizer() {
        return this.tokenizer;
    }
    get_analyzer() {
        return this.analyzer.get_analyzer('program');
    }
    build_parse_tree( program ) {
        let tokens = Array.isArray(program) ? program : this.tokenizer.tokenize( program );
        tokens = tokens.filter( t => t.type !== 'comment' );
        console.log( tokens.map(t=>t.type+ ":" + t.value).join("\n") );        
        //        console.log( tokens.map(t=>t.type+ ":" + t.value).join("\n") );
        let programNode = this.analyzer._compile_formula( 'program' );
        let result = programNode.expect( tokens );
        if( result[0] ) {
            return result[1][0];
        }
    }
    
    
} // Javascript class

module.exports = { Javascript };
