let {matches, match} = require( "./tools.mjs" );

const language_definitions = {
    js : {
        lineComment : /^\s*(\/\/.*)/,
        commentStart : /^\s*(\/\*)(.*)/,
        commentEnd   : /^(.*?\*\/)(.*)/,
        textStart    : /^\s*([\'\"\`])(.*)/,
        symbol_types : ['keyword',
                        'symbol',
                        'numberConstant',
                        'identifier'],
        types : {
            keyword : /^\s*(abstract|arguments|await|boolean|break|byte|case|catch|char|class|const|continue|debugger|default|delete|do|double|else|enum|eval|export|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|let|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|volatile|while|with|yield)\b(.*)/,
            symbol : /^\s*([-{}()\*\[\]\.;+\/\&\|<>=~,])(.*)/,
            numberConstant : /^\s*(\d+(?:\.\d*)?|\.\d+)(.*)/,
            identifier      : /^\s*([_a-zA-Z][_a-zA-Z0-9]*)(.*)/,
        },
    },

};

class Token {
    constructor(type,line,value,last_token) {
        this.type  = type;
        this.line  = line;
        this.value = value;
        this.nextToken;
        this.doesNotMatch = {};
        this.doesMatch = {};
    }
}

class Tokenizer {
    constructor( language ) {
        this.defs = typeof language === 'object' ? language : language_definitions[language];
    }
    tokenize( text ) {
        let lines = text.split( /[\n\r]+/ );
        let tokens = [];

        let inComment = false;
        let inText = false;
        let inTextValue = '';

        let {lineComment,commentStart,commentEnd,textStart,symbol_types,types} = this.defs;
        
        lines.forEach( (raw_line,linecount) => {
            let line = raw_line;
            while( line.match(/\S/) ) {
                if( inComment ) {
                    if( match( line, commentEnd ) ) {
                        tokens.push( new Token('comment', linecount, inComment + matches[1]) );
                        inComment = false;
                        line = matches[2];
                    } else {
                        inComment = inComment + line + '\n';
                        line = '';
                    }
                }
                else if( inText ) {
                    if( match( line, inText ) ) {
                        tokens.push( new Token('stringConstant', linecount, inTextValue + matches[1] ) );
                        inText = false;
                        line = matches[2];
                    } else {
                        inTextValue = inTextValue + line + "\n";
                        line = '';
                    }
                }
                else if( match( line, textStart) ) {
                    inText = new RegExp( "^([^" + matches[1] + ']*)' + matches[1] + '(.*)' );
                    line = matches[2];
                }
                else if( match( line, commentStart) ) {
                    inComment = matches[1];
                    line = matches[2];
                }
                else if( match( line, lineComment) ) {
                    tokens.push( new Token('comment', linecount, matches[1]) );
                    line = '';
                }
                else {
                    let found = false;
                    for( let i in symbol_types ) {
                        let type = symbol_types[i];
                        let re = types[type];
                        if( match( line, re ) ) {
                            tokens.push( new Token(type, linecount, matches[1]) );
                            found = true;
                            line = matches[2];
                            break;
                        }
                    }
                    if( ! found ) {
                        throw new Error( "Cant tokenize '" +line + "'" );
                    }
                }
            }
        } );
        return tokens;
    }
} //tokenizer class

module.exports = { Tokenizer };

