let {Javascript} = require( "./javascript.js" );

let js = new Javascript();

let ana = js.get_analyzer();
let t   = js.get_tokenizer();

test_tokenizer();
test_composites();
test_javascript();

function test_javascript() {


    let tree;
    if( true ) {
        tree = js.build_parse_tree( `sn.subnodes.map( sn => sn.parent = this );` );
        console.log( "TREE", tree.to_string() );
        console.log( '-----------------------------' );
    }
    else {
        tree = js.build_parse_tree( `let {matches, match} = require( "./tools.mjs" );

let nodes = [];

let depth = 1;

class CompositeNode {
    constructor( type, args ) {
        this.type = type;
        if( args ) {
            // LITERAL or TOKEN or composite node name
            this.value  = args.value;

            // parent node of this one
            this.parent = args.parent;

            // * or ? for zero or more, or zero or one
            this.repeat = args.repeat;

            // compiled composite
            this.composite_node   = args.composite_node;

            this.assumed_group = false;

            // if this node has a name
            // then on analyzis, it produces a
            // compiled node
            this.name   = args.name;
            
            // debugging stuff
            this.isTop  = args.isTop;
            this.parentID = args.parentID;
            this.depth  = args.depth;
        }
        nodes.push( this );
        this.id       = nodes.length;
        this.subnodes = [];
    }
    static start(name) {
        return new CompositeNode('AND',{isTop:true,name:name,depth:1});
    }
    squish() {return;
        // if a node has a single child, have this node
        // assume the identity of that child (repeat value, type, value, node
        if( this.subnodes.length === 1 ) {
            let sn = this.subnodes[0];
            if( (sn.type === 'AND' || sn.type === 'OR') && (!this.repeat) ) {
                this.subnodes       = [...sn.subnodes];
                this.repeat = sn.repeat;
                this.type  = sn.type;
                this.value = sn.value;
                this.composite_node  = sn.composite_node;
                this.name  = this.name || sn.name;
                if( this.subnodes.length === 1 ) {
                    this.squish();
                    return;
                }
            }
        }
        if( this.type === 'AND') {
            // flatten AND subnodes of a node
            let sns = this.subnodes;
            for( let i=0; i<sns.length; i++ ) {
                let sn = sns[i];
                if( sn.type === 'AND' && ! sn.repeat) {
                    sn.subnodes.map( sn => sn.parent = this );
                    sns.splice(i,1,...sn.subnodes);
                    i = i + (sn.subnodes.length-1);
                }
            }
        }
        this.subnodes.map( sn => {
            sn.parent = this;
            sn.squish();
        } );
    }
    set_repeat( rep ) {
        if( rep ) {
            this.repeat = rep;
            // the opposite of squishing.
            // make a subnode to carry the subnodes of this one.
            let sn = [...this.subnodes];
            
            let repeater_box = this.extend();
            repeater_box.type = this.type;
            repeater_box.compiled_composite = this.compiled_composite;
            this.type = 'AND';
            this.subnodes = [ repeater_box ];
            repeater_box.subnodes = sn;
            
            sn.map( s => {
                s.parent = repeater_box;
                s.parentID = repeater_box.id;
            } );
        }
    }
    attach( node ) {
        node.parent = this;
        node.parentID = this.id;
        node.depth = this.depth + 1;
        if( node.id === this.id ) throw new Error("Cant attach a node to itself" );
        this.subnodes.push( node );
    }
    close() {
        return this.parent;
    }
    close_if_assumed() {
        if( this.assumed_group ) {
            return this.parent;
        }
        return this;
    }
    to_or() {
        if( this.type === 'AND' ) {
            this.type = 'OR';
            if( this.subnodes.length > 1 ) {
                let new_and = this.extend();
                new_and.subnodes = this.subnodes.filter( sn => sn.id !== new_and.id );
                this.subnodes = [new_and];
            }
        }
        return this;
    }
    extend() {
        let newnode = new CompositeNode('AND');
        this.attach(newnode);
        return newnode;
    }
    add_literal( value, repeat ) {
        let newnode = new CompositeNode('LITERAL',{value,repeat});
        this.attach(newnode);
        return newnode;

    }
    add_token( value, repeat ) {
        let newnode = new CompositeNode('TOKEN',{value,repeat});
        this.attach(newnode);
        return newnode;
    }
    add_node( composite_node, nodename, repeat ) {
        let newnode = new CompositeNode('NODE',{value:nodename,composite_node,name:nodename,repeat});
        this.attach(newnode);
        return newnode;
    }
    val() {
        return typeof this.value === 'object' ? this.value.val() : this.value !== undefined ? this.value : '';
    }
    to_string() {
        //        return this.id + ":" + this._to_string();
        return this._to_string();
    }
    _to_string(seen) {
        // when a CompositeNode is compiled from a formula, it should be able to recreate that
        // formula exactly the same (barring whitespace and possibly extraneous groupings written into it )
        let val;
        seen = seen || {};
        if( seen[this.id] ) {
            throw new Error( "stringified " + this.id + " already" );
        }
        seen[this.id] = this;

        if( this.type === 'AND' ) {
            val = this.subnodes.map(sn=>sn._to_string(seen)).filter(a=>a.match(/\S/)).join(" ");
            if( ( this.isTop && this.repeat && this.subnodes.length > 1 ) || (!this.isTop && this.subnodes.length > 1) )
                val = "(" + val + ")";
        }
        else if( this.type === 'OR' ) {
            val = this.subnodes.map(sn=>sn._to_string(seen)).filter(a=>a.match(/\S/)).join("|");
            if( ! this.isTop || this.repeat )
                val = "(" + val + ")";
        }
        else if( this.type === 'LITERAL' ) {
            val = '"' + this.value + '"';
        }
        else {
            val = this.value;
        }
        if( this.repeat ) {
            val = val +  this.repeat;
        }
        return val;
    } //_to_string

    expect( tokens ) {
        const toks = [...tokens];
        return this._expect( tokens );
    }

    topSolid() {
        if( this.isTop ) return this;
        if( this.parent.type === 'OR' || this.parent.repeat ) {
            return this.parent;
        }
        return this.parent.topSolid();
    }

    
    _expect( tokens, inRepeat, noPrint ) {
        let firstToken;
        if( tokens.length > 0 ) {
            firstToken = tokens[0];
            //            console.log( "CHECK "+this.id+" against [" + Object.keys(firstToken.doesNotMatch).join(" ")+']');
            let doesmatch = firstToken.doesMatch[this.id];
            if( doesmatch && firstToken.doesNotMatch[this.id] ) {
                throw new Error( "BOTH MATCHES AND NOT" );
            }
            if( doesmatch ) {
                tokens.splice( 0, doesmatch[1] );
//                console.log( 'ALREADYMATCHED ' + this.to_string() );
//                return [true,doesmatch[0]];
            }
            if( firstToken.doesNotMatch[this.id] ) {
                //console.log( 'NYET ' +  firstToken.value + " for " + this.to_string() );
                return [false];
            }
        } else { //can't match no token
            return [false];
        }
        depth++;
//        console.log( "DEPTH " + depth );
        if( this.repeat && ! inRepeat) {
            let ret = [];
            if( ! this.type === 'NODE' && this.subnodes.length !== 1 ) {
                throw new Error("OH NOES " + this.subnodes.length + "|" + this.type + "" + this.to_string());
            }
            let sn = this.composite_node || this.subnodes[0];
            if( ! sn ) {
                sn = this;
                inRepeat = true;
            }
            if( this.repeat === '*' ) {
                while( true ) {
                    let snode = sn._expect( tokens, inRepeat );
                    if( ! snode[0] ) {
//                        firstToken.doesNotMatch[this.id] = 1;
                        break;
                    }
                    let sret = snode[1];
                    ret.push( ...sret );
                }
            }
            else { //'?'
                let qnode = sn._expect( tokens, inRepeat );
                if( qnode[0] ) {
                    ret.push( qnode[1][0] );
                } else {
//                    firstToken.doesNotMatch[this.id] = 1;
                }
            }
            depth--;
            return [true,ret];
        }
        if( this.type === 'LITERAL' || this.type === 'TOKEN' ) {
            let tokNodes = [];
            while( true ) {
                let token = tokens.shift();
                let tokstr = tokens.map( t => t.type === 'comment' || t.type === 'stringConstant' ? t.type : t.value ).join(" ");
                if( ! token ) {
                    throw new Error( "OUT OF TOKENS" );
                }
                if( token && token.type === 'comment' ) {
                    tokNodes.push( new CompiledNode( 'COMMENT', {token}) );
                }
                else {
                    if( token && ((this.type==='LITERAL' && this.value === token.value) ||
                                  (this.type==='TOKEN'   && this.value === token.type))  )
                    {
                        let token_type = this.type === 'LITERAL' ? 'literal' : token.type;
                        tokNodes.push( new CompiledNode( this.type, { token, token_type, composite_node: this } ) );
                        //console.log( depth + " FOUND "+token.value+"("+token.type+")\t <<"+tokstr+">>\tFOR " + this.topSolid().to_string() + "" );
                        depth--;
//                        firstToken.match( this.id, tokNodes );
                        return [true,tokNodes];
                    }
                    for( let i=(tokNodes.length-1); i>=0; i-- ) {
                        tokNodes[i].returnTokens( tokens );
                    }
//                    console.log( depth + " WRONG TOKEN "+token.value+"("+token.type+") <<"+tokstr+">>FOR {" + this.value + "} " + this.topSolid().to_string() + "" );
                    tokens.unshift( token );
                    depth--;
//                    firstToken.doesNotMatch[this.id] = 1;
                    //console.log( "CLOSING OFF " + this.to_string() + "(" + firstToken.value + ")" );
                    return [false];
                }
            }
        }
        else if( this.type === 'NODE' ) {
//            console.log( depth + " NODE SEEKING " + this.to_string() );
            let ret = this.composite_node._expect( tokens );
            if( ret[0] ) {
                //console.log( depth + " NODE FOUND " + this.to_string(), '['+ret[1][0].tokens().join(' ')+']' );
                //console.log( "\tREMAINING : " + tokens.map( t => t.type === 'comment' || t.type === 'stringConstant' ? t.type : t.value ).join(" ") );
                depth--;
//                firstToken.match( this.id, ret[1] );
                return [true,ret[1]];
            }
            //console.log( depth + " NODE DID NOT FIND " + this.to_string() );
            depth--;
            firstToken.doesNotMatch[this.id] = 1;
            //console.log( "CLOSING OFF " + this.to_string() + "(" + firstToken.value + ")" );            
            return [false];
        }
        else if( this.type === 'AND' ) {
            let print = this.subnodes.length > 1 || (! noPrint);
            if( print ) {
                //console.log( depth + " AND SEEKING " + this.to_string() );
            }
            let andNode = new CompiledNode( 'AND', { composite_node: this } );
            for( let i in this.subnodes ) {
                let sn = this.subnodes[i];
                let opNode = sn._expect( tokens );
                if( opNode[0] ) {
                    let ret = opNode[1];
                    andNode.subnodes.push( ...ret );
                }
                else {
                    if( print) {
                        //console.log( depth + " AND DID NOT FIND " + this.to_string() );
                    }
                    andNode.returnTokens( tokens );
                    depth--;
                    firstToken.doesNotMatch[this.id] = 1;
                    //console.log( "CLOSING OFF " + this.to_string() + "(" + firstToken.value + ")" );
                    return [false];
                }
            }
            if( !print ) {
                //console.log( depth + " AND SEEKING " + this.to_string() );
            }
            //console.log( depth + " AND FOUND " + this.to_string(), '['+andNode.tokens().join(' ')+']' );
            //console.log( "\tREMAINING : " + tokens.map( t => t.type === 'comment' || t.type === 'stringConstant' ? t.type : t.value ).join(" ") );
            depth--;
//            firstToken.match( this.id, andNode );
            return [true,[andNode]];
        }
        else if( this.type === 'OR' ) {
            //console.log( depth + " OR SEEKING " + this.to_string() );

            firstToken.doesNotMatch[this.id] = 1; //assume no. fix if it does match
            for( let i in this.subnodes ) {
                let sn = this.subnodes[i];
                let opNode = sn._expect( tokens, false, true );
                if( opNode[0] ) {
                    //console.log( depth + " OR FOUND " + this.to_string(), "WITH", sn.to_string(), '['+opNode[1][0].tokens().join(' ')+']' );
                    //console.log( "\tREMAINING : " + tokens.map( t => t.type === 'comment' || t.type === 'stringConstant' ? t.type : t.value ).join(" ") );
                    depth--;
                    delete firstToken.doesNotMatch[this.id]; //found a match, so yes
//                    firstToken.match( this.id, opNode[1] );
                    //console.log( "REOPENING " + this.to_string() + "(" + firstToken.value + ")" );
                    return [true,opNode[1]];
                }
            }
            //console.log( depth + " OR DID NOT FIND " + this.to_string() );
            depth--;
            //console.log( "CLOSING OFF " + this.to_string() + "(" + firstToken.value + ")" );
            firstToken.doesNotMatch[this.id] = 1;
            return [false];
        }
        throw new Error( "WEHA");
    } //_expect

} //CompositeNode

let ccount = 0;
class CompiledNode {
    constructor(type,args) {
        this.type = type;
        if( args ) {
            this.composite_node = args.composite_node;
            this.token      = args.token;
            this.token_type = args.token_type;
        }
        this.id = ++ccount;
        this.subnodes = [];
    }
    tokens() {
        if( this.type === 'LITERAL' || this.type === 'TOKEN' || this.type === 'COMMENT') 
            return [this.token.value];
        let ret = [];
        for( let i=0; i<this.subnodes.length; i++ ) {
            let toks = this.subnodes[i].tokens();
            ret.push( ...toks );
        }
        return ret;
    }
    returnTokens(tokens) {
        if( this.type === 'LITERAL' || this.type === 'TOKEN' || this.type === 'COMMENT') {
            tokens.unshift( this.token );
        }
        else {
            // most recent give back first
            for( let i=(this.subnodes.length-1); i>=0; i-- ) {
                this.subnodes[i].returnTokens( tokens );
            }
        }
    }
    to_string(indent,icount) {
        indent = indent ? indent : '';
        icount = icount || 1;
        let compo = this.composite_node;
        if( compo ) { //otherwise a comment
            if( compo.type === 'LITERAL' || compo.type === 'TOKEN' ) {
                //                return icount + indent + "<" + this.token.type + ">" + this.token.value + "</" + this.token.type + ">";
                return indent + "<" + this.token.type + ">" + this.token.value + "</" + this.token.type + ">";
            }
            else {
                let txt = '';
                if( compo.name ) {
                    //                    txt = txt + icount + indent + "<" + compo.name + ">";
                    txt = txt + indent + "<" + compo.name + ">";
                }
                this.subnodes.map( sn => txt = txt + sn.to_string(indent+' ',1+icount) );
                if( compo.name ) {
                    txt = txt + indent + "</" + compo.name + ">";
//                    txt = txt + icount + indent + "</" + compo.name + ">";
                }
                return txt;
            }
        } else {
            //            return icount +  indent + "<comment>" + this.token.value + "</comment>";
            return indent + "<comment>" + this.token.value + "</comment>";            
        }
        return compo.type;
    }
} //CompiledNode


const language_definitions = {
    js : {
        token_types : [ 'keyword',
                        'symbol',
                        'numberConstant',
                        'identifier',
                        'stringConstant' ],
        named : [ 'program',
                  'block',
                  'classDec',
                  'functionDec',
                  'varDec',
                  'returnStat',
                  'ifStat',
                  'whileStat',
                  'forStat',
                  'assignStat',
                  'method',
                  'functionCall',
                  'term',
                ],
        composites : {
            program : " statements ",
            
            statements : "statement*",

            statement  : "( classDec | functionDec | varDec | returnStat | ifStat | whileStat | forStat | assignStat | functionCall | expression  ) ';'?",

            classDec : " 'class' identifier ( 'extends' expression )? '{' method* '}' ",

            expression : "term (op term)*",
            
            term : "numberConstant | stringConstant | keywordConstant | ( '(' expression ')' ) | functionCall |( ( unaryOp | inFixOp ) identifier ) |( identifier inFixOp ? )  ",
            
            op : "'+' | '-' | '*' | '/' | '&' | '|' | '<' | '>' | '=' | '==' | '===' | '!==' | '+=' | '*=' | '/=' | '>=' | '<=' | '!='  | '&&' | '||' | '.' ",
            
            method : "identifier '(' paramList ')' '{' statements '}'",
            
            keywordConstant : "'true' | 'false' | 'null' | 'this' ",

            unaryOp : "'-' | '!'",
            
            inFixOp : "'--'| '++'",

            paramList  : "(param ( ',' param )* )?",
            
            param   : "(( '...' ? identifier) | list | hash | expression )",
            
            x   : "( identifier | symbol  ) ';'?",

            functionDec : "(identifier '=>' (statement | ( '{' statements '}' ) ))" + ' | ' +
                "( '(' paramList ')' '=>' ( statement | ( '{' statements '}' ) ))" + ' | ' +
                "( 'function' identifier? '(' paramList ')' '{' statements '}' )",

            varDec : "( 'constant' | 'let' | 'var' ) identifier ('=' expression)? ( ',' identifier ('=' expression)? )*",

            returnStat      : "'return' expression?",

            ifStat      : "'if' '(' expression ')' ( statement | ( '{' statements '}' ) ) " + 
                "( 'else' 'if' '(' expression ')' ( statement | ( '{' statements '}' ) ) )* " +
                "( 'else' ( statement | ( '{' statements '}' ) ))?",

            whileStat      : "'while' '(' expression ')' ( statement | ( '{' statements '}' ) ) ",

            forStat      : "'for' '(' expression? ';' expression? ';' expression? ')' ( statement | ( '{' statements '}' ) ) ",

            assignStat : "identifier '=' ( expression | functionDec | functionCall )",
            
            expressionList  : "(expression ( ',' expression )* )?",

            functionCall : "identifier '(' paramList ')'",

            list : "'[' ( expression ( ',' expression )* )? ',' ? ']'",

            hash : "'{' ( ( identifier | stringConstant ) ':' expression ( ',' ( identifier | stringConstant ) ':' expression )* )? ',' ? '}'",

        },
    },
}; //language definitions

class TokenAnalyzer {
    constructor( language ) {
        this.defs = typeof language === 'object' ? language : language_definitions[language];
        this.COMPILED = {};
        this.NAMED = {};
        this.defs.named.forEach( n => { this.NAMED[n] = 1; } );
        this.is_token = {};
        this.defs.token_types.forEach( tt => { this.is_token[tt] = tt; } );
        this.speculation_level = 0;
    }

    _compile_formula( formula_name ) {

        let { COMPILED, NAMED, is_token } = this;
        let COMPOSITES = this.defs.composites;

        // see if the group described by this formula
        // has already been compiled. If so, return the compiled group.
        let compiled_node = COMPILED[formula_name];
        if ( compiled_node ) {
	        return compiled_node;
        }

        let formula = COMPOSITES[formula_name];
        if( ! formula ) {
	        throw new Error( "Unknown formula '" + formula_name + "'" );
        }

        compiled_node = CompositeNode.start(formula_name);
        COMPILED[formula_name] = compiled_node;

        // start with an AND node in the compiled group
        // in case there are OR markers that would require the node to
        // be divided
        let working_node = compiled_node;

        // compile the formula
        while ( formula.match(/\S/) ) {
            // group is the group currently being compiled.

	        if ( match( formula, /^\s*([^]+)\s*([\*\?])?\s*(.*)/ ) ) {
                // LITERAL + possible repeat
                formula = matches[3];
                working_node.add_literal( matches[1], matches[2] );
            }
	        else if ( match( formula, /^\s*\(\s*(.*)/ ) ) {
                // START GROUP
                formula = matches[1];
                working_node = working_node.extend();
            }
	        else if ( match( formula, /^\s*\)\s*([\*\?])?\s*(.*)/ ) ) {
                // END GROUP + possible repeat
                formula = matches[2];
                working_node = working_node.close_if_assumed();

                working_node.set_repeat( matches[1] );
                working_node = working_node.close();
            }
	        else if ( match( formula, /^\s*\|(.*)/ ) ) {
                // OR MARKER - ends last group and turns the parent group into an OR node.
                formula = matches[1];
                working_node = working_node.close_if_assumed().to_or().extend();
                working_node.assumed_group = true;
            }
	        else if ( match( formula, /^\s*([a-zA-Z]+)\s*([\*\?])?\s*(.*)/ ) ) {
                // TOKEN or COMPILED FORMULA + possible repeat
                formula    = matches[3];
                let part   = matches[1];
                let repeat = matches[2];

                if ( is_token[part] ) {
                    working_node.add_token( part, repeat );
                }
                else if ( is_token[COMPOSITES[part]] ) {
                    working_node.add_token( COMPOSITES[part], repeat );
                }
                else if ( COMPOSITES[part] ) {
                    working_node.add_node( this._compile_formula( part ), part, repeat );
                }
                else {
		            throw new Error( "Error compiling formula '" + formula_name + "' : Unknown identifier '" + part + "'" );
                }
            }
            else {
		        throw new Error( "Error compiling formula '" + formula_name + "' : Unknown part '" + formula + "'" );
            }
        } //while still a formula

        compiled_node.squish();

        return compiled_node;
    } //_compile_formula


    _out() {
        console.log( this.speculation_level, ...arguments );
    }

    _expect_group( group ) {
        // returns [ success?, node, tokens ] where node
        //   is one of the following :
        //  group node { named, type, name, val }
        //   where named is the node formula (if any)
        //         type is one of 'REPEAT', 'AND', 'OR'
        //         name is the composite name that created this node
        //         val is a list of nodes inside this one
        //
        //  atom node  { token, type, name, val, line }
        //   where token is the token object
        //         type is 'LITERAL' or 'TOKEN'
        //         val is the token value

        this._out( "Seeking " + group.to_string() );
        if ( group.type === 'REPEAT' ) {
	        if ( group.repeat === '?' ) {
                this.speculation_level++;
                let items = [];

	            let res  = this._expect_group( group.val );
	            let got  = res[0];
	            let ret  = res[1];
	            let toks = res[2];

                this.speculation_level--;

                if ( ! got ) {
		            this.tokstore = toks.concat( this.tokstore );
                } else {
		            items.push( ret );
                }

                if ( this.speculation_level === 0 ) {
                    this._out( "Found " + ( group.name || '' ) + ' ' + group.to_string() );
		            return [ 1, { i  : 'A', type : group.type, named : group.named, name : group.name, val : items }, [] ];
                }
                this._out( "Found " + ( group.name || '' ) + ' ' + group.to_string(), toks.map( e => e[2] ).join(" ") );
                return [ 1, { i : 'B', named : group.named, type : group.type, name : group.name, val : items }, toks ];
            }
	        else if ( group.repeat === '*' ) {
                let rtoks = [];
                let repeats = [];
                while (1) {
		            this.speculation_level++;
		            let res  = this._expect_group( group.val );
		            let got  = res[0];
		            let rep  = res[1];
		            let toks = res[2];
		            this.speculation_level--;
		            if ( got ) {
		                if( rep !== undefined ) {
			                repeats.push( rep );
		                } else {
			                try {throw new Error("BEEEG"); } catch(e){} }
		                rtoks = rtoks.concat( toks );
		            } else {
		                this.tokstore = toks.concat( this.tokstore );
		                break;
		            }
                }
                if ( this.speculation_level === 0 ) {
                    this._out( "Found " + ( group.name || '' ) + ' ' + group.to_string() );
		            return [1, { i : 'C', named : group.named, type : group.type, name : group.name, val : repeats }, []];
                }
                this._out( "Found " + ( group.name || '' ) + ' ' + group.to_string(), rtoks.map( e => e[2] ).join(" ") );
                return [ 1, { i : 'D', named : group.named, type : group.type, name : group.name, val : repeats }, rtoks ];
	        }
        }
        else if ( group.type === 'AND' ) {
            if( group.named ) {
                this._out( "Opening", group.named, group.to_string() );
            }

	        let rtoks = [];
	        let succ = [];
	        for( let i=0; i<group.val.length; i++ ) {
	            let grp = group.val[i];

	            let res  = this._expect_group( grp );
	            let got  = res[0];
	            let val  = res[1];
	            let toks = res[2];

	            rtoks = rtoks.concat( toks );
                if ( got ) {
		            if( val ) {
		                succ.push( val );
		            }
                } else {
		            if ( this.speculation_level > 0 ) {
		                this.tokstore = rtoks.concat( this.tokstore );
                    }
		            if( this.lasterr === undefined && grp.formula ) {
		                this.lasterr = "Unable to find "+grp.name+" at line " + this.linenum;
		            }
                    this._out( "Did not find", group.name, group.to_string() );
		            return [0, undefined, []];
                }
            }

	        this.lasterr = undefined;
            this._out( "Found " + ( group.name || '' ) + ' ' + group.to_string(), rtoks.map( e => e[2] ).join(" ") );
	        return [ 1, { i : 'E', named : group.named, type : group.type, name : group.name, val : succ }, rtoks ];
        }
        else if ( group.type === 'OR' ) {

	        this.speculation_level++;
	        for( let i=0; i<group.val.length; i++ ) {
	            let groupcand = group.val[i];

	            let res  = this._expect_group( groupcand );
	            let success  = res[0];
	            let val      = res[1];
	            let toks     = res[2];
                // the toks go back
                if ( success ) {
		            this.speculation_level--;

		            if ( this.speculation_level == 0 ) {
		                // can throw out the used tokens if not speculating
                        this._out( "Found " + group.to_string() );

		                if ( groupcand.named ) {
			                return [ 1, { i : 'F', named : group.named, type : group.type, name : group.name, val : val, }, [] ];
		                }
		                return [1, val, [] ];
		            }
                    this._out( "Found" + group.to_string(), toks.map( e => e[2] ).join(" ") );
		            if ( group.named ) {
		                return [ 1, { i : 'F', named : group.named, type : group.type, name : group.name, val : val, }, toks ];
		            }
		            this.lasterr = undefined;
		            return [ 1, val, toks ];
                }
                // okey, if a failed loop, those tokens should be recycled
                // back to the hopper
	            this.tokstore = toks.concat( this.tokstore );
	        }
	        this.speculation_level--;

	        if( this.lasterr === undefined && group.formula ) {
	            this.lasterr = "Unable to find "+group.name+" at line "+ this.linenum;
	        }
            this._out( "Did not find", group.name, group.to_string() );
	        return [ 0, undefined, [] ];
        }
        else if ( group.type === 'LITERAL' ) {

	        let tok = this.tokstore.shift() || this.all_tokens.shift();
	        if ( tok ) {
	            if( this.linenum === undefined ) {
		            this.linenum = tok[1];
	            }
                if ( tok[2] === group.val && ( tok[0] === 'symbol' || tok[0] === 'keyword' ) ) {
                    this._out( "Found " + tok[2] );
		            this.maxlinenum = tok[1] > (this.maxlinenum||0) ? tok[1] : this.maxlinenum;
		            return [ 1, { i : 'H', token : tok, type : group.type, name : group.name, val : tok[2], line : tok[1] }, [tok] ];
                }
	            if( this.lasterr === undefined ) {
		            this.lasterr = "Literal '"+group.val+"' not found in line '"+tok[1]+"'. Got '" + tok[2] + "' instead";
	            }
	        }
            this._out( "Did not find", group.name, group.to_string() );
	        return [0, undefined, [tok]];
        }
        else if ( group.type === 'TOKEN' ) {
	        let tok = this.tokstore.shift() || this.all_tokens.shift();
	        if ( tok ) {
	            if( this.linenum === undefined ) {
		            this.linenum = tok[1];
	            }
                if ( tok[0] === group.val ) {
                    this._out( "Found " + group.val + ' ' + tok[2] );
		            this.maxlinenum = tok[1] > (this.maxlinenum||0) ? tok[1] : this.maxlinenum;
		            return [ 1, { i : 'I', token : tok, type : group.type, name : group.name, val : tok[2], line : tok[1] }, [tok] ];
                }
	            if( this.lasterr === undefined ) {
		            this.lasterr = group.val + " not found in line '" + tok[1] + "', got '" + tok[0] + "'";
	            }
	        }
            this._out( "Did not find", group.name, group.to_string() );
	        return [0, undefined, [tok] ];
        }
        else {
	        throw new Error( "Unknown formula type "+group.type );
        }
    } //_expect_group

    analyze_tokens( tokens, formulaName ) {
        this.speculation_level = 0;
        this.all_tokens = [...tokens];
        this.tokstore   = [];
        this.linenum    = 0;
        this.maxlinenum = 0;
        this.lasterr    = 0;
        let formula = this._compile_formula( formulaName );
        return this._expect_group( formula  );
    } //analyze_tokens

} //TokenAnalyzer class

module.exports = { TokenAnalyzer };
/*

 FOR THE formulas, there is always an implied grouping
  If there are | bars, it means an or group. If none, then
  an and group. If a mix, something is wrong.
 Formula --> group.
  group fields
    repeat '*' or '?'
    type 'AND', 'OR', 'LITERAL', 'TOKEN', 'REPEAT'
    val : list of groups or literal or token value
*/
` );
        console.log( "TREE", tree.to_string() );
    }
} //test_javascript

function test_analyzer() {
    let toks = t.tokenize( `
    let /* comment */ foo = "123" /*more comment*/;
 class HelloWorld /*OMMENTy
and sort of multiline */ extends /*COMMENT*/ Component.withWatchables("textstuff") {
       dbElements(){
           return e("div","HELLO
ON MULTIPLE LINES
 WORLD");
       }
 }
 render( HelloWorld, "root" ); 
` );
    console.log( ana.to_string() );
    //    let output = a.analyze_tokens( toks, "program" );
    output = ana.expect("TOKS",toks);
    console.log( "parsing succeeded" );
    console.log( "PARSE OUTPUT", output.to_string() );
} //test_analyzer

function test_tokenizer() {
    let toks = t.tokenize( `
    let /* comment */ foo = "123" /*more comment*/;
 class HelloWorld /*OMMENTy
and sort of multiline */ extends /*COMMENT*/ Component.withWatchables("textstuff") {
       dbElements(){
           x++ !== --y;
           return e("div","HELLO
ON MULTIPLE LINES
 WORLD");
       }
 }
 render( HelloWorld, "root" ); 
` );
    if( toks.length !== 46 ) {
        console.log( "BROKENIZER TOKENIZER" );
        return;
    }
    console.log( "token test passes" );
    //console.log( toks, toks.length );
}

function test_composites() {
    const test_composite = (k,errs) => {
        let formula = ana.lang.composites[ k ];
//        console.log( 'testing ' + k + ":" + formula );
        
        formula = formula.replace( /([\(\|]) +/g, '$1' );
        formula = formula.replace( / +([\?\*\)\|])/g, '$1' );
        formula = formula.replace( /^ +/g, '' );
        formula = formula.replace( / +$/g, '' );
        formula = formula.replace( /\'/g, '"' );
        let node = ana.get_analyzer( k );
        try {
            if( node.to_string() != formula ) {
                errs.push( "ERROR IN " + k + "\n\t" + node.to_string() + "\n\t" + formula + "\n" );
                console.log( 'failed', formula );
            } else {
//                console.log( 'passed', formula );
            }
        } catch( e ) {
            errs.push( "PROGRAMMER ERROR IN " + k + "\n\t" + node.to_string() + "\n\t" + formula + "\n" );
        }
    }


    let errs = [];
     Object.keys( ana.lang.composites ).map( k => test_composite(k,errs) );

    if( errs.length === 0 ) {
        console.log( "composite test passes" );
    }
    else {
        console.log( "** COMPOSITE TEST FAILED " + errs.length + " tests ***\n\t", errs.join("\n\t") + "\n" );
    }
}


if( 0 ) {
    Object.keys( a.defs.composites ).map( k => test_composite(k) );

    let {Tokenizer} = require( "./tokenizer.mjs" );
    let {TokenAnalyzer} = require( "./token_analyzer.mjs" );

    let t = new Tokenizer( 'js' );
    let a = new TokenAnalyzer( 'js' );

    let toks = t.tokenize( `
    let /* comment */ foo = "123" /*more comment*/;
 class HelloWorld /*OMMENTy*/ extends /*COMMENT*/ Component.withWatchables("textstuff") {
       dbElements(){
           return e("div","HELLO WORLD");
       }
 }
 render( HelloWorld, "root" ); 
` );


    ana( `
     foo[]().bar
` );


    function ana( prog ) {

        console.log( prog );
        
        toks = t.tokenize( prog );

        console.log( toks );
        
        console.log( a.analyze_tokens( toks, 'value' ) );
        
    } //ana
}
