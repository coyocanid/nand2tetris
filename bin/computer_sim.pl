#!/usr/bin/perl

#
# Simulate a hack computer in perl.
#

use strict;
use warnings;

#
# All the components of the computer.
#
my( @ROM, @RAM, $A_REG, $D_REG, $PC, $KEYBOARD, @SCREEN );

shell();
exit;

# ---------------------------------------------------------------------------------------------

sub shell {
    while(<>) {
        print "PHACK>";
        my $CMD = <STDIN>;
        if( $CMD =~ /^(?|h)/i ) {
            print "PHACK Commands\n" .
                join("\n\t", 
                     "?|help - this help",
                     "load <file>  - load hack file into ROM",
                     "reset - resets program counter to zero and blanks memory",
                     "run - runs the program from wherever the program counter is onwards",
                     "quit - quits this program",
                )."\n";
            
        }
        elsif( $CMD =~ /^q/i ) {
            exit;
        }
        elsif( $CMD =~ /^ru/i ) {
            
        }
        elsif( $CMD =~ /^l\S* (\S+)/i ) {
            if( open( my $in, '<', $1 ) ) {
                my $as_machine = $1 =~ /\.hack$/;
                while( <$in> ) {
                    
                }
                close $in;
            } else {
                print "Unable to open $1 : $!\n";
            }
        }
        elsif( $CMD =~ /^re/i ) {
            splice @SCREEN;
            splice @RAM;
            undef $KEYBOARD;
        }
    }
}

#
# Machine code to assembly.
#
sub h2a {

}
