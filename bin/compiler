#!/usr/bin/perl
use strict;
use warnings;
no warnings 'uninitialized';
use Data::Dumper;
use JSON;

my( @nodes, $node, $className, $classNode, $subName, $subNode, $lbls, $tagsub, $ecount, $pcount, $line, %methods, $pass );

sub err {
  my $val = shift;
  use Carp 'longmess'; print STDERR Data::Dumper->Dump([longmess]);
  warn $val;
}

my $capturing;
sub capture {
  $capturing = [];
}

sub release {
  my $ret = [@$capturing];
  undef $capturing;
  $ret;
}

sub out {
  return if $pass ne 'second';
  my $val = shift;
  if ( $capturing ) {
    push @$capturing, $val;
  } else {
    print "$val\n";
  }
}

sub say {
  return;
  print STDERR join( ' ',
                     map {
                       ref( $_ ) ?
                         to_json( $_ )
                         : $_
                       } @_ ) . "\n";
}

my $file = shift @ARGV;
if ( -e $file )
  {
    open( my $fin, $file );

    $pass = 'initial';
    while (<$fin>)
      {
        chomp $_;
        process( $_ );
      }
    seek $fin, 0, 0;
    $pass = 'second';
    while (<$fin>)
      {
        chomp $_;
        say $_;
        process( $_ );
      }
    close $fin;
  }
else
  {
    print STDERR "Usage : $0 <map file>\n";
  }

exit;

sub process {
  ( $_ ) = @_;

  if ( /^\s*(\d+)\s+BEGIN (.*)\s*$/ )
    {
      # OPEN
      $line = $1;
      $node = { type => 'NODE', name => $2 };
      $tagsub = \&{$2};
      &$tagsub("start", undef, 0);
      push @nodes, $node;
    }
  elsif ( /^\s*(\d+)\s+END (.*)\s*$/ )
    {
      # CLOSE
      $line = $1;
      my $deepernode = pop @nodes;
      say $node, "VS", $deepernode;
      
      &$tagsub("close",undef,$deepernode->{subcount}||0);
      $node = $nodes[$#nodes]; #previous one
      if ( $node )
        {
          $node->{subcount}++;
          $tagsub = \&{$node->{name}};
            &$tagsub("subn", $deepernode,$node->{subcount}||0);
        }
    }
  elsif ( /^\s*(\d+)\s+TERMINAL (\S+) (.*)\s*$/ )
    {
      # TERMINAL
      $line = $1;
      my $term = { type => 'TERM', name => $2, val => $3 };
      $node->{subcount}++;
      &$tagsub("subn", $term,$node->{subcount});
    }
} #process

sub lbl {
  my $tag = shift || '';
  $lbls++;
  "$className.$subName.$tag.$lbls";
}

sub class {
  # 'class' className '{' classVarDec* subroutineDec* '}'
  my( $ev, $term, $subcount ) = @_;
  if ( $ev eq 'subn' && 2 == $subcount )
    {
      $className = $term->{val};
      $classNode = $node;
    }
}

sub classVarDec {
  # ('static'|'field') type varName(',' varName)*';'
  my( $ev, $term, $subcount ) = @_;
  if ( $ev eq 'subn' )
    {
      if ( 1 == $subcount )
        {
          $node->{varscope} = $term->{val};
        }
      elsif ( 2 == $subcount )
        {
          $node->{vartype} = $term->{val};
        }
      elsif ( $subcount > 2 && $term->{name} ne 'symbol' )
        {
          my $varname = $term->{val};
          my $vars = $classNode->{$node->{varscope}} //= {};
          my $idx = scalar( keys %$vars );
          err "Name '$term->{val}' already defined in $node->{varscope} scope in class $className" if $vars->{$varname};
          # [ index, type, name, scope ]
          $vars->{$term->{val}} = [ $idx, $node->{vartype}, $varname, $node->{varscope} eq 'static' ? 'static' : 'this'];
          say "Assigning var '$term->{val}' to", $node;
        }
    }
} #classVarDec

sub doStatement {
  # 'do' subroutineCall ';'
  # 'do' ( className | varName ) '.' subroutineName '(' expressionList ')' | subroutineName '(' expressionList ')' ';'
  my( $ev, $term, $subcount ) = @_;
  return unless $ev eq 'subn';

  if( $term->{name} eq 'identifier' )
    {
      $node->{firstId} = $node->{lastId};
      $node->{lastId}  = $term->{val};
    }
  elsif( $term->{val} eq '(' )
    {
      # see if this is an instance method. an instance is a variable, which could be
      # a parameter, local var, class var or instance var

      # TODO - field only for methods method 
      if( $node->{firstId} ) {
        my ($var) = grep { defined } map { $_->{ $node->{firstId} } } ( $subNode->{params}, $subNode->{vars}, $classNode->{static}, $classNode->{field} );
        if ( $var ) { #instance var, otherwise a class
          $node->{methodclass} = $var->[1]; # the class (vartype) of the method
          out "push $var->[3] $var->[0]";   # put the var's this on the stack
        }
      }
      elsif( $methods{$node->{lastId}} ) {
        $node->{methodclass} = $className;
        out "push pointer 0";   # put the var's this on the stack
      }
    }
  elsif( $term->{val} eq ')' )
    {
      ++$ecount if $node->{methodclass};
      my $cname = $node->{methodclass} || $node->{firstId} || $className;
      out "call $cname.$node->{lastId} $ecount";
      out "pop temp 0"; # get rid of return value.
    }

} #doStatement

sub expression {
  # term (op term)*
  my( $ev, $term, $subcount ) = @_;

  # term values are pushed on the stack already by the term handler, so only the symbols do stuff here
  my $closeSymbol = $ev eq 'close';
  if ( $ev eq 'subn' )
    {
      if ( $term->{name} eq 'symbol' )
        {
          $node->{lastSymbol} = $term->{val};
        }
      else
        {
          $closeSymbol = $node->{lastSymbol};
        }
    }
  if ( $closeSymbol && ( my $s = $node->{lastSymbol} ) )
    {
      if ( $s eq '+' )
        {
          out 'add';
        }
      elsif ( $s eq '-' )
        {
          out 'sub';
        }
      elsif ( $s eq '*' )
        {
          out "call Math.multiply 2";
        }
      elsif ( $s eq '/' )
        {
          out "call Math.divide 2";
        }
      elsif ( $s eq '&' )
        {
          out 'and';
        }
      elsif ( $s eq '|' )
        {
          out 'or';
        }
      elsif ( $s eq '<' )
        {
          out 'lt';
        }
      elsif ( $s eq '>' )
        {
          out 'gt';
        }
      elsif ( $s eq '=' )
        {
          out 'eq';
        }
      else
        {
          err "Unknown symbol '$s'";
        }
      undef $node->{lastSymbol};
    }

}

sub expressionList {
  # (expression ( ',' expression )* )?
  my ( $ev, $term, $subcount ) = @_;
  if ( $ev eq 'subn' && $term->{val} ne ',' ) {
    $node->{ecount}++;
  }
  if ( $ev eq 'close' ) {
      $ecount = $node->{ecount} || 0;
    }
}

sub ifStatement {
  # 'if' '(' expression ')' '{' statements '}' ( 'else' '{' statements '}' )?
  my ( $ev, $term, $subcount ) = @_;
  if ( $ev eq 'start' )
    {
      $node->{yeslab} = lbl 'true';
      $node->{skiplab} = lbl 'false';
    }
  elsif ( $ev eq 'close' )
    {
      if ( $node->{endlab} )
        {
          out "label $node->{endlab}";
        }
      else
        {
          out "label $node->{skiplab}";
        }
    }
  elsif ( 3 == $subcount )
    {                           #expression
      out "if-goto $node->{yeslab}";
      out "goto $node->{skiplab}";
      out "label $node->{yeslab}";
    }
  elsif ( 8 == $subcount )
    {                           # else statements
      $node->{endlab} = lbl 'end';
      out "goto $node->{endlab}";
      out "label $node->{skiplab}";
    }
}

sub letStatement {
  # 'let' varName ( '[' expression ']' )? '=' expression ';'
  my ( $ev, $term, $subcount ) = @_;

  if ( $ev eq 'subn' )
    {
      if ( 2 == $subcount )
        {
          # find the var
          ($node->{var}) = grep { defined } map { $_->{ $term->{val} } }
              ( $subNode->{params}, $subNode->{vars}, $classNode->{static}, $classNode->{field} );
          err "Couldn't find variable '$term->{val}' in line $line" unless $node->{var};
        }
      elsif ( 3 == $subcount && $term->{val} eq '[' )
        {
          capture; #grabs the expression resolving the array index
        }
      elsif ( 5 == $subcount && $term->{val} eq ']' )
        {
          $node->{capture} = release; # stores the expression resolveing the array index
        }
    }
  elsif ( $ev eq 'close' )
    {
      # at this point, the value of the expression is on the stack
      my( $idx, $type, $name, $seg ) = @{$node->{var}};
      if ( $subcount == 8 )
        {
          # value to assign is now on the stack

          # put the expression resolving to the array index on the stack
          map { out $_ } @{$node->{capture}};
          
          my( $idx, $type, $name, $seg ) = @{$node->{var}};
          out "push $seg $idx";
          out "add";
          out "pop pointer 1";
          out "pop that 0";
        }
      else
        {
          out "pop $seg $idx";
        }
    }
} #letstatement

sub parameterList {
  # ((type varName)(',' type varName)*)?
  my ( $ev, $term, $subcount ) = @_;
  if ( $ev eq 'subn' )
    {
      if( ! $node->{vartype} ) {
        $node->{vartype} = $term->{val};
      }
      elsif ( $term->{val} ne ',' )
        {
          my $vars = $subNode->{params} //= {};
          my $idx = scalar( keys %$vars );
          $idx++ if $methods{$subName};
          $vars->{ $term->{val} } = [ $idx, $node->{vartype}, $term->{val}, 'argument' ];
        }
      else # is ','
        {
          undef $node->{vartype};
        }
    }
  elsif ( $ev eq 'close' )
    {
      $pcount = $subcount;
    }
}

sub returnStatement {
  # 'return' expression? ';'
  my( $ev, $term, $subcount ) = @_;
  if ( $ev eq 'close' ) {
    if( $subcount == 2 ) {
      out 'push constant 0';
    }
    out 'return';
  }
}

sub statements {
  my( $ev, $term, $subcount ) = @_;
  if( $ev eq 'start' ) {
    if( ! $subNode->{init} ) {
      $subNode->{init} = 1;
      my( $count ) = scalar( keys %{$subNode->{vars}} );
      out "function $className.$subName $count";
      if ( $subNode->{mtype} eq 'constructor' )
        {
          out "push constant ".scalar(keys %{$classNode->{field}||{}});
          out "call Memory.alloc 1";
          out "pop pointer 0";
        }
      elsif ( $subNode->{mtype} eq 'method' )
        {
          # put the argument zero on the pointer 0 as this
          out "push argument 0";
          out "pop pointer 0";
        }
    }
  }
}

sub subroutineBody {
  #NOOP
}

sub subroutineDec {
  # ('constructor'|'function'|'method') ('void' | type) subroutineName '(' parameterList ')' subroutineBody
  my( $ev, $term, $subcount ) = @_;

  if ( 1 == $subcount )
    {
      $node->{mtype} = $term->{val};
    }
  elsif( 3 == $subcount )
    {
      $subName = $term->{val};
      $subNode = $node;
      if( $node->{mtype} eq 'method' ) {
        $methods{$subName} = 1;
      }
    }
}

sub term {
  # integerConstant | stringConstant | keywordConstant | subroutineCall | varName '[' expression ']'  | varName | '(' expression ')' | unaryOp term",
  # subroutineCall - ( className | varName ) '.' subroutineName '(' expressionList ')' | subroutineName '(' expressionList ')'
  my( $ev, $term, $subcount ) = @_;
  if ( $ev eq 'subn' )
    {
      my( $name, $val ) = ( $term->{name}, $term->{val} );
      if ( $name eq 'integerConstant' )
        {
          out "push constant $val";
        }

      elsif ( $name eq 'stringConstant' )
        {
          my $len = length( $val );
          out "push constant $len";
          out 'call String.new 1';
          my( @chars ) = split //, $val;
          while ( @chars )
            {
              out "push constant ".ord( shift @chars );
              out 'call String.appendChar 2'; # pushes string obj back on the stack
            }
        }

      elsif ( $name eq 'keyword' )
        {
          if ( $val eq 'true' )
            {
              out "push constant 1";
              out "neg";
            }
          elsif ( $val eq 'false' || $val eq 'null' )
            {
              out "push constant 0";
            }
          else
            { #this
              out "push pointer 0";
            }
        }

      elsif ( $name eq 'identifier' )
        {
          # variable or sub call
          $node->{firstId} = $node->{lastId};
          $node->{lastId} = $val;
        }

      elsif ( $name eq 'symbol' )
        { # '(', ')', '.', '[', ']', '-' or '~'
          if ( $val eq '~' or $val eq '-' )
            { #unop
              $node->{unop} = $val;
            }
          elsif ( $val eq ']' )
            {
              my ($var) = grep { defined } map { $_->{ $node->{lastId} } } ( $subNode->{params}, $subNode->{vars}, $classNode->{static}, $classNode->{field} );
              unless( $var ) {
                err "Unknown variable '$node->{lastId}' in line $line"; # should include line numbers, yeah
              }
              if( $var->[ 1 ] ne 'Array' ) {
                err "variable $node->{lastId} not array";
              }
              out "push $var->[3] $var->[0]";
              out 'add';
              # problem here with sequential arrays in same statement
              out 'pop pointer 1';
              out 'push that 0';
              undef $node->{firstId};
              undef $node->{lastId};
            }
          elsif ( $val eq '(' && $node->{lastId} )
            {
                # method call
                my ($var) = grep { defined } map { $_->{ $node->{firstId} } } ( $subNode->{params}, $subNode->{vars}, $classNode->{static}, $classNode->{field} );
                if ( $var )
                  {
                    $node->{methodclass} = $var->[1];
                    out "push $var->[3] $var->[0]";
                  }
                elsif( $methods{$node->{lastId}} )
                  {
                    $node->{methodclass} = $className;
                    out "push pointer 0";
                }
            }
          elsif ( $val eq ')' && $node->{lastId} )
            {
              my $cname = $node->{methodclass} || $node->{firstId} || $className;
              my ($var) = grep { defined } map { $_->{ $node->{firstId} } } ( \%methods, $subNode->{params}, $subNode->{vars}, $classNode->{static}, $classNode->{field} );
              ++$ecount if $var;
              out "call $cname.$node->{lastId} $ecount";
              undef $node->{firstId};
              undef $node->{lastId};
            }
        }

    } #subnode

  elsif ( $ev eq 'close' )
    {
      if ( $node->{unop} eq '-' )
        {
          out "neg";
        }
      elsif ( $node->{unop} eq '~' )
        {
          out "not";
        }
      elsif ( $node->{lastId} )
        {
          # variable
          my ($var) = grep { defined } map { $_->{ $node->{lastId} } } ( $subNode->{params}, $subNode->{vars}, $classNode->{static}, $classNode->{field} );
          if ($var) {
            my( $idx, $type, $name, $seg ) = @$var;
            out "push $seg $idx";
          } else {
            err "Variable '$node->{lastId}' not found";
          }
        }
    }

} #term

sub varDec {
  # 'var' type varName (',' varName)* ';'
  my( $ev, $term, $subcount ) = @_;
  return unless $ev eq 'subn';

  if ( 2 == $subcount )
    {
      $node->{vartype} = $term->{val};
    }
  elsif ( 2 < $subcount && $term->{name} ne 'symbol')
    {
      my $vars = $subNode->{vars} //= {};
      my $idx = scalar( keys %$vars );
      $vars->{$term->{val}} = [ $idx, $node->{vartype}, $term->{val}, 'local' ];
      say "Assigning var '$term->{val}' to", $subNode;
    }
}

sub whileStatement {
  # 'while' '(' expression ')' '{' statements '}'
  my( $ev, $term, $subcount ) = @_;
  if ( $ev eq 'start' )
    {
      my $top = lbl 'top';
      $node->{top} = $top;
      $node->{do} = lbl 'do';
      $node->{end}  = lbl 'end';
      out "label $top";
    }
  elsif ( $ev eq 'subn' )
    {
      if ( 3 == $subcount )
        { #expression
          out "if-goto $node->{do}";
          out "goto $node->{end}";
          out "label $node->{do}";
        }
      elsif ( 6 == $subcount )
        { #statements
          out "goto $node->{top}";
        }
    }
  elsif ( $ev eq 'close' )
    {
      out "label $node->{end}";
    }
} #whileStatement

__END__

