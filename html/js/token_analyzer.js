const token_types = [ 'keyword',
                      'symbol',
                      'integerConstant',
                      'identifier',
                      'stringConstant' ];
const is_token = {};
token_types.forEach( tt => { is_token[tt] = tt; } );

const named = [ 'class',
                'classVarDec',
                'doStatement',
                'expression',
                'expressionList',
                'ifStatement',
                'letStatement',
                'parameterList',
                'returnStatement',
                'statements',
                'subroutineBody',
                'subroutineDec',
                'term',
                'varDec',
                'whileStatement' ];
const NAMED = {};
named.forEach( n => { NAMED[n] = 1; } );

const COMPOSITES = {
   statements      : 'statement*',
   statement       : 'letStatement | ifStatement | whileStatement | doStatement | returnStatement',
   letStatement    : "'let' varName ( '[' expression ']' )? '=' expression ';'",
   ifStatement     : "'if' '(' expression ')' '{' statements '}' ( 'else' '{' statements '}' )?",
   whileStatement  : "'while' '(' expression ')' '{' statements '}'",
   doStatement     : "'do' subroutineCall ';'",
   returnStatement : "'return' expression? ';'",
   unaryOp         : "'-' | '~'",
   keywordConstant : "'true' | 'false' | 'null' | 'this' ",
   expressionList  : "(expression ( ',' expression )* )?",
   op              : "'+' | '-' | '*' | '/' | '&' | '|' | '<' | '>' | '='",
   expression      : "term (op term)*",
   term            : "integerConstant | stringConstant | keywordConstant | subroutineCall | varName '[' expression ']'  | varName | '(' expression ')' | unaryOp term",
   subroutineCall : "( className | varName ) '.' subroutineName '(' expressionList ')' | subroutineName '(' expressionList ')'",
   class          : "'class' className '{' classVarDec* subroutineDec* '}'",
   classVarDec    : "('static'|'field') type varName(',' varName)*';'",
   type           : "'int' | 'char' | 'boolean' | className",
   subroutineDec  : "('constructor'|'function'|'method') ('void' | type) subroutineName '(' parameterList ')' subroutineBody",
   parameterList  : "((type varName)(',' type varName)*)?",
   subroutineBody : "'{' varDec* statements '}'",
   varDec         : "'var' type varName (',' varName)* ';'",
   className      : 'identifier',
   subroutineName : 'identifier',
   varName        : 'identifier',
};

let all_tokens = [];
let COMPILED = {};
let tokstore = [];
let speculation_level = 0;
let linenum, maxlinenum, lasterr;

/*

 FOR THE forumlas, there is always an implied grouping
  If there are | bars, it means an or group. If none, then
  an and group. If a mix, something is wrong.
 Forumla --> group.
  group fields
    repeat '*' or '?'
    type 'AND', 'OR', 'LITERAL', 'TOKEN', 'REPEAT'
    val : list of groups or literal or token value
*/


const spaces = indent => {
    let lnstr = '' + linenum;
    let times = 2*indent - lnstr.length;
    if( times < 0 ) { times = 0; }
    let spaces = '';
    for( let i=0; i<times; i++ ) {
	spaces = spaces + ' ';
    }
    return spaces;
}; //spaces

const show = (node,indent) => {
    let lines = [];
    
    if ( indent === undefined ) {
	let mls = maxlinenum + '';
	indent = mls.length;
    }

    if ( node.named ) {
	lines.push( [linenum,spaces(indent++),'BEGIN',node.name] );
    }

    if ( node.type === 'TOKEN' || node.type === 'LITERAL' ) {
	if( node.token ) {
	    let k = node.token[0];
	    let l = node.token[1];
	    let v = node.token[2];
	    linenum = node.line;
	    lines.push( [linenum,spaces(indent++),'TERMINAL', k, v ] );
	}
    }
    else if ( Array.isArray( node.val ) ) {
	node.val.forEach( v => {
	    lines = lines.concat( show( v, indent ) );
	} );
    } else {
	lines = lines.concat( show( node.val, indent ) );
    }
    if ( node.named ) {
	lines.push( [linenum,spaces(--indent),'END',node.name] );
    }
    return lines;
}; //show

const compile_formula = formula_name => {
    let group = COMPILED[formula_name];
    if ( group ) {
	return group;
    }
    
    let groups = [];
    
    let formula = COMPOSITES[formula_name];
    if( ! formula ) {
	throw new Error( "Unknown formula '" + formula_name + "'" );
    }

    group = { name : formula_name,
	      val  : [],
	      type : 'AND',
	      formula : formula,
	      named   : NAMED[formula_name] };

    COMPILED[formula_name] = group;

    while ( formula.match(/\S/) ) {

	if ( match( formula, /^\s*'([^']+)'([\*\?])?\s*(.*)/ ) ) {
            let compo   = matches[1];
            let repeat  = matches[2];
            formula = matches[3];
            let node = { val : compo, type : 'LITERAL' };
            if ( repeat ) {
		group.val.push( { val  : node,
				  type : 'REPEAT',
				  repeat : repeat,
				  formula : repeat } );
            } else {
		group.val.push( node );
            }
        }
	else if ( match( formula, /^\s*\(\s*(.*)/ ) ) {
            groups.push( group );
            group = { val : [], type : 'AND', name : 'grouping' };
            formula = matches[1];
        }
	else if ( match( formula, /^\s*\)([\*\?])?\s*(.*)/ ) ) {
            let repeat  = matches[1];
            formula = matches[2];
            let closedgroup = group;
	    if ( closedgroup.parentOR ) {
		let orgroup = closedgroup.parentOR;
		if ( group.val.length === 1 ) {
                    orgroup.val.push( group.val[0] );
                }
		else {
                    orgroup.val.push( group );
		}
		closedgroup = orgroup;
		groups.pop();
            }

            group = groups.pop();
            if ( repeat ) {
		group.val.push( { val     : closedgroup,
				  type    : 'REPEAT',
				  repeat  : repeat,
				  formula : repeat } );
            } else {
		group.val.push( closedgroup );
            }
        }
	else if ( match( formula, /^\s*\|(.*)/ ) ) {
            formula = matches[1];
	    
            if ( group.parentOR ) {
		let orgroup = group.parentOR;
		// check for single 'AND' items
		if ( group.val.length === 1 ) {
		    orgroup.val.push( group.val[0] );
                }
		else {
		    orgroup.val.push( group );
                }
		group = orgroup;
            } else {
		group.type = 'OR';
		if ( group.val.length > 1 ) {
                    //if more than one thing is already there,
                    // combind it into one AND node as the ors first val
		    group.val = [ { val  : group.val.concat([]),
				    type : 'AND',
				    name : group.name,
				    named : group.named } ];
                }
		groups.push( group );
            }
            group = { val  : [],
		      type : 'AND',
		      name : 'grouping',
		      parentOR : group };
        }
	else if ( match( formula, /^\s*([a-zA-Z]+)([\*\?])?\s*(.*)/ ) ) {
            let part   = matches[1];
            let repeat = matches[2];
            formula    = matches[3];
            if ( is_token[part] ) {
		if ( repeat ) {
		    group.val.push( { val : { name : part,
					      val  : part,
					      type : 'TOKEN' },
				      type : 'REPEAT',
				      repeat : repeat,
				      formula : repeat } );
                } else {
		    group.val.push( { name : part, val : part, type : 'TOKEN' } );
                }
            }
            else if ( is_token[COMPOSITES[part]] ) {
		if ( repeat ) {
		    group.val.push ( { val : { name : part,
					       val : COMPOSITES[part],
					       type : 'TOKEN' },
				       type : 'REPEAT',
				       repeat : repeat,
				       formula : repeat } );
                } else {
                    group.val.push( { name : part, val : COMPOSITES[part], type : 'TOKEN' } );
                }
            }
            else if ( COMPOSITES[part] ) {
		let cgroup = compile_formula( part );
		if ( repeat ) {
		    group.val.push( { val     : cgroup,
				      type    : 'REPEAT',
				      repeat  : repeat,
				      formula : repeat }  );
                } else {
		    group.val.push( cgroup );
                }
            } else {
		throw new Error( "Error compiling formula '" + formula_name + "' : Unknown part '" + part + "'" );
          }
        }
    } //while still a formula

    // might have to close a group here now
    let orgroup = groups.pop();
    if ( orgroup ) {
	if ( group.val.length === 1 ) {
	    orgroup.val.push( group.val[0] );
	} else {
	    group.length;
            orgroup.val.push( group );
        }
	group = orgroup;
    }

    return group;
} //compile_formula


const expect_group = group => {

    if ( group.type === 'REPEAT' ) {
	if ( group.repeat === '?' ) {
            speculation_level++;
            let items = [];
	    
	    let res  = expect_group( group.val );
	    let got  = res[0];
	    let ret  = res[1];
	    let toks = res[2];

            speculation_level--;

            if ( ! got ) {
		tokstore = toks.concat( tokstore );
            } else {
		items.push( ret );
            }
            if ( speculation_level === 0 ) {
		return [ 1, { i  : 'A', type : group.type, named : group.named, name : group.name, val : items }, [] ];
            }
            return [ 1, { i : 'B', named : group.named, type : group.type, name : group.name, val : items }, toks ];
        }
	else if ( group.repeat === '*' ) {
            let rtoks = [];
            let repeats = [];
            while (1) {
		speculation_level++;
		let res  = expect_group( group.val );
		let got  = res[0];
		let rep  = res[1];
		let toks = res[2];
		speculation_level--;
		if ( got ) {
		    if( rep !== undefined ) { 
			repeats.push( rep );
		    } else {
			try {throw new Error("BEEEG"); } catch(e){} }
		    rtoks = rtoks.concat( toks );
		} else {
		    tokstore = toks.concat( tokstore );
		    break;
		}
            }
	    
            if ( speculation_level === 0 ) {
		return [1, { i : 'C', named : group.named, type : group.type, name : group.name, val : repeats }, []];
            }
            return [ 1, { i : 'D', named : group.named, type : group.type, name : group.name, val : repeats }, rtoks ];
	}
    }
    else if ( group.type === 'AND' ) {
	let rtoks = [];
	let succ = [];
	for( let i=0; i<group.val.length; i++ ) {
	    let grp = group.val[i];

	    let res  = expect_group( grp );
	    let got  = res[0];
	    let val  = res[1];
	    let toks = res[2];

	    rtoks = rtoks.concat( toks );
            if ( got ) {
		if( val ) {
		    succ.push( val );
		}
            } else {
		if ( speculation_level > 0 ) {
		    tokstore = rtoks.concat( tokstore );
                }
		if( lasterr === undefined && grp.formula ) {
		    lasterr = "Unable to find "+grp.name+" at line " + linenum;
		}
		return [0, undefined, []];
            }
        }

	lasterr = undefined;
	return [ 1, { i : 'E', named : group.named, type : group.type, name : group.name, val : succ }, rtoks ];
    }
    else if ( group.type === 'OR' ) {
	speculation_level++;
	for( let i=0; i<group.val.length; i++ ) {
	    let groupcand = group.val[i];

	    let res  = expect_group( groupcand );
	    let success  = res[0];
	    let val      = res[1];
	    let toks     = res[2];
            // the toks go back
            if ( success ) {
		speculation_level--;
		if ( speculation_level == 0 ) {
		    // can throw out the used tokens if not speculating
		    if ( groupcand.named ) {
			return [ 1, { i : 'F', named : group.named, type : group.type, name : group.name, val : val, named : 1, }, [] ];
		    }
		    return [1, val, [] ];
		}
		if ( group.named ) {
		    return [ 1, { i : 'F', named : group.named, type : group.type, name : group.name, val : val, named : 1, }, toks ];
		}
		lasterr = undefined;
		return [ 1, val, toks ];
            }
            // okey, if a failed loop, those tokens should be recycled
            // back to the hopper
	    tokstore = toks.concat( tokstore );
	}
	speculation_level--;

	if( lasterr === undefined && group.formula ) {
	    lasterr = "Unable to find "+group.name+" at line "+ linenum;
	}
	return [ 0, undefined, [] ];
    }
    else if ( group.type === 'LITERAL' ) {
	let tok = tokstore.shift() || all_tokens.shift();
	if ( tok ) {
	    if( linenum === undefined ) {
		linenum = tok[1];
	    }
            if ( tok[2] === group.val && ( tok[0] === 'symbol' || tok[0] === 'keyword' ) ) {
		maxlinenum = tok[1] > (maxlinenum||0) ? tok[1] : maxlinenum;
		return [ 1, { i : 'H', token : tok, type : group.type, name : group.name, type : group.type, val : tok[2], line : tok[1] }, [tok] ];
            }
	    if( lasterr === undefined ) {
		lasterr = "Literal '"+group.val+"' not found in line '"+tok[1]+"'. Got '" + tok[2] + "' instead";
	    }
	}
	return [0, undefined, [tok]];
    }
    else if ( group.type === 'TOKEN' ) {
	let tok = tokstore.shift() || all_tokens.shift();
	if ( tok ) {
	    if( linenum === undefined ) {
		linenum = tok[1];
	    }
            if ( tok[0] === group.val ) {
		maxlinenum = tok[1] > (maxlinenum||0) ? tok[1] : maxlinenum;
		return [ 1, { i : 'I', token : tok, type : group.type, name : group.name, type : group.type, val : tok[2], line : tok[1] }, [tok] ];
            }
	    if( lasterr === undefined ) {
		lasterr = group.val + " not found in line '" + tok[1] + "', got '" + tok[0] + "'";
	    }
	}
	return [0, undefined, [tok] ];
    }
    else {
	throw new Error( "Unknown formula type "+group.type );
    }
}; //expect_group


const expect = formula => {
  return expect_group( compile_formula( formula ) );
};

N2T.analyze_tokens = tokens => {
    all_tokens = tokens;
    let res = expect('class');
    let succ = res[0];
    let node = res[1];
    let toks = res[2];
    if( succ ) {
	return show( node );
    } else {
	throw new Error( "Analysis failed" );
    }
}
