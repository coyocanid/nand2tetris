let dsttbl  = N2T.consts.get_dest_table();
let jumptbl = N2T.consts.get_jump_table();
let comptbl = N2T.consts.get_comp_table();

// returns program object with the fields
//    ops             : operation objects
//    words           : list of opcode words
//    lineData        : list of [ PC idx, op obj, raw_line ] (includes every line)
//    opIdxLookup     : table mapping PC idx to an index in lineData
//
// The operation objects have the fields
//    type : 'A' or 'C' for those types of instructions
//    word : opcode word
//  if the operation is for an A instruction, it also has this field
//    val  : numeric value of a
//  if the operation is for a C instruction, it also has these fields
//    jump : binary jump instruction part of the opcode
//    comp : assembly version of the computation part of the opcode
//    dest : binary register destinatoin part of the opcode
//    
 
N2T.assemble_program = lines => {
    
    let labels = N2T.consts.get_label_tabel();

    let needsLabelOrVariableResolution = {}; // for @label that hasnt been yet defined
    let ops             = []; // list of operator objects
    let lineData        = []; // contains [PC,op,line] data. PC may be undef for comments
    let PC2lineData_idx = {}; // an index into lineData corresponding to the PC index
    
    
    lines.forEach( raw_lines => {
        let raw_line = raw_lines[0];
	    line = raw_line.replace( /\/\/.*/, '' );

	    if( line.match( /^\s*$/ ) ) {
	        // comment or empty line. ignore
            lineData.push( [ undefined, undefined ].concat( raw_lines ) ); // no PC or op here
	    }
	    else if( line.charAt(0) === '(' ) {
	        let label = line.substr( 1, line.length-2 );
	        if( labels[ label ] !== undefined ) {
		        throw new Error(line + " defined twice" );
	        }
            let val = ops.length;
	        let word = '0' + _num2binstr( val, 15 );
	        labels[ label ] = val;
	        if( needsLabelOrVariableResolution[ label ] ) {
		        needsLabelOrVariableResolution[ label ].forEach( needsop => {
		            needsop.word = word;
                    needsop.val  = val;
		        } );
		        delete needsLabelOrVariableResolution[ label ];
	        }
            lineData.push( [ undefined, undefined ].concat( raw_lines ) ); // no PC or op here
	    }
	    // A instruction :  @number or @label
	    else if( line.charAt(0) === '@' ) {
	        let op = { type : 'A' };
            
            PC2lineData_idx[ ops.length ] = lineData.length;
            lineData.push( [ ops.length, op ].concat( raw_lines ) );
            
	        let val = parseInt( line.substr(1) );
            
	        // @label
	        if( isNaN( val ) ) {
		        let label = line.substr( 1 );
		        if( labels[label] !== undefined ) {
                    op.val  = labels[label];
		            op.word = '0' + _num2binstr( op.val, 15 );
		        } else {
		            // this either references a label that has not yet
		            // been defined, or it references a variable referencing
		            // a RAM address (starting at 16)
		            let needs = needsLabelOrVariableResolution[ label ];
		            if( ! needs ) {
			            needs = [];
			            needsLabelOrVariableResolution[ label ] = needs;
		            }
		            needs.push( op );
		        }
	        }
	        else {
		        op.word = '0' + _num2binstr( val, 15 );
                op.val = val;
	        }

	        ops.push( op );
	    }
        
	    // C instruction
	    else {
	        let op = { type : 'C',
		               jump : '000',     // pc doesnt jump by default
                       comp : '0',   
		               dest : '000' };   // data goes no where by default
            
		    let comp = '0101010'; // compute zero
	        
            PC2lineData_idx[ ops.length ] = lineData.length;
            lineData.push( [ ops.length, op ].concat( raw_lines ) ); 
            
	        if( line.slice(-4,-3) === ';' ) {
		        op.jump = jumptbl[ line.slice(-3) ];
	        }

            if( match( line, /(0|([AMD]+)=)?([^;]*)/ ) ) {
		        if( matches[2] !== undefined ) {
		            op.dest = dsttbl[ matches[2] ];
		        }
		        if( matches[3] ) {
		            // computation with destination
		            op.comp = matches[3];
                    if( ! comptbl[ matches[3] ] ) {
		                throw new Error( line + " : cant parse assembly instruction" );
                    }
		            comp = comptbl[ matches[3] ][0];
		        }
	        } else {
		        throw new Error( line + " : cant parse assembly instruction" );
	        }
	        op.word = '111' + comp + op.dest + op.jump;

	        ops.push( op );
	    }
    } );

    // resolve variables
    let RAMaddress = 16;
    Object.keys( needsLabelOrVariableResolution ).forEach( key => {
	    let val = RAMaddress++;
	    let word = '0' + _num2binstr( val, 15 );
	    needsLabelOrVariableResolution[key].forEach( op => {
	        op.word = word;
            op.val  = val;
	    } );
    } );
    
    return {
        lineData         : lineData,
        ops              : ops, // for tests, really
        PC2lineIdx       : PC2lineData_idx,
    };

}; //assemble_program
