const lineComment  = /\/\//;
const inlineComment = /\s*\/\*\*.*?\*\/\s*/g;
const commentStart = /\/\*\*.*/;
const commentEnd   = /.*\*\//;
const symbol_types = ['keyword',
                      'symbol',
                      'integerConstant',
                      'identifier'];
const types = {
    keyword : /^(boolean|char|class|constructor|do|else|false|field|function|if|int|let|method|null|return|static|this|true|var|void|while)\b/,
    symbol : /^([-{}()\*\[\]\.;+\/\&\|<>=~,])/,
    integerConstant : /^(\d+)/,
    identifier      : /^([_a-zA-Z][_a-zA-Z0-9]*)/,
};

//let match   = N2T.consts.get_match_fun();
//let matches = match.matches;

N2T.tokenize = lines => {
    let tokens = [];
    // emit = tokens.push( [token-type, linecount, token-val] )
    let inComment = false;
    
    lines.forEach( (raw_line,linecount) => {
        let line = raw_line.replace( inlineComment, '' );
        while( line.match(/\S/) ) {
            if( inComment && line.match( commentEnd ) ) {
                line = line.replace( commentEnd, '' );
                inComment = false;
            }
            if( inComment ) { return; }

            line = line.replace( /^\s*/, '' );
            if( match( line, /^"([^"]*)"\s*/ ) ) {
                tokens.push( ['stringConstant', linecount, matches[1]] );
                line = line.replace( /^"([^"]*)"\s*/, '' );
            }
            else if ( line.match( lineComment ) ) {
                line = undefined;
            }
            else if ( line.match( commentStart ) ) {
                inComment = true;
                line = undefined;
            }
            else {
                let found = false;
                for( i in symbol_types ) {
                    let type = symbol_types[i];
                    let re = types[type];
                    if( match( line, re ) ) {
                        let tok = matches[1];
                        line = line.replace( re, '' );
                        if( type === 'integerConstant' && parseInt(tok) > 32767 ) {
                            throw new Error( "Integer Token too large '" + tok + "'" );
                        }
                        tokens.push( [type, linecount, tok] );
                        found = true;
                        break;
                    }
                }
                if( ! found ) {
                    throw new Error( "Cant tokenize '" +line + "'" );
                }
            }
        }
    } );
    return tokens;
};
