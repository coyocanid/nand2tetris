N2T = {};

const _num2binstr = (num,bits) => {
    if( ! bits ) {
        throw new Error( "_num2binstr requires 2 parameters" );
    }
    let strval = '';
    if( num < 0 ) {
        return _add( _not( _num2binstr(-num,bits) ), '0000000000000001' );
    }
    for( let i=(bits-1); i>-1; i-- ) {
        let exp = 2**i;
            if( num >= exp ) {
                strval += '1';
                num -= exp;
            } else {
                strval += '0';
            }
    }
    return strval;
};


const _not = num => {
    let ret = '';
    for( let i=0; i<16; i++ ) {
	    if( num.charAt(i) === '0' ) {
	        ret = ret + '1';
	    } else {
	        ret = ret + '0';
	    }
    }
    return ret;
};

const _neg = num => _add( _not(num), "0000000000000001" );

const _add = (a,b) => {
    let ret = [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false];
    let carry = false;
    for( let i=15; i> -1; i-- ) {
	    if( a.charAt(i) === '1' && b.charAt(i) === '1' ) {
	        if( carry ) {
		        ret[i] = true;
	        } else {
		        carry = true;
	        }
	    }
	    else if( a.charAt(i) === '1' || b.charAt(i) === '1' ) {
	        if( ! carry ) {
		        ret[i] = true;
	        }
	    }
	    else if( carry ) {
	        ret[i] = true;
	        carry = false;
	    }
    }
    return ret.map( b => b ? '1' : '0' ).join('');
};

const _sub = (a,b) => {
    let ret = [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false];
    let borrow = false;
    for( let i=15; i> -1; i-- ) {

	    let topOne = ( borrow && a.charAt(i) === '0' )
            || ( !borrow && a.charAt(i) === '1' );
	    if( b.charAt(i) === '1' ) {
            if( ! topOne ) {
                ret[ i ] = true;
                borrow = true;
            }
        }
        else if( topOne ) {
            ret[ i ] = true;
        }
        else if( borrow ) {
            // a 1 up top and a zero on the bottom
            borrow = false;
        }
    }
    return ret.map( b => b ? '1' : '0' ).join('');
};
const _and = (a,b) => {
    let ret = [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false];
    for( let i=0; i<16; i++ ) {
	    if( a.charAt(i) === '1' && b.charAt(i) === '1' ) {
	        ret[i] = true;
	    }
    }
    return ret.map( b => b ? '1' : '0' ).join('');
};
const _or = (a,b) => {
    let ret = [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false];
    for( let i=0; i<16; i++ ) {
	    if( a.charAt(i) === '1' || b.charAt(i) === '1' ) {
	        ret[i] = true;
	    }
    }
    return ret.map( b => b ? '1' : '0' ).join('');
};

N2T.consts = {
    get_comp_table : () => {
        return {
            '0'   : ['0101010', () => '0000000000000000' ],
            '1'   : ['0111111', () => '0000000000000001' ],
            '-1'  : ['0111010', () => '1111111111111111' ],
            'D'   : ['0001100', comp => comp.D ],
            'A'   : ['0110000', comp => comp.A ],
            'M'   : ['1110000', comp => { return comp.M(); } ],
            '!D'  : ['0001101', comp => _not(comp.D) ],
            '!A'  : ['0110001', comp => _not(comp.A) ],
            '!M'  : ['1110001', comp => { return _not(comp.M()); } ],
            '-D'  : ['0001111', comp => _neg(comp.D) ],
            '-A'  : ['0110011', comp => _neg(comp.A) ],
            '-M'  : ['1110011', comp => { return _neg(comp.M()); } ],
            'D+1' : ['0011111', comp => _add(comp.D,'0000000000000001') ],
            'A+1' : ['0110111', comp => _add(comp.A,'0000000000000001') ],
            'M+1' : ['1110111', comp => { return _add(comp.M(),'0000000000000001'); } ],
            'D-1' : ['0001110', comp => _sub(comp.D,'0000000000000001') ],
            'A-1' : ['0110010', comp => _sub(comp.A,'0000000000000001') ],
            'M-1' : ['1110010', comp => { return _sub(comp.M(),'0000000000000001'); } ],
            'D+A' : ['0000010', comp => _add(comp.D,comp.A) ],
            'D+M' : ['1000010', comp => { return _add(comp.D,comp.M()) } ],
            'D-A' : ['0010011', comp => _sub(comp.D,comp.A) ],
            'D-M' : ['1010011', comp => { return _sub(comp.D,comp.M()); } ],
            'A-D' : ['0000111', comp => _sub(comp.A,comp.D) ],
            'M-D' : ['1000111', comp => { return _sub(comp.M(),comp.D); } ],
            'D&A' : ['0000000', comp => _and(comp.D,comp.A) ],
            'D&M' : ['1000000', comp => { return _and(comp.D,comp.M()); } ],
            'D|A' : ['0010101', comp => _or(comp.D,comp.A) ],
            'D|M' : ['1010101', comp => { return _or(comp.D,comp.M()); } ],
        };
    },
    get_jump_table : () => {
        return {
            JMP : '111',
            JEQ : '010',
            JLT : '100',
            JNE : '101',
            JGT : '001',
            JGE : '011',
            JLE : '110'
        };
    },
    get_dest_table : () => {
        return {
            AMD : '111',
            M   : '001',
            D   : '010',
            MD  : '011',
            A   : '100',
            AM  : '101',
            AD  : '110',
        };
    },
    get_segment_table : () => {
        return {
            'argument' : 'ARG',  //1
            'local'    : 'LCL',  //2
            'this'     : 'THIS', //3
            'that'     : 'THAT', //4
        };
    },
    get_label_tabel : () => {
        let labels = {
	        SP     : 0,
	        LCL    : 1,
	        ARG    : 2,
	        THIS   : 3,
	        THAT   : 4,
	        SCREEN : 16384,
	        KBD    : 24576,
        };
        for( let i=0; i<16; i++ ) {
	        labels[ "R" + i ] = i;
        }
        return labels;
    },
    binstr2num : (binstr) => {
        let val = 0;
        let pos = 0;
        for( let i=binstr.length-1; i>=0; i-- ) {
            if( binstr.charAt( pos++ ) == '1' ) {
                val += 2 ** i;
            }
        }
        return val;
    },
    
} //consts

let matches = [];
const match = ( str, regex ) => {
    matches.length = 0;
    let m = str.match( regex );
    if( m ) {
	    m.forEach( ma => matches.push(ma) );
	    return true;
    }
    return false;
}
