let SEG = N2T.consts.get_segment_table();

N2T.generate_assembly = (lines,root) => {
    let alines = [];
    let vlines = [];
    
    let labels = 0;
    let label = (lab) => {
	    lab = lab || 'L';
	    return lab + '_' + ++labels;
    };
    
    let push_reg, push_idx;
    let readystack = push_or_pop => {
	    if ( push_reg === undefined ) {
	        if ( push_or_pop === 'POP' ) {
		        alines.push( "@SP" );
		        alines.push( "AM=M-1" );
		        alines.push( "D=M" );
	        }
	        return 0;
	    }
	    if ( push_reg === 'constant' ) {
	        alines.push( "@" + push_idx );
	        alines.push( "D=A" );
	    }
	    else if ( push_reg.match( /^(this|that|local|argument)$/ ) ) {
	        let REG = SEG[push_reg];
	        if ( push_idx > 3 ) {
		        alines.push( "@" + push_idx );
		        alines.push( "D=A" );
		        alines.push( "@" + REG );
		        alines.push( "A=M+D" );
            }
	        else if( push_idx > 0 ) {
		        alines.push( "@" + REG );
		        alines.push( 'A=M+1' );
		        for ( let i=2; i<=push_idx; i++ )
		        {
		            alines.push( 'A=A+1' );
		        }
            }
	        else
            {
		        alines.push( "@" + REG );
		        alines.push( 'A=M' );
	        }
	        alines.push( 'D=M' );
	    }
	    else if ( push_reg==='static' ) {
	        alines.push( "@" + root + '.' + push_idx );
	        alines.push( 'D=M' );
	    }
	    else if ( match( push_reg, /^(temp|pointer)$/ ) ) {
	        let pos = push_idx + ( push_reg==='temp' ? 5 : 3 );
	        alines.push( "@" + pos );
	        alines.push( 'D=M' );
	    }

	    if ( push_or_pop==='PUSH' ) {
	        alines.push( '@SP' );
	        alines.push( 'M=M+1' );
	        alines.push( 'A=M-1' );
	        alines.push( 'M=D' );
	    }

	    push_reg = undefined;
	    return 1;
    };

    let fun, locals;
    
    lines.forEach( raw_lines => {
        let raw_line = raw_lines[0];
        let vstart = alines.length;

	    line = raw_line.replace( /\/\/.*/, '' );
	    if( line.match( /^\s*$/ ) ) {
	        // comment or empty line. ignore
	    }
	    else if ( match( line, /^push +(\S+) +(\d+)/ ) ) {
            readystack( 'PUSH' );
            push_reg = matches[1];
            push_idx = matches[2];
        }
        else if ( match( line, /^pop +static +(\d+)/ ) ) {
            readystack( 'POP' );
            alines.push( '@' + root + '.' + matches[1] );
            alines.push( 'M=D' );
        }
        else if ( match( line, /^pop +(temp|pointer) +(\d+)/ ) ) {
            readystack( 'POP' );
            let pos = matches[2] + ( matches[1]==='temp' ? 5 : 3 );
            alines.push( '@' + pos );
            alines.push( 'M=D' );
        }
        else if ( match( line, /^pop +(argument|that|this|local) +(\d+)/ ) ) {
            readystack( 'POP' );
            let reg = SEG[matches[1]];
            let pos = matches[2];

            if ( pos > 11 ) {
                alines.push( '@13' );
                alines.push( 'M=D' ); //store the popped value

                alines.push( '@' + pos );
                alines.push( 'D=A' );
                alines.push( '@' + reg );
                alines.push( 'D=D+M' );  //the location to pop to

                alines.push( '@14' );
                alines.push( 'M=D' );

                alines.push( '@13' );
                alines.push( 'D=M' );

                alines.push( '@14' );
                alines.push( 'A=M' );
                alines.push( 'M=D' );
            }
            else if( pos > 0 ) {
                alines.push( '@' + reg );
                alines.push( 'A=M+1' );
		        for ( let i=2; i<=pos; i++ ) {
                    alines.push( 'A=A+1' );
                }
                alines.push( "M=D" );
            }
            else {
                alines.push( '@' + reg );
                alines.push( 'A=M' );
                alines.push( "M=D" );
            }
	    } // pop
        else if( line==='add' || line==='sub' ) {
            // x + y   OR   x - y
            // y is the last one on the stack
            readystack( 'POP' );

            // D has the y value
            // SP is set at the y value position
            // stay at the y position and do the
            // arithmetic on the x position

            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( (line==='add' ? 'M=D+M' : 'M=M-D' ) );

        }
        else if( line==='eq' ) {
            // x == y ? then put -1 or 0 on the x spot
            readystack( 'POP' );
            
            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( 'D=D-M' );
            let lab = label('IF');
            alines.push( '@' + lab );
            alines.push( 'D;JEQ' );
            alines.push( 'D=-1' );
            alines.push( "(" + lab + ")" );
            alines.push( 'D=!D' );
            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( 'M=D' );
        }
        else if( line==='lt' || line==='gt' ) {
            readystack( 'POP' );
            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( 'D=D-M' );
            let lab   = lab(line.toUpperCase());
            let lab2  = lab(line.toUpperCase());	    
            alines.push( '@' + lab );
            alines.push( line==='lt' ? 'D;JGT' : 'D;JLT' );
            alines.push( 'D=0' );
            alines.push( '@' + lab2 );
            alines.push( '0;JMP' );
            alines.push( "(" + lab + ")" );
            alines.push( 'D=-1' );
	        alines.push( "(" + lab2 + ")" );
            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( 'M=D' );
        }
        else if( line==='or' || line==='and' ) {
            readystack( 'POP' );
            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( line==='or' ? 'M=D|M' : 'M=D&M' );
        }
        else if( line==='not' || line==='neg' ) {
            let symb = line === 'not' ? '!' : '-';
            if( readystack() ) {
                // the value to be transformed
                // is in D
                alines.push( '@SP' );
                alines.push( 'A=M-1' );
                alines.push( "M=" + symb + "D" );
            }
            else{
                // update the last value in the
                // stack withalines.push( changing stack position
                alines.push( '@SP' );
                alines.push( 'A=M-1' );
		        alines.push( "M=" + symb + "M" );
            }
        }
        else if( match( line, /^function +(\S+) +(\d+)/ ) ) {
            readystack( 'PUSH' );
            // label this function then initialize
            // the local variables by zeroing them out
            fun = matches[1];
            locals = matches[2];
            alines.push( "(" + fun + ")" );
	        if ( locals > 0 ) {
                if ( $locals > 5 ) {
                    alines.push( '@SP' );
                    alines.push( 'A=M' );
		            for ( let i=0; i<=locals; i++ ) {
			            alines.push( 'M=0' );
			            alines.push( 'A=A+1' );
                    }
		            alines.push( '@' + locals );
                    alines.push( 'D=A' );
                    alines.push( '@SP' );
                    alines.push( 'M=D+M' );
                } // 6 + 2xlocal
                else {  //  local = 6
		            for ( let i=0; i<=locals; i++ ) {
			            alines.push( '@SP' );
			            alines.push( 'AM=M+1' );
			            alines.push( 'A=A-1' );
			            alines.push( 'M=0' );
                    }
                }
            }
        }
        else if( line === 'return' ) {
            readystack( 'PUSH' );

            //save the outgoing ARG, will become restored SP
            alines.push( '@ARG' );
            alines.push( 'D=M' );
            alines.push( '@13' );
            alines.push( 'M=D' );

            // save the return value in 14
            alines.push( '@SP' );
            alines.push( 'AM=M-1' ); //pop to after the outgoing LCL
            alines.push( 'D=M' );
            alines.push( '@14' );
            alines.push( 'M=D' );

	        [ 'THAT', 'THIS', 'ARG' ].forEach( REG => {
                alines.push( '@LCL' );
                alines.push( 'AM=M-1' );  // at prev REG
                alines.push( 'D=M' );     // prev REG address value
                alines.push( "@" + REG );
                alines.push( 'M=D' );
            } );
            alines.push( '@LCL');   //at prev ARG
            alines.push( 'A=M-1' );   //at at prev LOCAL
            alines.push( 'A=A-1' );   //at return address
            alines.push( 'D=M' );
            // stash return address in 15
            alines.push( '@15' );
            alines.push( 'M=D' );

            alines.push( '@LCL' );
            alines.push( 'AM=M-1' );  // at prev ARG
            alines.push( 'D=M' );     // prev REG address value
            alines.push( "@LCL" );
            alines.push( 'M=D' );

            // restore SP
            alines.push( '@13' );
            alines.push( 'D=M' );
            alines.push( '@SP' );
            alines.push( 'M=D' );

            // push return value
            alines.push( '@14' );
            alines.push( 'D=M' );
            alines.push( '@SP' );
            alines.push( 'AM=M+1' );
            alines.push( 'A=A-1' );
            alines.push( 'M=D' );

            // jump to the return address
            alines.push( '@15' );
            alines.push( 'A=M' );
            alines.push( '0;JMP' );
        }
        else if( match( line, /^call +(\S+) +(\d+)/ ) ) {
            readystack( 'PUSH' );
            //
            // put the return address for this point on the
            // stack, then save the LCL ARG THIS THAT of this
            // frame onto the stack. Create a new LCL and put
            // the stack there.
            //
            let callfun = matches[1];
	        let args = matches[2];

            // push return addy onto the stack
            let returnlab = label('RET');
            alines.push( '@' + returnlab );
            alines.push( 'D=A' );
            alines.push( '@SP' );
            alines.push( 'M=M+1' );
            alines.push( 'A=M-1' );
            alines.push( 'M=D' );

            // push the old LCL ARG THIS THAT onto the stack
            // stack at the new LCL
	        [ 'LCL', 'ARG', 'THIS', 'THAT' ].forEach( REG => {
                alines.push( "@" + REG );
                alines.push( 'D=M' );
                alines.push( '@SP' );
                alines.push( 'M=M+1' );
                alines.push( 'A=M-1' );
                alines.push( 'M=D' );
            } );

            // set LCL to where SP is
            alines.push( 'D=A+1' );
            alines.push( '@LCL' );
            alines.push( 'M=D' );

            // set new ARG
            let back = 5 + args;
            alines.push( "@" + back );
            alines.push( 'D=D-A' );
            alines.push( '@ARG' );
            alines.push( 'M=D' );

            // jump to the function
            alines.push( '@' + callfun );
            alines.push( '0;JMP' );

            alines.push( "(" + returnlab + ")" );

        } //call
	    else if( match( line, /^label\s+(\S+)/ ) ) {
	        readystack( 'PUSH' );
	        alines.push( "(" + fun + '.' + matches[1] + ")" );
	    }
	    else if( match( line, /^goto +(\S+)/ ) ) {
            readystack( 'PUSH' );
            alines.push( '@' + fun + '.' + matches[1] );
            alines.push( '0;JMP' );
        }
        else if( /^if-goto +(\S+)/ ) {
            readystack( 'POP' );
            alines.push( '@' + fun + '.' + matches[1] );
            alines.push( 'D;JNE' );
        }
        let vend = alines.length;
        vlines.push( [alines[0],raw_line] );
        for( let i=(vstart+1); i<vend; i++ ) {
            section.push( [alines[i],undefined] );
        }
    } );
    readystack( 'PUSH' );
    return vlines;
};
