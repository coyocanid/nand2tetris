/*
  This emulates the hack processor outlined in the Nand2Tetris 
  class material.

  This is a simple processor that includes a ROM, a RAM,
  an A register, a D register and a Program Clock.
  
  This is a 16 bit processor.

  The instructions, for simplicity, are each a 16 length
  string of values 0 and 1.

  If the high bit of the op code is 0, this is an instruction
  to set the value of the A register.

*/

// opcode - object representing the opcode
// word - binary hack representation of the op code
// op   - assembly represensation of the op code


const _num2binstr = (num,bits) => {
    let strval = '';
    for( let i=(bits-1); i>-1; i-- ) {
        let exp = 2**i;
        if( num >= exp ) {
            strval += '1';
            num -= exp;
        } else {
            strval += '0';
        }
    }
    return strval;
}
const _not = num => {
    let ret;
    for( let i=0; i<16; i++ ) {
	if( num.charAt(i) === '0' ) {
	    ret = ret + '1';
	} else {
	    ret = ret + '0';
	}
    }
    return ret;
};

const _neg = num => _add( _not(num), 1 );

const _add = (a,b) => {
    let ret = [];
    let carry = false;
    for( let i=15; i> -1; i-- ) {
	if( a.charAt(i) === '1' && b.charAt(i) === '1' ) {
	    if( carry ) {
		ret[i] = true;
	    } else {
		carry = true;
	    }
	}
	else if( a.charAt(i) === '1' || b.charAt(i) === '1' ) {
	    if( ! carry ) {
		ret[i] = true;
	    }
	}
	else if( carry ) {
	    ret[i] = true;
	    carry = false;
	}
    }
    return ret.map( b => b ? '1' : '0' ).join('');
};

const _sub = (a,b) => {
    let ret = [];
    let borrow = false;
    for( let i=15; i> -1; i-- ) {

	let topOne = ( borrow && a.charAt(i) === '1' ) || ! borrow;
	if( b.charAt(i) === '1' ) {
	    if( topOne ) {
		borrow = false;
	    } else {
		ret[ i ] = true;
		borrow = true;
	    }
	}
	else {
	    if( topOne ) {
		ret[ i ] = true;
	    }
	    borrow = false;
	}
    }
    return ret.map( b => b ? '1' : '0' ).join('');
};
const _and = (a,b) => {
    let ret = [];
    for( let i=0; i<16; i++ ) {
	if( a.charAt(i) === '1' && b.charAt(i) === '1' ) {
	    ret[i] = true;
	}
    }
    return ret.map( b => b ? '1' : '0' ).join('');
};
const _or = (a,b) => {
    let ret = [];
    for( let i=0; i<16; i++ ) {
	if( a.charAt(i) === '1' || b.charAt(i) === '1' ) {
	    ret[i] = true;
	}
    }
    return ret.map( b => b ? '1' : '0' ).join('');
};

const comptbl = {
    '0'   : ['0101010', () => '0000000000000000' ],
    '1'   : ['0111111', () => '0000000000000001' ],
    '-1'  : ['0111010', () => '1111111111111111' ],
    'D'   : ['0001100', comp => comp.D ],
    'A'   : ['0110000', comp => comp.A ],
    'M'   : ['1110000', comp => comp.M ],
    '!D'  : ['0001101', comp => _not(comp.D) ],
    '!A'  : ['0110001', comp => _not(comp.A) ],
    '!M'  : ['1110001', comp => _not(comp.M) ],
    '-D'  : ['0001111', comp => _neg(comp.D) ],
    '-A'  : ['0110011', comp => _neg(comp.A) ],
    '-M'  : ['1110011', comp => _neg(comp.M) ],
    'D+1' : ['0011111', comp => _add(comp.D,1) ],
    'A+1' : ['0110111', comp => _add(comp.A,1) ],
    'M+1' : ['1110111', comp => _add(comp.M,1) ],
    'D-1' : ['0001110', comp => _sub(comp.D,1) ],
    'A-1' : ['0110010', comp => _sub(comp.A,1) ],
    'M-1' : ['1110010', comp => _sub(comp.M,1) ],
    'D+A' : ['0000010', comp => _add(comp.D,comp.A) ],
    'D+M' : ['1000010', comp => _add(comp.D,comp.M) ],
    'D-A' : ['0010011', comp => _sub(comp.D,comp.A) ],
    'D-M' : ['1010011', comp => _sub(comp.D,comp.M) ],
    'A-D' : ['0000111', comp => _sub(comp.A,comp.D) ],
    'M-D' : ['1000111', comp => _sub(comp.M,comp.D) ],
    'D&A' : ['0000000', comp => _and(comp.D,comp.A) ],
    'D&M' : ['1000000', comp => _and(comp.D,comp.M) ],
    'D|A' : ['0010101', comp => _or(comp.D,comp.A) ],
    'D|M' : ['1010101', comp => _or(comp.D,comp.M) ],
};

const jumptbl = {
    JMP : '111',
    JEQ : '010',
    JLT : '100',
    JNE : '101',
    JGE : '011',
    JLE : '110'
};

const dsttbl = {
    ADM : '111',
    M   : '001',
    D   : '010',
    MD  : '011',
    A   : '100',
    AM  : '101',
    AD  : '110'
};

let matches = [];
const match = ( str, regex ) => {
    matches.length = 0;
    let m = str.match( regex );
    if( m ) {
	m.forEach( ma => matches.push(ma) );
	return true;
    }
    return false;
}

const SEG = {
    'argument' : 'ARG',  //1
    'local'    : 'LCL',  //2
    'this'     : 'THIS', //3
    'that'     : 'THAT', //4
};
const vm2assembly = (lines,root) => {
    let alines = [];
    
    let labels = 0;
    let label = (lab) => {
	    lab = lab || 'L';
	    return lab + '_' + ++labels;
    };
    
    let push_reg, push_idx;
    let readystack = push_or_pop => {
	if ( push_reg === undefined ) {
	    if ( push_or_pop === 'POP' ) {
		alines.push( "@SP" );
		alines.push( "AM=M-1" );
		alines.push( "D=M" );
	    }
	    return 0;
	}
	if ( push_reg === 'constant' ) {
	    alines.push( "@" + push_idx );
	    alines.push( "D=A" );
	}
	else if ( push_reg.match( /^(this|that|local|argument)$/ ) ) {
	    let REG = SEG[push_reg];
	    if ( push_idx > 3 ) {
		alines.push( "@" + push_idx );
		alines.push( "D=A" );
		alines.push( "@" + REG );
		alines.push( "A=M+D" );
            }
	    else if( push_idx > 0 ) {
		alines.push( "@" + REG );
		alines.push( 'A=M+1' );
		for ( let i=2; i<=push_idx; i++ )
		{
		    alines.push( 'A=A+1' );
		}
            }
	    else
            {
		alines.push( "@" + REG );
		alines.push( 'A=M' );
	    }
	    alines.push( 'D=M' );
	}
	else if ( push_reg==='static' ) {
	    alines.push( "@" + root + '.' + push_idx );
	    alines.push( 'D=M' );
	}
	else if ( match( push_reg, /^(temp|pointer)$/ ) ) {
	    let pos = push_idx + ( push_reg==='temp' ? 5 : 3 );
	    alines.push( "@" + pos );
	    alines.push( 'D=M' );
	}

	if ( push_or_pop==='PUSH' ) {
	    alines.push( '@SP' );
	    alines.push( 'M=M+1' );
	    alines.push( 'A=M-1' );
	    alines.push( 'M=D' );
	}

	push_reg = undefined;
	return 1;
    };

    let fun, locals;
    
    lines.forEach( raw_line => {
	line = raw_line.replace( /\/\/.*/, '' );
	if( line.match( /^\s*$/ ) ) {
	    // comment or empty line. ignore
	}
	else if ( match( line, /^push +(\S+) +(\d+)/ ) ) {
            readystack( 'PUSH' );
            push_reg = matches[1];
            push_idx = matches[2];
        }
        else if ( match( line, /^pop +static +(\d+)/ ) ) {
            readystack( 'POP' );
            alines.push( '@' + root + '.' + matches[1] );
            alines.push( 'M=D' );
        }
        else if ( match( line, /^pop +(temp|pointer) +(\d+)/ ) ) {
            readystack( 'POP' );
            let pos = matches[2] + ( matches[1]==='temp' ? 5 : 3 );
            alines.push( '@' + pos );
            alines.push( 'M=D' );
        }
        else if ( match( line, /^pop +(argument|that|this|local) +(\d+)/ ) ) {
            readystack( 'POP' );
            let reg = SEG[matches[1]];
            let pos = matches[2];

            if ( pos > 11 ) {
                alines.push( '@13' );
                alines.push( 'M=D' ); //store the popped value

                alines.push( '@' + pos );
                alines.push( 'D=A' );
                alines.push( '@' + reg );
                alines.push( 'D=D+M' );  //the location to pop to

                alines.push( '@14' );
                alines.push( 'M=D' );

                alines.push( '@13' );
                alines.push( 'D=M' );

                alines.push( '@14' );
                alines.push( 'A=M' );
                alines.push( 'M=D' );
            }
            else if( pos > 0 ) {
                alines.push( '@' + reg );
                alines.push( 'A=M+1' );
		for ( let i=2; i<=pos; i++ ) {
                    alines.push( 'A=A+1' );
                }
                alines.push( "M=D" );
            }
            else {
                alines.push( '@' + reg );
                alines.push( 'A=M' );
                alines.push( "M=D" );
            }
	} // pop
        else if( line==='add' || line==='sub' ) {
            // x + y   OR   x - y
            // y is the last one on the stack
            readystack( 'POP' );

            // D has the y value
            // SP is set at the y value position
            // stay at the y position and do the
            // arithmetic on the x position

            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( (line==='add' ? 'M=D+M' : 'M=M-D' ) );

        }
        else if( line==='eq' ) {
            // x == y ? then put -1 or 0 on the x spot
            readystack( 'POP' );
            
            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( 'D=D-M' );
            let lab = label('IF');
            alines.push( '@' + lab );
            alines.push( 'D;JEQ' );
            alines.push( 'D=-1' );
            alines.push( "(" + lab + ")" );
            alines.push( 'D=!D' );
            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( 'M=D' );
        }
        else if( line==='lt' || line==='gt' ) {
            readystack( 'POP' );
            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( 'D=D-M' );
            let lab   = lab(line.toUpperCase());
            let lab2  = lab(line.toUpperCase());	    
            alines.push( '@' + lab );
            alines.push( line==='lt' ? 'D;JGT' : 'D;JLT' );
            alines.push( 'D=0' );
            alines.push( '@' + lab2 );
            alines.push( '0;JMP' );
            alines.push( "(" + lab + ")" );
            alines.push( 'D=-1' );
	    alines.push( "(" + lab2 + ")" );
            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( 'M=D' );
        }
        else if( line==='or' || line==='and' ) {
            readystack( 'POP' );
            alines.push( '@SP' );
            alines.push( 'A=M-1' );
            alines.push( line==='or' ? 'M=D|M' : 'M=D&M' );
        }
        else if( line==='not' || line==='neg' ) {
            let symb = line === 'not' ? '!' : '-';
            if( readystack() ) {
                // the value to be transformed
                // is in D
                alines.push( '@SP' );
                alines.push( 'A=M-1' );
                alines.push( "M=" + symb + "D" );
            }
            else{
                // update the last value in the
                // stack withalines.push( changing stack position
                alines.push( '@SP' );
                alines.push( 'A=M-1' );
		alines.push( "M=" + symb + "M" );
              }
        }
        else if( match( line, /^function +(\S+) +(\d+)/ ) ) {
            readystack( 'PUSH' );
            // label this function then initialize
            // the local variables by zeroing them out
            fun = matches[1];
            locals = matches[2];
            alines.push( "(" + fun + ")" );
	    if ( locals > 0 ) {
                if ( $locals > 5 ) {
                    alines.push( '@SP' );
                    alines.push( 'A=M' );
		    for ( let i=0; i<=locals; i++ ) {
			alines.push( 'M=0' );
			alines.push( 'A=A+1' );
                    }
		    alines.push( '@' + locals );
                    alines.push( 'D=A' );
                    alines.push( '@SP' );
                    alines.push( 'M=D+M' );
                  } // 6 + 2xlocal
                else {  //  local = 6
		    for ( let i=0; i<=locals; i++ ) {
			alines.push( '@SP' );
			alines.push( 'AM=M+1' );
			alines.push( 'A=A-1' );
			alines.push( 'M=0' );
                    }
                }
            }
        }
        else if( line === 'return' ) {
            readystack( 'PUSH' );

            //save the outgoing ARG, will become restored SP
            alines.push( '@ARG' );
            alines.push( 'D=M' );
            alines.push( '@13' );
            alines.push( 'M=D' );

            // save the return value in 14
            alines.push( '@SP' );
            alines.push( 'AM=M-1' ); //pop to after the outgoing LCL
            alines.push( 'D=M' );
            alines.push( '@14' );
            alines.push( 'M=D' );

	    [ 'THAT', 'THIS', 'ARG' ].forEach( REG => {
                alines.push( '@LCL' );
                alines.push( 'AM=M-1' );  // at prev REG
                alines.push( 'D=M' );     // prev REG address value
                alines.push( "@" + REG );
                alines.push( 'M=D' );
            } );
            alines.push( '@LCL');   //at prev ARG
            alines.push( 'A=M-1' );   //at at prev LOCAL
            alines.push( 'A=A-1' );   //at return address
            alines.push( 'D=M' );
            // stash return address in 15
            alines.push( '@15' );
            alines.push( 'M=D' );

            alines.push( '@LCL' );
            alines.push( 'AM=M-1' );  // at prev ARG
            alines.push( 'D=M' );     // prev REG address value
            alines.push( "@LCL" );
            alines.push( 'M=D' );

            // restore SP
            alines.push( '@13' );
            alines.push( 'D=M' );
            alines.push( '@SP' );
            alines.push( 'M=D' );

            // push return value
            alines.push( '@14' );
            alines.push( 'D=M' );
            alines.push( '@SP' );
            alines.push( 'AM=M+1' );
            alines.push( 'A=A-1' );
            alines.push( 'M=D' );

            // jump to the return address
            alines.push( '@15' );
            alines.push( 'A=M' );
            alines.push( '0;JMP' );
          }
        else if( match( line, /^call +(\S+) +(\d+)/ ) ) {
            readystack( 'PUSH' );
            //
            // put the return address for this point on the
            // stack, then save the LCL ARG THIS THAT of this
            // frame onto the stack. Create a new LCL and put
            // the stack there.
            //
            let callfun = matches[1];
	    let args = matches[2];

            // push return addy onto the stack
            let returnlab = label('RET');
            alines.push( '@' + returnlab );
            alines.push( 'D=A' );
            alines.push( '@SP' );
            alines.push( 'M=M+1' );
            alines.push( 'A=M-1' );
            alines.push( 'M=D' );

            // push the old LCL ARG THIS THAT onto the stack
            // stack at the new LCL
	    [ 'LCL', 'ARG', 'THIS', 'THAT' ].forEach( REG => {
                alines.push( "@" + REG );
                alines.push( 'D=M' );
                alines.push( '@SP' );
                alines.push( 'M=M+1' );
                alines.push( 'A=M-1' );
                alines.push( 'M=D' );
            } );

            // set LCL to where SP is
            alines.push( 'D=A+1' );
            alines.push( '@LCL' );
            alines.push( 'M=D' );

            // set new ARG
            let back = 5 + args;
            alines.push( "@" + back );
            alines.push( 'D=D-A' );
            alines.push( '@ARG' );
            alines.push( 'M=D' );

            // jump to the function
            alines.push( '@' + callfun );
            alines.push( '0;JMP' );

            alines.push( "(" + returnlab + ")" );

        } //call
	else if( match( line, /^label\s+(\S+)/ ) ) {
	    readystack( 'PUSH' );
	    alines.push( "(" + fun + '.' + matches[1] + ")" );
	}
	else if( match( line, /^goto +(\S+)/ ) ) {
            readystack( 'PUSH' );
            alines.push( '@' + fun + '.' + matches[1] );
            alines.push( '0;JMP' );
          }
        else if( /^if-goto +(\S+)/ ) {
            readystack( 'POP' );
            alines.push( '@' + fun + '.' + matches[1] );
            alines.push( 'D;JNE' );
        }

    } );
    readystack( 'PUSH' );
    
};

const assembly2program = lines => {
    
    let labels = {
	    SP     : 0,
	    LCL    : 1,
	    ARG    : 2,
	    THIS   : 3,
	    THAT   : 4,
	    KBD    : 24576,
	    SCREEN : 16384,
    }
    for( let i=0; i<16; i++ ) {
	    labels[ "R" + i ] = i;
    }

    let needsLabelOrVariableResolution = {}; // for @label that hasnt been yet defined
    let ops = [];
    let assembly_nice = [];
    
    lines.forEach( raw_line => {
	line = raw_line.replace( /\/\/.*/, '' );

	// line number and the line itself
	assembly_nice.push( [ops.length, raw_line ] );
	
	if( line.match( /^\s*$/ ) ) {
	    // comment or empty line. ignore
	}
	else if( line.charAt(0) === '(' ) {
	    let label = line.substr( 1, line.length-1 );
	    if( labels[ label ] ) {
		throw new Exception(line + " defined twice " );
	    }
	    let word = '0' + _num2binstr( ops.length );
	    lables[ label ] = ops.length;
	    if( needsLabelOrVariableResolution[ label ] ) {
		needsLabelOrVariableResolution[ label ].forEach( needsop => {
		    needsop.word = word;
		    needsop.val = ops.length;
		} );
		delete needsLabelOrVariableResolution[ label ];
	    }
	}
	// A instruction :  @number or @label
	else if( line.charAt(0) === '@' ) {
	    let op = { code : line,
		       line : ops.length,
		       type : 'A' };

	    let val = parseInt( line.substr(1) );
	    
	    // @label
	    if( isNaN( val ) ) {
		let label = line.substr( 1 );
		if( labels[label] ) {
		    op.val  = labels[label];
		    op.word = '0' + _num2binstr( op.val );
		} else {
		    // this either references a label that has not yet
		    // been defined, or it references a variable referencing
		    // a RAM address (starting at 16)
		    let needs = needsLabelOrVariableResolution[ label ];
		    if( needs ) {
			needs = [];
			needsLabelOrVariableResolution[ label ] = needs;
		    }
		    needs.push( op );
		}
	    }
	    else {
		op.num  = val;
		op.word = '0' + _num2binstr( val );
	    }
	    ops.push( op );
	    
	}
	// C instruction
	else {
	    let op = { line : line,
		       line_number : ops.length,
		       type : 'C',
		       jump : '000',     // pc doesnt jump
		       comp : '0101010', // compute zero
		       dest : '000' };   // data goes no where
	    
	    if( line.slice(-4,-3) === ';' ) {
		op.jump = jumptbl[ line.slice(-3) ];
	    }
	    
	    let parts = line.match( /(0|([AMD]+)*=)?([^;]*)/ );
	    if( parts ) {
		if( parts[2] !== undefined ) {
		    op.dest = dsttbl[ parts[2] ];
		}
		if( parts[3] !== undefined ) {
		    // computation with destination
		    op.comptxt = parts[3];
		    op.comp = comptbl[ parts[3] ][0];
		}
	    } else {
		throw new Exception( line + " : cant parse assembly instruction" );
	    }
	    op.word = '111' + op.comp + op.dest + op.jump;

	    ops.push( op );
	}
    } );

    // resolve variables
    let RAMaddress = 16;
    Object.keys( needsLabelOrVariableResolution ).forEach( key => {
	let val = RAMaddress++;
	let word = '0' + _num2binstr( val );
	needsLabelOrVariableResolution[key].forEach( op => {
	    op.val  = val;
	    op.word = word;
	} );
    } );
    
    return {
	ops              : ops,
	assembly_source  : lines,
	aseembly_nice    : aseembly_nice,
	words            : ops.map( op => op.word ),
    };

}; //assembly2program


let hackcomputer = {
    MAXROM : 32768,
    MAXMEM : 24576,
    MAXRAM : 16383,
    
    RAM : [],
    ROM : [],
    SCREEN  : 16384,       // 16384 - 24575  (512 columns x 256 rows)
    KEYBD   : 24576,

    A   : '',
    D   : '',
    PC  : 1,
    
    reset : function() {
        this.PC = 0;
	this.RAM = [];
    },

    load : function(program) {
	if( program.length > this.MAXROM ) {
	    throw new Exception( "Program Too Large" );
	}
	this.ROM = program;
    },
    
    tick : function() {
        if( this.PC > MAXROM ) {
            this.PC = 0;
        }
        this.PC = this.doOp( this.ROM[this.PC] );
    },
    
    doOp : function( op ) {
	if( op.type === 'A' ) {
	    this.A = op.word;
	}
	else {
	    let ALUoutput = comptbl[op.comp][1]( this );
	    if( op.dest ) {
		if( op.dest.charAt( 0 ) === '1' ) {
		    this.A = ALUoutput;
		}
		if( op.dest.charAt( 0 ) === '2' ) {
		    this.D = ALUoutput;
		}
		if( op.dest.charAt( 0 ) === '3' ) {
		    let A = this.A;
		    this.RAM[A] = ALUoutput;

		    // check if this memory maps to the screen
		    if( A > 16383 && A < 24576 ) {
			let row = Math.floor( (A - this.SCREEN)/256 );
			let col = (start - this.SCREEN) % 512;
			for( let i = 0; i<16; i++ ) {
			    if( ALUoutput.charAt(i) == '1' ) {
				ctx.fillRect( col, row, col, row );
			    } else {
				ctx.clearRect( col, row, col, row );
			    }
			}
		    }
		}
	    }
	    if( op.jump ) {
		let A = this.A;
		if( op.jump === '000' ||
		    ( op.jump.charAt(0) === '1' && ALUoutput.charAt(0) === '1' ) ||
		    ( op.jump.charAt(1) === '1' && ALUoutput === '0000000000000000' ) ||
		    ( op.jump.charAt(2) === '1' && parseInt( ALUoutput.substr(1) ) > 0 ) )
		{
		    return A;
		}
	    }
	    
	}
	return this.PC + 1;
    },
};

// link up keyboard

const kbdUI = document.getElementById( 'keyboard' );
const tnode = document.createTextNode('');
kbdUI.append( tnode );
window.addEventListener( 'keydown', ev => {
    let code = ev.shiftKey ? ev.keyCode : ev.keyCode + 32;
    tnode.nodeValue = ev.key + " = " + code;
    hackcomputer.RAM[ hackcomputer.KEYBD ] = code;
} );
window.addEventListener( 'keyup', ev => {
    tnode.nodeValue = '';
    hackcomputer.RAM[ hackcomputer.KEYBD ] = 0;
} );

const canv = document.getElementById( 'screen' );
const ctx = canv.getContext( '2d' );

// link up screen
const screensync = () => {
    let start = hackcomputer.SCREEN;
    while( start < hackcomputer.KEYBD ) {
	let screenbyte = hackcomputer.RAM[ start ];
	let row = Math.floor( (start - hackcomputer.SCREEN)/256 );
	let col = (start - hackcomputer.SCREEN) % 512;
	for( let i = 0; i<16; i++ ) {
	    if( screenbyte.charAt(i) == '1' ) {
		ctx.fillRect( col, row, col, row );
	    } else {
		ctx.clearRect( col, row, col, row );
	    }
	}
	start++;
    }
}

