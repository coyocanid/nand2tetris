let nodes   = [];
let output  = [];
let methods = {}; 
let node, className, classNode, subName, subNode, lbls, tagsub, ecount, pcount, line, pass;

let capturing;
const capture = () => {
  capturing = [];
};
const release = () => {
    let ret = capturing.concat([]);
    capturing = undefined;
    return ret;
};
const out = val => {
    if ( pass !== 'second' ) {
	return;
    }
    if ( capturing ) {
	capturing.push( val );
    } else {
	output.push( val );
    }
}; //out

const lbl = tag => {
    tag = tag || '';
    lbls++;
    return className+'.'+subName+'.'+tag+'.'+lbls;
}; //lbl

const findvar = (key,buckets) => {
    let vv;
    buckets.forEach( bucket => {
	if( vv === undefined && bucket[ key ] !== undefined ) {
	    vv = bucket[ key ];
	}
    } );
    return vv;
};

const taghandlers = {
    
    class : (ev,term,subcount ) => {
	// 'class' className '{' classVarDec* subroutineDec* '}'
	if ( ev === 'subn' && 2 === subcount ) {
	    className = term.val;
	    classNode = node;
	}
    }, //class

    classVarDec : (ev,term,subcount ) => {
	// ('static'|'field') type varName(',' varName)*';'
	if ( ev === 'subn' ) {
	    if ( 1 === subcount ) {
		node.varscope = term.val;
            }
	}
	else if ( 2 === subcount ) {
          node.vartype = term.val;
        }
	else if ( subcount > 2 && term.name !== 'symbol' ) {
            let varname = term.val;
	    if( classNode.node === undefined ) {
		classNode.node = {};
	    }
	    if( classNode.node.varscope === undefined ) {
		classNode.node.varscope = {};
	    }
            let vars = classNode.node.varscope;
            let idx = Object.keys( vars ).length;
	    if( vars.varname ) {
		throw new Error( "Name '"+term.val + "' already defined in "+node.varscope+" scope in class " +className );
	    }
            // [ index, type, name, scope ]
            vars[term.val] = [ idx, node.vartype, varname, node.varscope === 'static' ? 'static' : 'this'];
        }
    }, //classVarDec

    doStatement : (ev,term,subcount ) => {
	// 'do' subroutineCall ';'
	// 'do' ( className | varName ) '.' subroutineName '(' expressionList ')' | subroutineName '(' expressionList ')' ';'
	if( ev !== 'subn' ) {
	    return;
	}

	if( term.name === 'identifier' ) {
	    node.firstId = node.lastId;
	    node.lastId  = term.val;
	}
	else if( term.val === '(' ) {
	    // see if this is an instance method. an instance is a variable, which could be
	    // a parameter, local var, class var or instance var

	    // TODO - field only for methods method 
	    if( node.firstId ) {
		let varval = findvar( node.firstId, [subNode.params, subNode.vars, classNode.static, classNode.field] );
		if ( varval ) { //instance var, otherwise a class
		    node.methodclass = varval[1]; // the class (vartype) of the method
		    out( "push " + varval[3] + ' ' + varval[0] );  // put the var's this on the stack
		}
	    }
	    else if ( methods[node.lastId] ) {
		node.methodclass = className;
		out( "push pointer 0" ); // put the var's this on the stack
	    }
	}
	else if( term.val === ')' ) {
	    if( node.methodclass ) {
		ecount++;
	    }
	    let cname = node.methodclass || node.firstId || className;
	    out( "call " + cname + '.' + node.lastId + ' ' + ecount );
	    out( "pop temp 0" ); // get rid of return value.
	}
    }, //doStatement

    expression : (ev,term,subcount ) => {
	// term (op term)*
	// term values are pushed on the stack already by the term handler, so only the symbols do stuff here
	let closeSymbol = ev === 'close';
	if ( ev === 'subn' ) {
	    if ( term.name === 'symbol' ) {
		node.lastSymbol = term.val;
	    } else {
		closeSymbol = node.lastSymbol;
            }
	}
	if ( closeSymbol && node.lastSymbol ) {
	    let s = node.lastSymbol;
	    if ( s === '+' ) {
		out( 'add' );
            } else if ( s === '-' ) {
		out( 'sub' );
            } else if ( s === '*' ) {
		out( "call Math.multiply 2" );
            } else if ( s === '/' ) {
		out( "call Math.divide 2" );
            } else if ( s === '&' ) {
		out( 'and' );
            } else if ( s === '|' ) {
		out( 'or' );
            } else if ( s === '<' ) {
		out( 'lt' );
            } else if ( s === '>' ) {
		out( 'gt' );
            } else if ( s === '=' ) {
		out( 'eq' );
            }
	    else {
		throw new Error( "Unknown symbol '" + s + "'" );
            }
	    node.lastSymbol = undefined;
	}
    }, //expression

    expressionList : (ev,term,subcount ) => {
	// (expression ( ',' expression )* )?
	if ( ev === 'subn' && term.val !== ',' ) {
	    node.ecount++;
	}
	if ( ev === 'close' ) {
	    ecount = node.ecount || 0;
	}
    }, //expressionList

    ifStatement : (ev,term,subcount ) => {
	// 'if' '(' expression ')' '{' statements '}' ( 'else' '{' statements '}' )?
	if ( ev === 'start' ) {
	    node.yeslab  = lbl( 'true' );
	    node.skiplab = lbl( 'false' );
	} else if ( ev === 'close' ) {
	    if ( node.endlab ) {
		out( "label " + node + '.' + endlab );
            } else { 
		out( "label " + node + '.' + skiplab );
            }
	} else if ( 3 === subcount ) { //expression
	    out( "if-goto " +node.yeslab );
	    out( "goto " + node.skiplab );
	    out( "label " + node.yeslab );
	} else if ( 8 === subcount ) { //else statements
	    node.endlab = lbl( 'end' );
	    out( "goto " + node.endlab );
	    out( "label " + node.skiplab );
	}
    }, //if

    letStatement : (ev,term,subcount ) => {
	// 'let' varName ( '[' expression ']' )? '=' expression ';'
	if ( ev === 'subn' ) {
	    if ( 2 === subcount ) {
		// find the var
		let varval = findvar( term.val, [subNode.params, subNode.vars, classNode.static, classNode.field] );
		if( varval ) {
		    node.var = varval;
		} else {
		    throw new Error( "Couldn't find variable '" + term.val + "' in line " + line );
		}
            }
	    else if ( 3 === subcount && term.val === '[' ) {
		capture(); //grabs the expression resolving the array index
            }
	    else if ( 5 === subcount && term.val === ']' ) {
		node.capture = release(); // stores the expression resolveing the array index
            }
	}
	else if ( ev === 'close' ) {
	    // at this point, the value of the expression is on the stack
	    let idx = node.var[0];
	    let type = node.var[1];
	    let name = node.var[2];
	    let seg = node.var[3];
	    if ( subcount === 8 ) {
		// value to assign is now on the stack
		// put the expression resolving to the array index on the stack
		node.capture.forEach( c => out );
          
		out( "push "+seg+" "+idx );
		out( "add" );
		out( "pop pointer 1" );
		out( "pop that 0" );
            } else {
		out( "pop " + seg + ' ' + idx );
            }
	}
    }, //letstatement

    parameterList : (ev,term,subcount ) => {
	// ((type varName)(',' type varName)*)?
	if ( ev === 'subn' ) {
	    if( ! node.vartype ) {
		node.vartype = term.val;
	    }
	    else if ( term.val !== ',' ) {
		let vars = subNode.params;
		if( ! vars ) {
		    vars = {};
		    subNode.params = vars;
		}
		let idx = Object.keys( vars ).length;
		if( methods.subName ) {
		    idx++;
		}
		vars[ term.val ] = [ idx, node.vartype, term.val, 'argument' ];
            } else { // is ','
		node.vartype = undefined;
            }
	}
	else if ( ev === 'close' ) {
	    pcount = subcount;
	}
    }, //parameterList

    returnStatement : (ev,term,subcount ) => {
	// 'return' expression? ';'
	if ( ev === 'close' ) {
	    if( subcount === 2 ) {
		out( 'push constant 0' );
	    }
	    out( 'return' );
	}
    }, //returnStatement

    statements : (ev,term,subcount ) => {
	if( ev === 'start' ) {
	    if( ! subNode.init ) {
		subNode.init = 1;
		let count = Object.keys( subNode.vars ).length;
		out( "function "+className+'.'+subName+' '+count );
		if ( subNode.mtype === 'constructor' ) {
		    var consty = classNode.field ? Object.keys(classNode.field).length : 0;
		    out( "push constant " + consty );
		    out( "call Memory.alloc 1" );
		    out( "pop pointer 0" );
		}
		else if ( subNode.mtype === 'method' ) {
		    // put the argument zero on the pointer 0 as this
		    out( "push argument 0" );
		    out( "pop pointer 0" );
		}
	    }
	}
    }, //statements

    subroutineBody : (ev,term,subcount ) => {
	//NOOP
    },

    subroutineDec : (ev,term,subcount ) => {
	// ('constructor'|'function'|'method') ('void' | type) subroutineName '(' parameterList ')' subroutineBody
	if ( 1 === subcount ) {
	    node.mtype = term.val;
	}
	else if( 3 === subcount ) {
	    subName = term.val;
	    subNode = node;
	    if( node.mtype === 'method' ) {
		methods.subName = 1;
	    }
	}
    }, //subroutineDec

    term : (ev,term,subcount ) => {
	//integerConstant | stringConstant | keywordConstant | subroutineCall | varName '[' expression ']'  | varName | '(' expression ')' | unaryOp term",
	//subroutineCall - ( className | varName ) '.' subroutineName '(' expressionList ')' | subroutineName '(' expressionList ')'
	if ( ev === 'subn' ) {
	    let name = term.name;
	    let val = term.val;
	    if ( name === 'integerConstant' ) {
		out( "push constant " + val );
            }
	    else if ( name === 'stringConstant' ) {
		let len = val.length;
		out( "push constant " + len );
		out( 'call String.new 1' );
		for( let i=0; i<len; i++ ) {
		    out( "push constant " + val.charCodeAt(i) );
		    out( 'call String.appendChar 2'); // pushes string obj back on the stack
		}
            }
	    else if ( name === 'keyword' ) {
		if ( val === 'true' ) {
		    out( "push constant 1" );
		    out( "neg" );
		} else if ( val === 'false' || val === 'null' ) {
		    out( "push constant 0" );
		} else {//this
		    out( "push pointer 0" );
		}
            }
	    else if ( name === 'identifier' ) {
		//variable or sub call
		node.firstId = node.lastId;
		node.lastId = val;
            }
	    
	    else if ( name === 'symbol' ) {
		// '(', ')', '.', '[', ']', '-' or '~'
		if ( val === '~' || val === '-' ) { //unop
		    node.unop = val;
		} else if ( val === ']' ) {
		    let varval = findvar( node.lastId, [subNode.params, subNode.vars, classNode.static, classNode.field] );
		    if( varval === undefined ) {
			throw new Error( "Unknown variable '" +node.lastId+"' in line "+line );
		    }
		    if( varval[ 1 ] !== 'Array' ) {
			throw new Error( "variable '" + node.lastId + "' is not array" );
		    }
		    out( "push " + varval[3] + ' ' + varval[0] );
		    out( 'add' );
		    // problem here with sequential arrays in same statement
		    out( 'pop pointer 1' );
		    out( 'push that 0' ); 
		    node.firstId = undefined;
		    node.lastId = undefined;
		}
		else if ( val === '(' && node.lastId ) {
                    // method call
		    let varval = findvar( node.firstId, [subNode.params, subNode.vars, classNode.static, classNode.field] );
                    if ( varval ) {
			node.methodclass = varval[1];
			out( "push " + varval[3] + ' ' + varval[0] );
                    }
                    else if( methods[node.lastId] ) {
			node.methodclass = className;
			out( "push pointer 0" );
                    }
		}
		else if ( val === ')' && node.lastId ) {
		    let cname = node.methodclass || node.firstId || className;
		    let varval = findvar( node.firstId, [methods, subNode.params, subNode.vars, classNode.static, classNode.field] );
		    if( varval ) {
			++ecount;
		    }
		    out( "call "+cname+'.'+node.lastId+' '+ecount );
		    node.firstId = undefined;
		    node.lastId = undefined;
		}
            }
	} //subnode

	else if ( ev === 'close' ) {
	    if ( node.unop === '-' ) {
		out( "neg" );
            }
	    else if ( node.unop === '~' ) {
		out( "not" );
            }
	    else if ( node.lastId ) {
		//variable
		let varvar = findvar( node.lastId, [subNode.params, subNode.vars, classNode.static, classNode.field] );
		if (varvar) {
		    let idx = varvar[0];
		    let type = varvar[1];
		    let name = varvar[2];
		    let seg = varvar[3];
		    out( "push " + seg + ' '+ idx );
		} else {
		    throw new Erro( "Variable '" + node.lastId + "' not found" );
		}
            }
	}
    }, //term

    varDec : (ev,term,subcount ) => {
	// 'var' type varName (',' varName)* ';'
	if( ev !== 'subn' ) {
	    return;
	}

	if ( 2 === subcount ) {
	    node.vartype = term.val;
	}
	else if ( 2 < subcount && term.name !== 'symbol') {
	    let vars = subNode.vars;
	    if( vars === undefined ) {
		vars = {};
		subNode.vars = vars;
	    }
	    let idx = Object.keys( vars ).length;
	    vars[ term.val ] = [ idx, node.vartype, term.val, 'local' ];
	}
    }, //varDec

    whileStatement : (ev,term,subcount ) => {
	// 'while' '(' expression ')' '{' statements '}'
	if ( ev === 'start' ) {
	    let top = lbl( 'top' );
	    node.top = top;
	    node.do = lbl( 'do' );
	    node.end  = lbl( 'end' );
	    out( "label " + top );
	}
	else if ( ev === 'subn' ) {
	    if ( 3 === subcount ) { //expression
		out( "if-goto " + node.do );
		out( "goto " + node.end );
		out( "label " + node.do );
            }
	    else if ( 6 === subcount ) { //statements
		out( "goto " + node.top );
            }
	}
	else if ( ev === 'close' ) {
	    out( "label " + node.end );
	}
    } //whileStatement
}; //taghandlers


const process = line => {

    line = lines[ 0 ];
    if ( line[ 2 ] === 'BEGIN' ) {
	// OPEN
	node = { type : 'NODE', name : line[3], subcount : 0 };
	tagsub = taghandlers( line[3] );
	tagsub("start", undef, 0);
	nodes.push( node );
    }
    else if ( line[ 2 ] === 'END' ) {
	// CLOSE
	let deepernode = nodes.pop();
	tagsub("close",undef,deepernode ? deepernode.subcount : 0 );
	node = nodes[nodes.length-1]; //previous one
	if ( node ) {
            node.subcount++;
            tagsub = taghandlers( node.name );
            tagsub("subn", deepernode, node.subcount ||0);
        }
    }
    else if ( lines[ 2 ] === 'TERMINAL' ) {
	// TERMINAL
	let term = { type : 'TERM', name : lines[3], val : lines[4] };
	node.subcount++;
	tagsub("subn", term, node.subcount);
    }
} //process

N2T.compile = lines => {
    pass = 'initial';
    lines.forEach( line => process );
    pass = 'second';
    lines.forEach( line => process );
}; //compile

