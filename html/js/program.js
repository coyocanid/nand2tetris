// its possible this really isnt needed

// program, a general programmy sort of thing.
/*
  from_jack
  from_tokens
  from_vm
  from_assembly
*/
const newProgram = () => {
    let classes     = {}; //name -> jack class. one per 'file'.
    let ops         = []; // list of ops
    let rows        = []; // [op, assembly, vm, tokens, jack]
    let PC2lineIdx  = {}; // pc -> index of row where this happens
    const program = {
        clear : () => {
            classes     = {};
            ops         = [];
            rows        = [];
            PC2lineIdx  = {};
            return program;
        },
        removeJackClass : name => {
            delete classes[name];
            return program;
        },
        setJackClass : (name,clsLines) => {
            classes[name] = clsLines;
            let vmLines = [];
            Object.values(classes).forEach( jackLines => {
                let vmRows = compile( jackLines );   // each row [ vminstruction, jack line, line number ]
                vmLines = vmLines.concat( vm.Rows );
            } );
            program.setVMCode( vmLines );
            return program;
        },
        setVMCode : vmLines => {
            program.setAssemblyCode( N2T.generate_assembly( vmLines ) );
            return program;
        },
        setAssemblyCode : assemblyLines => {
            let assembled = N2T.assemble( assemblyLines );
            rows = assembled.lineData;
            PC2lineIdx = assembled.PC2lineIdx;
            ops = assembled.lineData.filter( ln => ln[0] !== undefined ).map( ln => ln[1] );
            return program;
        },
        ops  : ()       => ops,
        rows : ()       => rows,
        PC2lineIdx : () => PC2lineIdx,
    };
    return program;
};
