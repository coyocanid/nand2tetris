let b2n = N2T.consts.binstr2num;

N2T.make_hackcomputer = () => {
    let comptbl = N2T.consts.get_comp_table();

    return {
        MAXROM : 32768,
        MAXMEM : 24576,
        MAXRAM : 16383,
        
        RAM : [],
        ROM : [],
        SCREEN  : 16384,       // 16384 - 24575  (512 columns x 256 rows)
        KBD     : 24576,

        A   : '0000000000000000',
        aval : 0,
        D   : '0000000000000000',
        PC  : 0,
        
        reset : function() {
            this.PC = 0;
            this.A = '0000000000000000';
            this.aval = 0;
            this.D = '0000000000000000';
	        this.RAM = [];
        },

        loadOps : function(words) {
	        if( words.length > this.MAXROM ) {
	            throw new Exception( "Program Too Large" );
	        }
	        this.ROM = words;
        },
        
        tick : function() {
            if( this.PC > this.MAXROM ) {
                this.PC = 0;
            }
            this.PC = this.doOp( this.ROM[this.PC] );
        },

        _RAMListener : undefined,
        onRAMChange : function( cb ) {
            this._RAMListener = cb;
        },
        fire : function( etype, val ) {
            let ev = typeof val === 'object' ? val : { value : val };
            ev.type = etype;
            this.listeners.forEach( pair => {
                if( etype === pair[0] ) {
                    pair[1](ev);
                }
            } );
        },

        M : function() {
            if( this.aval > 24576 || this.aval < 0 ) {
                throw new Error( 'Illegal Memory Address' );
            }
            return this.RAM[this.aval] || '0000000000000000';
        },
        
        doOp : function( op ) {
            if( op === undefined ) {
	            return this.PC; //go nowhere I'm afraid
            }
	        if( op.type === 'A' ) {
	            this.A = op.word;
                this.aval = op.val;
	        }
	        else {
	            let ALUoutput = comptbl[op.comp][1]( this );
                let A = this.aval;
	            if( op.dest ) {
		            if( op.dest.charAt( 2 ) === '1' ) { // RAM
                        if( A > 24575 || A < 0 ) {
                            throw new Error( 'Illegal Memory Address' );
                        }
		                this.RAM[A] = ALUoutput;
                        this._RAMListener && this._RAMListener( A );
		            }
		            if( op.dest.charAt( 0 ) === '1' ) {
		                this.A = ALUoutput;
                        this.aval = b2n( ALUoutput );
		            }
		            if( op.dest.charAt( 1 ) === '1' ) {
		                this.D = ALUoutput;
		            }
	            }
                else {
                    A = b2n(this.A);
                }
                
	            if( op.jump && ( op.jump === '111' ||
		                         ( op.jump.charAt(0) === '1' && ALUoutput.charAt(0) === '1' ) ||
		                         ( op.jump.charAt(1) === '1' && ALUoutput === '0000000000000000' ) ||
		                         ( op.jump.charAt(2) === '1' && ALUoutput.charAt(0) !== '1' && ALUoutput !== '0000000000000000' ) ) )
		        {
		            return A;
		        }
	        }
	        return this.PC + 1;
        },
    };
};
