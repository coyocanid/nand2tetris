let passes = 0;
let fails = 0;
const _is = ( a, b, desc ) => {
    if( a !== b ) {
        fails++;
        console.log( "FAIL " + desc + " got '" + a + "', expected '" + b + "'" );
//        throw new Error( "FAIL " + desc + " got '" + a + "', expected '" + b + "'" );
    }
    passes++;
//    console.log( "Passed " + desc );
};

const _state = ( computer, PC, A, M, D, msg ) => {
    _is( computer.PC, PC, msg + " program clock" );
    _is( computer.A, A, msg + " A reg" );
    _is( computer.D, D, msg + " D reg" );
    if( M === 'n/a' ) {
        _fail( () => { computer.M(); } , "Illegal Memory Address" );
    } else {
        _is( computer.M(), M, msg + " M reg" );
    }
    computer.tick();
};

const _fail = (code,msg) => {
    try {
        code();
        throw new Error( "SHOULD FAIL" );
    } catch(err) {
        if( err.message !== msg ) {
            fails++;
            console.log( "FAIL did not throw '" + msg + "', got " + err.message);
//            throw err;
        }
//        console.log( "Passed throwing " + msg );
        passes++;
    }
};

N2T.run_tests = () => {

    _is( _num2binstr(0,16), '0000000000000000', "zero num correct" );
    _is( _num2binstr(-1,16), '1111111111111111', "zero num correct" );

    // test processor math

    _is( _not("0101111101010101"), "1010000010101010", "NOT 1" );
    _is( _not("1111111111111111"), "0000000000000000", "NOT 2" );
    _is( _not("0000000000000000"), "1111111111111111", "NOT 3" );
    _is( _not("1010101010101010"), "0101010101010101", "NOT 4" );

    _is( _neg("0000000000000000"), "0000000000000000", "NEG 0" );
    _is( _neg("0000000000000001"), "1111111111111111", "NEG 1" );
    _is( _neg("0000000000001000"), "1111111111111000", "NEG 2" );
    _is( _neg("0000000000001010"), "1111111111110110", "NEG 3" );

    _is( _and("1111111111111111","1111101111111111"), "1111101111111111", "AND 1" );
    _is( _and("1111111111111111","1111101111111110"), "1111101111111110", "AND 2" );
    _is( _and("1010101010101010","1100110011001100"), "1000100010001000", "AND 3" );

    _is( _or("1111111111111111","1111101111111111"), "1111111111111111", "OR 1" );
    _is( _or("1111111111111111","1111101111111110"), "1111111111111111", "OR 2" );
    _is( _or("1010101010101010","1100110011001100"), "1110111011101110", "OR 3" );

    _is( _add("0000000000000001","0000000000000011"), "0000000000000100", "AND 1" );
    _is( _add("0111111111111111","0000000000000001"), "1000000000000000", "AND 2" );
    _is( _add("1111111111111111","0000000000000001"), "0000000000000000", "AND 3" );
    _is( _add("0000000000000001","0000000000000000"), "0000000000000001", "AND 1" );

    _is( _sub("0000000000000001","0000000000000011"), "1111111111111110", "SUB 1" );
    _is( _sub("0111111111111111","0000000000000001"), "0111111111111110", "SUB 2" );
    _is( _sub("1111111111111111","0000000000000000"), "1111111111111111", "SUB 3" );
    _is( _sub("0000000000000000","0000000000000001"), "1111111111111111", "SUB 4" );
    _is( _sub("0000000000000001","0000000000001101"), "1111111111110100", "SUB 5" );

    let failprog = [
        '(FAIL)',  // test label
        '(FAIL)',  // test label
    ].map( l => [l] );

    _fail( () => { N2T.assemble_program( failprog ); }, '(FAIL) defined twice' );

    // ********************* TEST ASSEMBLY ********************

    // test destination labels, comments, emtpy lines and simple jumps
    let p1 = N2T.assemble_program(
        [
            '@BOTTOM', //0
            '0;JMP',   //1
            '(TOP)',
            '@END',    //2
            '',             //emtpy line
            '0;JMP',   //3
            '// this is a comment',
            '(BOTTOM)',
            '@TOP',    //4
            '0;JMP',   //5
            '(END)',
            '@END',    //6
            '0;JMP'    //7
        ].map( l => [l] ) );

    _is( p1.ops.length, 8, "8 operations" );

    let HC = N2T.make_hackcomputer();
    _state( HC, 0, '0000000000000000', '0000000000000000', '0000000000000000', "computer made" );

    HC.loadOps( p1.ops );
    _state( HC, 0, '0000000000000000', '0000000000000000', '0000000000000000', "P1 loaded program" );
    _state( HC, 1, '0000000000000100', '0000000000000000', '0000000000000000', "P1 set dest to bottom" );
    _state( HC, 4, '0000000000000100', '0000000000000000', '0000000000000000', "P1 PC moved to bottom label" );
    _state( HC, 5, '0000000000000010', '0000000000000000', '0000000000000000', "P1 set dest to top" );
    _state( HC, 2, '0000000000000010', '0000000000000000', '0000000000000000', "P1 PC moved to top label" );
    _state( HC, 3, '0000000000000110', '0000000000000000', '0000000000000000', "P1 set dest to end" );
    _state( HC, 6, '0000000000000110', '0000000000000000', '0000000000000000', "P1 moved to to end" );
    _state( HC, 7, '0000000000000110', '0000000000000000', '0000000000000000', "P1 infinite loop at end" );
    _state( HC, 6, '0000000000000110', '0000000000000000', '0000000000000000', "P1 moved to to end" );
    _state( HC, 7, '0000000000000110', '0000000000000000', '0000000000000000', "P1 infinite loop at end" );
    _state( HC, 6, '0000000000000110', '0000000000000000', '0000000000000000', "P1 moved to to end" );
    _state( HC, 7, '0000000000000110', '0000000000000000', '0000000000000000', "P1 infinite loop at end" );

    // test variable symbols, predefined I/O pointers (screen and keyboard), virtual registers and predefined pointers
    let p2 = N2T.assemble_program(
        [
            '@SWEET_SIXTEEN',
            '@SP',
            '@LCL',
            '@ARG',
            '@THAT',
            '@THIS',
            '@SCREEN',
            '@KBD',
            '@R0',
            '@R1',
            '@R2',
            '@R3',
            '@R4',
            '@R5',
            '@R6',
            '@R7',
            '@R8',
            '@R9',
            '@R10',
            '@R11',
            '@R12',
            '@R13',
            '@R14',
            '@R15',
            '@R16',
            '(END)',
            '@END',
            '0;JMP',
        ].map( l => [l] ) );
    HC.loadOps( p2.ops );
    _state( HC, 6, '0000000000000110', '0000000000000000', '0000000000000000', "P2 loaded program and not yet reset" );
    HC.reset();
    _state( HC, 0, '0000000000000000', '0000000000000000', '0000000000000000', "P2 loaded program and reset" );
    _state( HC, 1, '0000000000010000', '0000000000000000', '0000000000000000', "P2 var val correct" );
    _state( HC, 2, '0000000000000000', '0000000000000000', '0000000000000000', "P2 SP correct" );
    _state( HC, 3, '0000000000000001', '0000000000000000', '0000000000000000', "P2 LCL correct" );
    _state( HC, 4, '0000000000000010', '0000000000000000', '0000000000000000', "P2 ARG correct" );
    _state( HC, 5, '0000000000000100', '0000000000000000', '0000000000000000', "P2 THAT correct" );
    _state( HC, 6, '0000000000000011', '0000000000000000', '0000000000000000', "P2 THIS correct" );
    _state( HC, 7, '0100000000000000', '0000000000000000', '0000000000000000', "P2 SCREEN correct" );
    _state( HC, 8, '0110000000000000', '0000000000000000', '0000000000000000', "P2 KBD correct" );
    for( let i=0; i<16; i++ ) {
        _state( HC, 9+i, _num2binstr(i,16), '0000000000000000', '0000000000000000', "P2 R"+i +" correct" );
    }
    _state( HC, 25, '0000000000010001', '0000000000000000', '0000000000000000', "P2 R16 (normal not predefined) correct" );

    // test the destinations
    let p3 = N2T.assemble_program(
        [
            'D=1',
            'M=1',
            'A=1',

            'MD=0',
            'AD=1',
            'AMD=-1',
            'AD=0',

            'D=-1',
            'M=-1',

            'A=-1',
            'D=M',  // should fail, since -1 is an illegal memory address

            'M=0',  // should fail, since -1 is an illegal memory address

        ].map( l => [l] ) );
    HC.loadOps( p3.ops );
    HC.reset();
    let pc = 0;
    _state( HC, pc++, '0000000000000000', '0000000000000000', '0000000000000000', "P3 reset and loaded" );

    _state( HC, pc++, '0000000000000000', '0000000000000000', '0000000000000001', "P3 D=1" );
    _state( HC, pc++, '0000000000000000', '0000000000000001', '0000000000000001', "P3 M=1" );
    _state( HC, pc++, '0000000000000001', '0000000000000000', '0000000000000001', "P3 A=1" );

    _state( HC, pc++, '0000000000000001', '0000000000000000', '0000000000000000', "P3 MD=0" );
    _state( HC, pc++, '0000000000000001', '0000000000000000', '0000000000000001', "P3 AD=1" );
    _state( HC, pc++, '1111111111111111', 'n/a', '1111111111111111', "P3 AMD=-1" );
    _state( HC, pc++, '0000000000000000', '0000000000000001', '0000000000000000', "P3 AD=0" );

    _state( HC, pc++, '0000000000000000', '0000000000000001', '1111111111111111', "P3 D=-1" );
    _state( HC, pc++, '0000000000000000', '1111111111111111', '1111111111111111', "P3 M=-1" );
    _is( HC.PC, pc, 'P3 A=-1 PC val' );
    _is( HC.D, '1111111111111111', 'P3 A=-1 D val' );
    _is( HC.A, '1111111111111111', 'P3 A=-1 A val' );
    _fail( () => { HC.M() }, "Illegal Memory Address" );

    _fail( () => { HC.tick(); }, "Illegal Memory Address" );

    _is( HC.PC, pc, 'P3 Fail keeps PC at same place' );

    pc++; // forcibly move to the next tick, and next error
    HC.PC = pc;
    _fail( () => { HC.tick(); }, "Illegal Memory Address" );
    _is( HC.PC, pc, 'P3 Fail keeps PC at same place' );


    // test the calculations
    let p4 = N2T.assemble_program(
        [
            '@13',
            'D=1',
            'M=D-A',
            'D=A',
            '@140',
            'M=D+A',
            'M=D-A',
            'M=A-D',
            'M=D|A',
            'M=D&A',
            'M=M-1',
            'M=M+1',
            'M=D+1',
            'M=D-1',
            'D=A+1',
            'D=A-1',
            'M=D+M',
            'M=D-M',
            'M=M-D',
            'M=!M',
            'M=-M',
            'D=!D',
            'D=-D',
            '@17',
            'D=!A',
            'D=-A',
        ].map( l => [l] ) );
    HC.loadOps( p4.ops );
    HC.reset();
    pc = 0;
    _state( HC, pc++, '0000000000000000', '0000000000000000', '0000000000000000', "P3 reset and loaded" );
    _state( HC, pc++, '0000000000001101', '0000000000000000', '0000000000000000', "P4 set A" );
    _state( HC, pc++, '0000000000001101', '0000000000000000', '0000000000000001', "P4 set D" );
    _state( HC, pc++, '0000000000001101', '1111111111110100', '0000000000000001', "P4 M=D-A" );
    _state( HC, pc++, '0000000000001101', '1111111111110100', '0000000000001101', "P4 D=A" );
    _state( HC, pc++, '0000000010001100', '0000000000000000', '0000000000001101', "P4 @140" );
    _state( HC, pc++, '0000000010001100', '0000000010011001', '0000000000001101', "P4 M=D+A" );
    _state( HC, pc++, '0000000010001100', '1111111110000001', '0000000000001101', "P4 M=D-A" );
    _state( HC, pc++, '0000000010001100', '0000000001111111', '0000000000001101', "P4 M=A-D" );
    _state( HC, pc++, '0000000010001100', '0000000010001101', '0000000000001101', "P4 M=D|A" );
    _state( HC, pc++, '0000000010001100', '0000000000001100', '0000000000001101', "P4 M=D&A" );
    _state( HC, pc++, '0000000010001100', '0000000000001011', '0000000000001101', "P4 M=M-1" );
    _state( HC, pc++, '0000000010001100', '0000000000001100', '0000000000001101', "P4 M=M+1" );
    _state( HC, pc++, '0000000010001100', '0000000000001110', '0000000000001101', "P4 M=D+1" );
    _state( HC, pc++, '0000000010001100', '0000000000001100', '0000000000001101', "P4 M=D-1" );
    _state( HC, pc++, '0000000010001100', '0000000000001100', '0000000010001101', "P4 D=A+1" );
    _state( HC, pc++, '0000000010001100', '0000000000001100', '0000000010001011', "P4 D=A-1" );
    _state( HC, pc++, '0000000010001100', '0000000010010111', '0000000010001011', "P4 M=D+M" );
    _state( HC, pc++, '0000000010001100', '1111111111110100', '0000000010001011', "P4 M=D-M" ) ;
    _state( HC, pc++, '0000000010001100', '1111111101101001', '0000000010001011', "P4 M=M-D" );
    _state( HC, pc++, '0000000010001100', '0000000010010110', '0000000010001011', "P4 M=!M" );
    _state( HC, pc++, '0000000010001100', '1111111101101010', '0000000010001011', "P4 M=-M" );
    _state( HC, pc++, '0000000010001100', '1111111101101010', '1111111101110100', "P4 D=!D" );
    _state( HC, pc++, '0000000010001100', '1111111101101010', '0000000010001100', "P4 D=-D" );
    _state( HC, pc++, '0000000000010001', '0000000000000000', '0000000010001100', "P4 @17" );
    _state( HC, pc++, '0000000000010001', '0000000000000000', '1111111111101110', "P4 D=!A" );
    _state( HC, pc++, '0000000000010001', '0000000000000000', '1111111111101111', "P4 D=-A" );

    // test the jumps
    //   OP   -1    0    1
    let mtrxb = ['1111111111111111','0000000000000000','0000000000000001'];
    let mtrx = [ -1, 0, 1 ];
    [ ['JMP',[true,true,true]],
      ['JEQ',[false,true,false]],
      ['JGT',[false,false,true]],
      ['JGE',[false,true,true]],
      ['JLT',[true,false,false]],
      ['JLE',[true,true,false]],
      ['JNE',[true,false,true]] ]
        .forEach( blk => {
            let jp =  blk[0];
            let ans = blk[1];
            [0,1,2].forEach( idx => {
                let jt = N2T.assemble_program(
                    [
                        '@ONE',
                        'D=-1',
                        'M=' + mtrx[idx],
                        'M=M;' + jp,
                        'D=1',
                        '(ONE)',
                        '@12',
                        '@13',
                    ].map( l => [l] ) );
                HC.loadOps( jt.ops );
                HC.reset();
                _state( HC, 0, '0000000000000000', '0000000000000000', '0000000000000000', jp + " " + idx + ' test readied' );
                _state( HC, 1, '0000000000000101', '0000000000000000', '0000000000000000', jp + " " + idx + ' set A to 5' );
                _state( HC, 2, '0000000000000101', '0000000000000000', '1111111111111111', jp + " " + idx + ' set D to -1' );
                _state( HC, 3, '0000000000000101', mtrxb[idx], '1111111111111111', jp + " " + idx + ' set M to ' + mtrx[idx] );
                if( ans[idx] ) {
                    _state( HC, 5, '0000000000000101', mtrxb[idx], '1111111111111111', jp + " " + idx + ' jump' );
                    _state( HC, 6, '0000000000001100', '0000000000000000', '1111111111111111', jp + " " + idx + ' @12' );
                } else {
                    _state( HC, 4, '0000000000000101', mtrxb[idx], '1111111111111111', jp + " " + idx + ' no jump' );
                    _state( HC, 5, '0000000000000101', mtrxb[idx], '0000000000000001', jp + " " + idx + ' D=1' );
                }
                
            } );
        } );

    if( fails === 0 ) {
        console.log( "Passed all " + passes + " tests" );
    } else {
        console.log( "Failed " + fails + " out of " + (fails+ passes) + " tests" );
//        alert( "Failed " + fails + " out of " + (fails+ passes) + " tests. Check Logs" );
    }


};
