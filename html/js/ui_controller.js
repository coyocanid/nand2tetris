// link up a hack computer to the UI

/*
  Components :

   computerControl
      run
      stop
      step
      sync_screen

   program
      compile_jack
      generate_assembly
      assemble

  UI helpers :
      byId
      textNode
      make
      progLineView

  Linkages :


  Controllers :

*/
N2T.run_tests();

const hackcomputer = N2T.make_hackcomputer();

// convenince functions ------------------------------------

const byId = id => document.getElementById( id );
const textNode = (id,txt) => {
    let el = document.getElementById( id );
    let tn = document.createTextNode(txt);
    el.append( tn );
    return function(newtxt) {
        tn.textContent = newtxt;
    };
}
const make = (tag,classes,txt) => {
    let el = document.createElement(tag);
    classes && classes.forEach( cls => el.classList.add( cls ) );
    txt !== undefined && el.append( document.createTextNode(txt) );
    return el;
}

const progLineView = id => {
    return {
        lineTbl : byId( id ),
        lineUI  : [],
        lastRow : undefined,
        updatePC : function() {
            this.lastRow && this.lastRow.classList.remove( 'curr-pc' );
            let row = this.lineUI[this.opLookup[hackcomputer.PC]];
            row.classList.add( 'curr-pc' );
            this.lastRow = row;
        },
        fill : function(lineData,opLookup) {
            // empty things out
            while(this.lineTbl.firstChild && this.lineTbl.removeChild(this.lineTbl.firstChild));
            this.lineUI.length = 0;
            
            this.lineData = lineData;
            this.opLookup = opLookup;
            let row = make( 'tr', ['head'] );
            row.append( make( 'th', undefined, 'PC' ) );
            row.append( make( 'th', undefined, 'hack op' ) );
            row.append( make( 'th', undefined, 'assembly op' ) );
            this.lineTbl.append( row );

            lineData.forEach( ld => {
                let PC  = ld[0];
                let op  = ld[1];
                let raw = ld[2];
                
                if( PC === undefined ) {
                    row = make( 'tr', ['comment'] );
                    row.append( make( 'td', undefined, '' ) );
                    row.append( make( 'td', undefined, '' ) );
                    row.append( make( 'td', undefined, raw ) );
                } else {
                    row = make( 'tr', ['op'] );
                    row.append( make( 'td', ['PC','line_'+PC], PC ) );

                    // convert this into something cool
//                    op.bitflipper();
//                    row.append( make( 'td', ['word'], op.word ) );
                    
                    row.append( make( 'td', ['code'], raw ) );
                }
                this.lineTbl.append( row );
                this.lineUI.push( row );
            } );
        },
    };
}; //progLineView

// the BOX holding the computer ------------------------------------------

const BOX = {
    step : () => {
        hackcomputer.tick();
        PC( hackcomputer.PC );
        AREG( hackcomputer.A );
        DREG( hackcomputer.D );
        MREG( hackcomputer.RAM[ b2n(hackcomputer.A) ] || '0000000000000000' );
        assembleLines.updatePC();
    }, //hackcomputer.step 

    sync_screen : () => {
        let start = hackcomputer.SCREEN;
        while( start < hackcomputer.KBD ) {
	        let screenbyte = hackcomputer.RAM[ start ] || '0000000000000000';
	        let row = Math.floor( (start - hackcomputer.SCREEN)/256 );
	        let col = (start - hackcomputer.SCREEN) % 512;
	        for( let i = 0; i<16; i++ ) {
	            if( screenbyte.charAt(i) == '1' ) {
		            ctx.fillRect( col, row, col, row );
	            } else {
		            ctx.clearRect( col, row, col, row );
	            }
	        }
	        start++;
        }
    }, //sync_screen
    reset : () => {
        hackcomputer.reset();
        AREG( hackcomputer.A );
        DREG( hackcomputer.D );
        MREG( hackcomputer.M );
        PC( 0 );
        BOX.sync_screen();
        assembleLines.updatePC();
    }
};


// ***********************************************************************

// link up keyboard --------------------------------------

const tnode = textNode('keyboard', '');
window.addEventListener( 'keydown', ev => {
    let code = ev.shiftKey ? ev.keyCode : ev.keyCode + 32;
    tnode( ev.key + " = " + code );
    hackcomputer.RAM[ hackcomputer.KBD ] = _num2binstr(code,16);
    console.log(ev.key + " = " + code );
} );
window.addEventListener( 'keyup', ev => {
    tnode('');
    hackcomputer.RAM[ hackcomputer.KBD ] = '0000000000000000';
} );

// link up screen  ---------------------------------------

const canv = byId( 'screen' );
const ctx = canv.getContext( '2d' );

hackcomputer.onRAMChange( addy => {
    let abyte = hackcomputer.RAM[addy];

    // screen was changed
    if( addy > 16383 && addy < 24576 ) {  // screen is 256 rows x 512 columns pixels
                                          // which is 256 rows of 32 bytes each
        let d = addy - hackcomputer.SCREEN;
        let col = 1 + Math.floor( d/32 );
        let row = 1 + d % 32;
	    for( let i = 0; i<16; i++ ) {
            let x = col + (15 - i);
		    if( abyte.charAt(i) == '1' ) {
			    ctx.fillRect( x, row, 1, 1 );
		    } else {
			    ctx.clearRect( x, row, 1, 1 );
		    }
	    }
    }
} );


// UI components ----------------------------------------

let PC =   textNode( 'PC','0' );
let AREG = textNode( 'AREG','0000000000000000' );
let DREG = textNode( 'DREG','0000000000000000' );
let MREG = textNode( 'MREG','0000000000000000' );

// Program flow and generation

const compile = () => {
    let lines = byId( 'jackcode' ).value.split("\n" );
    let tokens = N2T.tokenize( lines );
    byId( 'tokens' ).value = tokens.join("\n");
    let analyzed_toks = N2T.analyze_tokens( tokens );
    byId( 'analyzed-tokens' ).value = analyzed_toks
	.map( l => l.join(' ') )
	.join("\n");
};

// generate assembly from vm code.
const generate_assembly = program => {
    
    // line --- expands to assembly --
    let lines = program || byId( 'vmcode' ).value.split("\n").map( line => { return [line]; } );

    program = N2T.generate_assembly( lines );
    program.unshift( ["M=D"] );
    program.unshift( ["@SP"] );
    program.unshift( ["D=A"] );
    program.unshift( ["@100"] );
    program.push( ["(END)"] );
    program.push( ["@END"] );
    program.push( ["0;JMP"] );
    let program = N2T.assemble_program( program );

    // make this into something more than a text area
    byId('assemblycode').value = program.join("\n");

    assemble();
}; //generate_assembly

// generate hack binary code from assembly
// and load it into the computer
let assembleLines = progLineView( 'hacklines' );
const assemble = () => {
    
    let lines = byId( 'assemblycode' ).value.split("\n");
    let program = N2T.assemble_program( lines );
    assembleLines.fill( program.lineData ,program.PC2lineIdx );
    assembleLines.updatePC();
    byId('hackcode').value = program.map( op => op.word ).join("\n");
    hackcomputer.loadOps( program
                          .filter( ln => ln[0] !== undefined )
                          .map( ln => ln[1] ) );
    BOX.reset();

}; //assemble

// wire up buttons -----------------------------------------------

byId( 'v2a' ).addEventListener( 'click', generate_assembly );
byId( 'assemble' ).addEventListener( 'click', assemble );

byId( 'step' ).addEventListener( 'click', BOX.step );

byId( 'compile' ).addEventListener( 'click', compile );
